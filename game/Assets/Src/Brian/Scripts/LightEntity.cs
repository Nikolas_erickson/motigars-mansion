using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;

public class LightEntity : MonoBehaviour
{
    public LightState currentState;
    public OnState onState = new OnState();
    public OffState offState = new OffState();

    [SerializeField] public GameObject lightObject;

    // sets light state to on
    public virtual void TurnOn()
    {
        currentState = onState;
        currentState.EnterState(lightObject);
    }

    // sets light state to off
    public virtual void TurnOff()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // returns state for testing purposes
    public LightState GetState()
    {
        return currentState;
    }

    // checks if flashlight is on for testing purposes
    public bool IsOn()
    {
        if (currentState == onState)
        {
            return true;
        }
        return false;
    }
}
