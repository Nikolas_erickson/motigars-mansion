using UnityEngine;

public class LanternUI : MonoBehaviour
{
    [SerializeField] private Lantern lantern;
    public void lanternIconClick()
    {
        Player p = Player.instance;
        if (p == null)
        {
            Debug.LogError("LanternUI: player not found");
            return;
        }
        if (lantern == null)
        {
            Debug.LogError("Lantern: playerFlashlightScript not found");
            return;
        }
        lantern.toggleState();
    }
}