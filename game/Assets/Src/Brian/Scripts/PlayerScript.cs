using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR.Haptics;

public class Player : MonoBehaviour
{
    public static Player instance;
    private static readonly object padlock = new object();

    // Variables dealing with Player movement.
    private Vector2 playerMovement;

    [SerializeField] private float maxSpeed = 5f;
    [SerializeField] private float currentSpeed = 0f;
    [SerializeField] private float acceleration = 5f;

    [SerializeField] private Flashlight flashlight;
    [SerializeField] private Lantern lantern;

    /*
     * Doing it this is not really necessary the more that I think about but Dr. BC did mention making everything
     * private and not public so this is one way to do I guess. This method really is only necessary when dealing 
     * prefabs I believe but I did want to provide a couple of examples. 
     */
    private Rigidbody2D rigidBody;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Warning: More than one instance of Player found! Deleting extra.");
            Destroy(this);
            return;
        }
    }

    public static void CreatePlayer()
    {
        if(instance == null)
        {
            GameObject playerObject = new GameObject();
            instance = playerObject.AddComponent<Player>();
        }
        else
        {
            Debug.Log("Instance of player already exists");
        }
    }

    public Player GetInstance() 
    { 
        return instance; 
    }

    // delete the instance of the player for testing purposes
    public void Delete()
    {
        instance = null;
    }

    // Find the player's rigid body.
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Get the player's movement, calls FaceMouse function.
    private void Update()
    {
        playerMovement.x = Input.GetAxisRaw("Horizontal");
        playerMovement.y = Input.GetAxisRaw("Vertical");

        playerMovement.x += Input.acceleration.x;
        playerMovement.y += Input.acceleration.y;

        if (playerMovement.x != 0f || playerMovement.y != 0f)
        {
            FindObjectOfType<AudioManager>().PlayMovementSound("playerSteps", this.transform.position);
        }

        FaceMouse();
    }

    // Move the player based on the direction of the input.
    private void FixedUpdate()
    {
        currentSpeed += Time.deltaTime * acceleration;
        if (currentSpeed > maxSpeed)
        {
            currentSpeed = maxSpeed;
        }

        rigidBody.velocity = (new Vector2(playerMovement.x, playerMovement.y).normalized) * currentSpeed;

        if (playerMovement.x == 0 && playerMovement.y == 0)
        {
            currentSpeed = 0f;
        }
    }

    // rotates player to face toward mouse
    private void FaceMouse()
    {

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);

        transform.up = direction;
    }

    // Get the player's x and y for other files. 
    public Vector2 getPlayerPos()
    {
        return transform.position;
    }

    // Get current speed of player entity
    public float GetPlayerSpeed()
    {
        return currentSpeed;
    }

    // Move player for testing purposes
    public void SetInput()
    {
        playerMovement.x = 1f;
        playerMovement.y = 1f;
    }

    // set player speed for testing purposes
    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }
}
