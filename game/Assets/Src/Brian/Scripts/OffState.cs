using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffState : LightState
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void EnterState(GameObject lightEntity)
    {
        lightEntity.SetActive(false);
    }
}
