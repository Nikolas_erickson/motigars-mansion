using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Flashlight : LightEntity
{
    public static Flashlight instance; 
    
    [SerializeField] public LightEntity lantern;

    private float batteryLife = 100;
    [SerializeField] private Slider batteryChargeIndicator;

    public static void CreateFlashlight()
    {
        GameObject flashlightObject = new GameObject();
        instance = flashlightObject.AddComponent<Flashlight>();
    }

    // initialized flashlight to off state
    void Start()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // checks for player flashlight inputs and drains the battery if in the on state
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            toggleState();
        }
    }

    private void FixedUpdate()
    {
        if (currentState == onState)
        {
            DrainBattery(0.05f);
            UpdateBatteryChargeIndicator();
            if (batteryLife <= 0f)
            {
                toggleState();
            }
        }
    }

    // switches the state of the flashlight
    public void toggleState()
    {
        if (currentState == onState)
        {
            TurnOff(); // inherited from LightEntity class
        }
        else if(batteryLife >= 0f)
        {
            TurnOn();
        }
    }

    // sets the flashlight to the on state and the lantern to the off state
    public override void TurnOn()
    {
        //Debug.Log("test");
        lantern.TurnOff();
        currentState = onState;
        currentState.EnterState(lightObject);
    }

    // reduces the amount of charge in the flashlight by amount
    public void DrainBattery(float amount)
    {
        batteryLife -= amount;
        if(batteryLife < 0)
        {
            batteryLife = 0;
        }
    }
    
    // increases the amount of charge in the flashlight by amount
    public void AddBatteryLife(float amount)
    {
        batteryLife += amount;
        // clamp battery life at 100
        if (batteryLife > 100)
        {
            batteryLife = 100;
        }
        UpdateBatteryChargeIndicator();
    }

    // updates the on screen battery indicator
    public void UpdateBatteryChargeIndicator()
    {
        batteryChargeIndicator.value = batteryLife;
    }

    // returns current battery life for testing purposes
    public float GetBatteryLife()
    {
        return batteryLife;
    }

    public LightState GetOffState()
    {
        return offState;
    }

    public LightState GetOnState()
    {
        return onState;
    }
}
