using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR.Haptics;

[System.Serializable]
public class Lantern : LightEntity
{
    public static Lantern instance;

    [SerializeField] public LightEntity flashlight;

    public static void CreateLantern()
    {
        GameObject lanternObject = new GameObject();
        instance = lanternObject.AddComponent<Lantern>();
    }

    // initializes lantern to off
    void Start()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // checks for player lantern input
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            toggleState();
        }
    }

    public void toggleState()
    {
        if (currentState == onState)
        {
            TurnOff();
        }
        else
        {
            TurnOn();
        }
    }

    // turns on the lantern and turns off the flashlight
    public override void TurnOn()
    {
        flashlight.TurnOff();
        currentState = onState;
        currentState.EnterState(lightObject);
    }
}
