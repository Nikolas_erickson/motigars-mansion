/*
 * PauseTime.cs
 * Rory Arnone-Wheat
 * This class allows for the
 * game to pause.
 */

using UnityEngine;

interface IPauseTime
{
    void PauseGame();   // Function for pausing game so that it will stop the time and open menu
    void ResumeGame();  // Function for unpausing game so it will start time and close pause menu
}

public class PauseTime : MonoBehaviour, IPauseTime
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject pauseButton;

    // Function for pausing game so that it will stop the time and open menu
    public void PauseGame()
    {
        Debug.Log("Button Clicked");
        pauseButton.SetActive(false);
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    // Function for unpausing game so it will start time and close pause menu
    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        pauseButton.SetActive(true);
        Time.timeScale = 1f;
    }
}
