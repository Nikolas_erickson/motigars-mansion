/*
 * OptionMenu.cs
 * Rory Arnone-Wheat
 * This class allows for the
 * user to open the options
 * menu and to adjust the 
 * options.
 */

using UnityEngine;
using UnityEngine.Audio;

interface IOptionsMenu
{
    void SetVolume(float volume);      // Sets volume of game
    void SetQuality(int qualityIndex); // Sets graphical quality
    void OptionsTime();                // Opens option menu and closes menu it was called from
    void MainTime();                   // Closes option menu and opens the menu it was called from
}

public class OptionsMenu : MonoBehaviour, IOptionsMenu
{
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private AudioMixer audioMixer;

    // Function for adjusting the volume of the game
    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    // Function for adjusting graphical quality
    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // Function for pausing game so that it will stop the time and open menu
    public void OptionsTime()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    // Function for unpausing game so it will start time and close pause menu
    public void MainTime()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
}
