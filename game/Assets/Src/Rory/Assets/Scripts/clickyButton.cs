/*
 * ClickyButton.cs
 * Rory Arnone-Wheat
 * This class allows for buttons to
 * change when being clicked.
 */

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image img;
    [SerializeField] private Sprite up, pressed;

    //Changes to appropriate sprite when button is clicked
    public void OnPointerDown(PointerEventData eventData)
    {
        img.sprite = pressed;
    }

    //Changes to appropriate sprite when button is unclicked
    public void OnPointerUp(PointerEventData eventData)
    {
        img.sprite = up;
    }
}
