/*
 * Sound.cs
 * Rory Arnone-Wheat
 * This class is the parent
 * class for all sounds in 
 * the game.
 */

using UnityEngine;

interface ISound
{
    void Build(Sound s);    // Called for when awake for audio manager is called
    void Play(Sound s, Vector3 pos);            // Calls when Play is called from audio manager
    bool isPlaying();       // Returns true if associated Sound is playing
}

[System.Serializable]
public class Sound : ISound
{
    public string name;                 // Sets a name for any given sound
    public AudioClip clip;              // makes allows a clip to be tied to each sound

    public float minDistance = 5f;      // sets min distance for sound
    public float maxDistance = 30f;     // sets max distance for sound

    public AnimationCurve distanceCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 1), new Keyframe(1, 0) });

    [Range(0f, 1f)]
    public float volume;                // sets volume within range
    [Range(0.1f, 3f)]
    public float pitch;                 // sets pitch within range
    [Range(0f, 1f)]
    public float spatialBlend;          // sets spatial blend within range
    
    //public AudioRolloffMode rolloffMode;
    
    [HideInInspector]
    public AudioSource source;          // allows sound to be heard

    // Instantiates the sounds for the game
    public void Build(Sound s)
    {
        s.source.clip = s.clip;

        s.source.maxDistance = s.maxDistance;
        s.source.minDistance = s.minDistance;
        s.source.volume = s.volume;
        s.source.pitch = s.pitch;
        s.source.spatialBlend = s.spatialBlend;
        s.source.rolloffMode = AudioRolloffMode.Linear;
        //s.source.SetCustomCurve(AudioSourceCurveType.CustomRolloff, distanceCurve);
    }

    // When Play in audio manager is called plays the associated sound
    public virtual void Play(Sound s, Vector3 pos)
    {
        Debug.Log(s.name);
        AudioSource.PlayClipAtPoint(source.clip, pos, s.volume);
    }

    // Allows for checking if set sound is playing
    public bool isPlaying()
    {
        return source.isPlaying;
    }
}
