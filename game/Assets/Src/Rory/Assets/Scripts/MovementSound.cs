/*
 * MovementSound.cs
 * Rory Arnone-Wheat
 * This class is the class
 * for handling movement
 * sounds and ensuring they
 * don't play every frame.
 */

using UnityEngine;

interface IMovementSound
{
    void Build(MovementSound m); // Called for when awake for audio manager is called
    void Play(MovementSound m, Vector3 pos);  // Calls when Play is called from audio manager
    bool isPlaying();            // Returns true if associated movement is playing
}

[System.Serializable]
public class MovementSound : Sound, IMovementSound
{
    public float soundDuration;

    // Instantiates the music for the game
    public void Build(MovementSound m)
    {
        m.source.clip = m.clip;

        m.source.maxDistance = m.maxDistance;
        m.source.minDistance = m.minDistance;
        m.source.spatialBlend = m.spatialBlend;
        m.source.volume = m.volume;
        m.source.pitch = m.pitch;
        m.soundDuration = soundDuration;
        m.source.rolloffMode = AudioRolloffMode.Linear;
        //m.source.SetCustomCurve(AudioSourceCurveType.CustomRolloff, distanceCurve);
    }

    // When Play in audio manager is called plays the associated sound
    public void Play(MovementSound m, Vector3 pos)
    {
        Debug.Log(m.name);
        AudioSource.PlayClipAtPoint(source.clip, pos, m.volume);
    }

    // Allows for checking if set movement is playing
    public bool isPlaying()
    {
        return source.isPlaying;
    }
}
