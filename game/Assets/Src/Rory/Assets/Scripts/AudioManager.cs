/*
 * AudioManager.cs
 * Rory Arnone-Wheat
 * This class manages the
 * sound class and its 
 * subclasses for adding 
 * sound into the game.
 */

using UnityEngine;
using System;
using System.Collections.Generic;

// To call a sound FindObjectOfType<AudioManager>().Play(soundName, Vector3 pos); will call a sound with soundName being the string that is the name of the sound and the position is the location for the sound to play
// To call music FindObjectOfType<AudioManager>().PlayBackgroundMusic(musicName, Vector3 pos); will call music with musicName being the string that is the name of the music
// To call movement FindObjectOfType<AudioManager>().PlayMovementSound(movementName, Vector3 pos); will call movement with movementName being the string that is the name of the movement's sound and the position is the location for the sound to play
interface IAudioManager
{
    void Play(string name, Vector3 pos);                // Plays a sound
    void PlayBackgroundMusic(string name, Vector3 pos); // Plays Background Music
    void PlayMovementSound(string name, Vector3 pos);   // Plays a movement sound if allowed
}

public class AudioManager : MonoBehaviour, IAudioManager
{
    public BackgroundMusic[] music;     // Array of Background music
    public Sound[] sounds;              // Array of Sounds
    public MovementSound[] movements;   // Array of Movement sounds

    private static Dictionary<MovementSound, float> movementTimerDictionairy = new Dictionary<MovementSound, float>();   // Tracks the last time each sound in movement is played for CanPlaySound()

    public static AudioManager instance;

    // Instantiates all sounds and background music with their settings so they can be called in game
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.Build(s);
        }
        foreach (BackgroundMusic b in music)
        {
            b.source = gameObject.AddComponent<AudioSource>();
            b.Build(b);
        }
        foreach (MovementSound m in movements)
        {
            m.source = gameObject.AddComponent<AudioSource>();
            m.Build(m);
            movementTimerDictionairy.Add(m, 0f);
        }
    }

    // Checks if a movement is already playing so it won't play every frame
    private static bool CanPlaySound(MovementSound m)
    {
        switch(m)
        {
            default: return true;
            case MovementSound:
                if(movementTimerDictionairy.ContainsKey(m))
                {
                    float lastTimePlayed = movementTimerDictionairy[m];
                    float moveTimeMax = m.soundDuration;
                    if (lastTimePlayed + moveTimeMax < Time.time)
                    {
                        movementTimerDictionairy[m] = Time.time;
                        return true;
                    } else
                    {
                        return false;
                    }
                } else
                {
                    return false;
                }
        }
    }

    // Starts the music for the main menu for the game's launch
    private void Start()
    {
        PlayBackgroundMusic("MainMenu", Camera.main.transform.position);
    }

    // Plays the called sound if available
    public void Play (string name, Vector3 pos)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " +  name + " not found!");
            return;
        }
        s.Play(s, pos);
    }

    // Plays the called background music if available
    public void PlayBackgroundMusic(string name, Vector3 pos)
    {
        BackgroundMusic b = Array.Find(music, music => music.name == name);
        if (b == null)
        {
            Debug.LogWarning("Music " + name + " not found!");
            return;
        }
        foreach(BackgroundMusic m in music)
            m.source.Stop();
        b.Play(b, pos);
    }

    // Plays the called movement if available and allowed
    public void PlayMovementSound(string name, Vector3 pos)
    {
        MovementSound m = Array.Find(movements, movement => movement.name == name);
        if (m == null)
        {
            Debug.LogWarning("Movement " + name + " not found!");
            return;
        }
        Debug.Log(m.name);
        if (CanPlaySound(m))
        {
            m.Play(m, pos);
        }
    }
}
