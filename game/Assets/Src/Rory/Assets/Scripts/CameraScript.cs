/*
 * CameraScript.cs
 * Rory Arnone-Wheat
 * This class has the camera follow the 
 * player and allows the camera to zoom.
 */

using UnityEngine;
using UnityEngine.SceneManagement;

interface ICameraScript
{
    void ZoomTime(); // Zooms the camera
    void DemoTime();    // sets demoscene bool
    void DoubleZoom(); // DON't CALL THIS: It's for stress test
    float WhatsTheSpeed(); // DON't CALL THIS: It's for stress test
    float HowZoomyIsIt(); // DON't CALL THIS: It's for stress test
    
}

public class CameraScript : MonoBehaviour, ICameraScript
{
    [SerializeField] private float zoomSpeed = 2f;      // sets zoom speed (only for stress test DO NOT ADJUST)

    private bool zoom = false;                           // sets whether to zoom in or out (default is out)
    private float maxZoom = 6f;                         // sets max zoom
    private float minZoom = 3f;                         // sets min zoom
    private static float t = 0.0f;                      // sets value for gradual zoom
    private Transform player;                           // calls singleton that is player so the camera can track them

    [SerializeField] private bool demoscene = false;    // checks if demo scene is triggered

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            player = Camera.main.GetComponent<Transform>();
        } else if (demoscene == false)
        {
            player = Player.instance.GetComponent<Transform>();     // finds player position so camera can follow player
        } else
        {
            player = DemoPlayer.instance.GetComponent<Transform>(); // finds demo player if demo mode active
        }
        
    }

    void Update()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, 0) + new Vector3(0, 0, -10); // Camera follows player
        if (zoom)
        {
            t = 0f;                                     // Resets Zoom speed
            t += 0.5f * Time.deltaTime * zoomSpeed;     // Adjust Zoom over time (zoomSpeed variable is for stress testing)
            ZoomOutCamera(t);                           // zooms out camera (default)
        }
        else
        {
            t = 0f;                                     // Resets Zoom Speed
            t += 0.5f * Time.deltaTime * zoomSpeed;     // Adjust zoom over time (zoomSpeed variable is for stress testing)
            ZoomInCamera(t);                            // zooms in camera if bool is called
        }
    }

    // Function for zooming camera in
    void ZoomInCamera(float target)
    {
        float goalPosition = Mathf.Lerp(Camera.main.orthographicSize, minZoom, target);         // Uses Lerp to zoom camera over time
        Camera.main.orthographicSize = Mathf.Clamp(goalPosition, minZoom, maxZoom);     // Uses Clamp to set bounds for zoom
    }

    // Function for zooming camera out
    void ZoomOutCamera(float target)
    {
        float goalPosition = Mathf.Lerp(Camera.main.orthographicSize, maxZoom, target);         // Uses Lerp to zoom camera over time
        Camera.main.orthographicSize = Mathf.Clamp(goalPosition, minZoom, maxZoom);     // Uses Clamp to set bounds for zoom
    }

    // Call this function when wanting to trigger a zoom
    public void ZoomTime()
    {
        zoom = !zoom;           // Switches bool to either zoom in or out (out is default)
    }

    // For the stress test no other purpose, DO NOT CALL PLZ
    public void DoubleZoom()
    {
        zoomSpeed *= 2;
    }

    // For the stress test no other purpose, DO NOT CALL PLZ
    public float WhatsTheSpeed()
    {
        return zoomSpeed;
    }

    // For the stress test no other purpose, DO NOT CALL PLZ
    public float HowZoomyIsIt()
    {
        return Camera.main.orthographicSize;
    }

    // Sets the demo scene bool
    public void DemoTime()
    {
        demoscene = !demoscene;
    }
}
