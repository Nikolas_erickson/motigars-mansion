/*
 * BackgroundMusic.cs
 * Rory Arnone-Wheat
 * This class is the class
 * for handling background
 * music.
 */

using UnityEngine;

interface IBackgroundMusic
{
    void Build(BackgroundMusic b); // Called for when awake for audio manager is called
    void Play(Sound s, Vector3 pos);                   // Calls when Play is called from audio manager
    bool isPlaying();              // Returns true if associated Music is playing
}

[System.Serializable]
public class BackgroundMusic : Sound, IBackgroundMusic
{

    public bool loop;               // Allows background music to loop

    // Instantiates the music for the game
    public void Build(BackgroundMusic b)
    {
        b.source.clip = b.clip;

        b.source.volume = b.volume;
        b.source.pitch = b.pitch;
        b.source.loop = b.loop;
    }

    // Only is called when override is removed from Sound, plays music backwords
    public override void Play(Sound s, Vector3 pos)
    {
        //source.pitch = -1f;
        source.Play();
    }

    // Allows for checking if set music is playing
    public bool isPlaying()
    {
        return source.isPlaying;
    }
}
