/*
 * SceneSwitch.cs
 * Rory Arnone-Wheat
 * This class allows for buttons 
 * that change scenes or close
 * the game to do so.
 */

using UnityEngine;
using UnityEngine.SceneManagement;

interface ISceneSwitch
{
    void LeaveTheGame();                          // Closes application
    void SwitchScene(string sceneName);           // Switches to correct scene
    void SwitchBackgroundMusic(string musicName); // Switches to the correct background music
}

public class SceneSwitch : MonoBehaviour, ISceneSwitch
{
    // Closes the game on button press
    public void LeaveTheGame()
    {
        Application.Quit();
    }

    // Changes a scene when button is clicked
    public void SwitchScene(string sceneName)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneName);
    }

    // Changes music on SceneSwitch
    public void SwitchBackgroundMusic(string musicName)
    {
        FindObjectOfType<AudioManager>().PlayBackgroundMusic(musicName, Camera.main.transform.position);
    }
}
