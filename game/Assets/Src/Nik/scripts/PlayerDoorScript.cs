using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Attach to Player
// This script is used to get a list of doors that are near the player.
public class PlayerDoorScript : MonoBehaviour
{

    // Check for picking up items
    private void Update()
    {
        /*
        // testing, generate the key
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject k = ItemGenerator.getInstance().generateItem(ItemType.Key, 1);
            k.transform.position = Player.instance.transform.position;
        }
        */
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.tag);
        if(other.gameObject.tag == "Door")
        {
            DoorScript ds = other.gameObject.GetComponent<DoorScript>();
            if(ds == null)
            {
                return;
            }
            int doorId = ds.getID();
            List<Item> invItems = Inventory.getInstance().getItems();
            foreach(Item i in invItems)
            {
                if(i.getType() == ItemType.Key)
                {
                    int keyDoorId = ((Key)i).getDoorID();
                    if(keyDoorId == doorId)
                    {
                        //open door and remove item from inventory
                        ds.open();
                        Inventory.getInstance().Remove(i);
                        return;
                    }
                }
            }
        }
    }

    public List<GameObject> getNearbyDoors()
    {
        List<GameObject> doors = new List<GameObject>();
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.1f);
        //Debug.Log("Checking nearby doors");
        foreach (Collider2D collider in colliders)
        {
            Debug.Log("Checking collider " + collider.name + " with tag " + collider.tag + ".");
            if (collider.CompareTag("Door"))
            {
                doors.Add(collider.gameObject);
            }
        }
        return doors;
    }
}
