using UnityEngine;

public class testLevelInit : MonoBehaviour
{

    IItemGenerator itemGenerator;


    // this is important
    public void getAndPlaceItem()
    {
        itemGenerator = ItemGenerator.getInstance();
        Debug.Log(itemGenerator);
        // this is the part that needs to be added to level initialization
        GameObject myObj = itemGenerator.generateItem(ItemType.Key);

        // This part makes sure that the key is always above the ground layer and not hiding below. 
        SpriteRenderer rend = myObj.GetComponent<SpriteRenderer>();
        rend.sortingOrder = 1;

        myObj.transform.position = RandomPosition();
    }

    private Vector3 RandomPosition()
    {
        int screenSize = 5;
        return new Vector3(Random.Range(-screenSize, screenSize), Random.Range(-screenSize, screenSize), 0);
    }




    void Start()
    {
        getAndPlaceItem();
    }
}
