using UnityEngine;

public interface IItemBuilder
{
   void setDescription(string description);
   void setItemType(ItemType type);
   void setSprite(Sprite sprite);
   public Item getBuiltItem();
}


// This class is used to build items.
// It implements the builder pattern.
public class ItemBuilder : IItemBuilder
{
   private static int _id = 0;
   private Item _item;
   public ItemBuilder(Item blankItem)
   {
      _item = blankItem;
      _item.setId(++_id);
      _item.setName("item_" + _id);
   }

   public void setDescription(string description)
   {
      _item.setDescription(description);
   }

   public void setItemType(ItemType type)
   {
      _item.setItemType(type);
   }

   public void setSprite(Sprite sprite)
   {
      _item.setSprite(sprite);
   }

   public Item getBuiltItem()
   {
      return _item;
   }
}