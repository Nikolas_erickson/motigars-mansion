using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrapDisabler : Item
{
    private static string[] descriptions = {
        "A moon stone. It seems that it will stop a trap from activating, but be destroyed in the process.",
    };

    public TrapDisabler()
    {
    }


    new public static string getRandomDescription()
    {
        return descriptions[UnityEngine.Random.Range(0, descriptions.Length)];
    }



    /// <summary>
    /// Use the item.
    /// </summary>
    /// <returns>Info string that tells the player what happened</returns>
    override public string use()
    {
        Player player = Player.instance;
        if (Player.instance == null)
        {
            return "E#Player instance not found. Where are you?";
        }
        return "E#This item cannot be consumed";

    }
    public ItemType getType()
    {
        return ItemType.TrapDisabler;
    }

    override public string getDescription()
    {
        return "Trap Disabling Item: " + _description;
    }

    override public void printDebugInfo()
    {
        Debug.Log("Trap Disabler Item: " + _name + " ID: " + _id + " Description: " + _description + " Type: " + _type);
    }
}
