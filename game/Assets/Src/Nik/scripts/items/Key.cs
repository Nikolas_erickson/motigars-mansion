using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Key : Item
{
    private static string[] descriptions = {
        "A key to a door.",
        "A mysterious key.",
        "I wonder what this key opens.",
        "A key... but to what?",
        "A key.",
        "It looks like a key.",
        "A key to something.",
        "A key, but what does it open?"
    };
    public int _doorID { get; private set; }

    public Key(int doorID)
    {
        _doorID = doorID;
    }


    new public static string getRandomDescription()
    {
        return descriptions[UnityEngine.Random.Range(0, descriptions.Length)];
    }



    /// <summary>
    /// Use the item.
    /// </summary>
    /// <returns>Info string that tells the player what happened</returns>
    override public string use()
    {
        Player player = Player.instance;
        if (Player.instance == null)
        {
            return "E#Player instance not found. Where are you?";
        }
        /*
        PlayerDoorScript doorScript = player.GetComponent<PlayerDoorScript>();
        if (doorScript == null)
        {
            return "E#No door script found to unlock.";
        }
        List<Door> doors = doorScript.getNearbyDoors();
        foreach (Door door in doors)
        {
            if (door.getID() == _doorID)
            {
                door.unlock();
                return "Unlocked door with key.";
            }
        }
        */
        return "E#No door found to unlock with key.";

    }
    public ItemType getType()
    {
        return ItemType.Key;
    }

    public int getDoorID()
    {
        return _doorID;
    }

    override public string getDescription()
    {
        return "Key: " + _description;
    }

    override public void printDebugInfo()
    {
        Debug.Log("Key: " + _name + " ID: " + _id + " Description: " + _description + " Type: " + _type + " DoorID: " + _doorID);
    }
}
