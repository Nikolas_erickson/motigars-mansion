using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Note : Item
{
    private static string[] descriptions = {
        "This is a note.",
        "This note is very important.",
        "This note is not important.",
        "This note is a clue.",
        "This note is a riddle.",
        "This note is a warning.",
        "This note is a threat.",
        "This note is a love letter.",
        "This note is a shopping list.",
        "This note is a recipe.",
        "This note is a poem.",
        "This note is a song.",
        "This note is a drawing.",
        "This note is a map.",
        "This note is a puzzle.",
        "This note is a joke.",
        "This note is a secret message.",
        "This note is a confession.",
        "This note is a diary entry.",
        "This note is a letter.",
        "This note is a contract.",
        "This note is a report.",
    };



    new public static string getRandomDescription()
    {
        return descriptions[UnityEngine.Random.Range(0, descriptions.Length)];
    }



    /// <summary>
    /// Use the item.
    /// </summary>
    /// <returns>Info string that tells the player what happened</returns>
    override public string use()
    {
        return "You read the note, but it doesn't seem to do anything.";
    }

    override public string getDescription()
    {
        return "Note: " + _description;
    }

    override public void printDebugInfo()
    {
        Debug.Log("Note: " + _name + " ID: " + _id + " Description: " + _description + " Type: " + _type);
    }
}
