using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A battery item that can be used to power devices.
/// </summary>
public class Battery : Item
{
   // array of descriptions for battery items
   private static string[] descriptions = {
         "A battery.",
         "A mysteriious battery. I wonder what it's for.",
         "This would work well with a flashlight.",
         "I hope this battery is charged.",
         "I hope I find more batteries before this one dies.",
         "This will be helpful I have something to put it in.",
    };
   private int _charge;
   public Battery(int charge)
   {
      this._charge = charge;
   }

   // get a random description from the array
   new public static string getRandomDescription()
   {
      return descriptions[UnityEngine.Random.Range(0, descriptions.Length)];
   }


   /// <summary>
   /// Use the battery to charge the flashlight.
   /// </summary>
   /// <returns>Info string that tells the player what happened</returns>
   override public string use()
   {
      // if player instance is not found, return gracefully
      if (Player.instance == null)
      {
         return "E#Player instance not found. Where are you?";
      }
        Flashlight flashlight = GameObject.FindWithTag("Flashlight").GetComponent<Flashlight>();
      if (flashlight == null)
      {
         return "E#No flashlight found to charge.";
      }
      flashlight.AddBatteryLife(_charge);
      return "Battery charged flashlight by " + _charge + " units.";

   }



   // get the description of the battery
   override public string getDescription()
   {
      return "Battery: " + _description;
   }

   // print debug info
   override public void printDebugInfo()
   {
      Debug.Log("Battery: " + _name + " ID: " + _id + " Description: " + _description + " Type: " + _type + " Charge: " + _charge);
   }

   public int getCharge()
   {
      return _charge;
   }
}
