using UnityEngine;


public enum ItemType
{
    Key,
    Battery,
    BigBattery,
    Note,
    TrapDisabler,
    Default
}

public class Item
{
    private static string[] descriptions = {
        "A key to a door.",
        "A mysterious key.",
    };
    protected string _name;
    protected int _id;
    protected string _description;
    protected ItemType _type;
    protected Sprite _icon;



    public void setSprite(Sprite sprite)
    {
        _icon = sprite;
    }

    public void setName(string name)
    {
        _name = name;
    }

    public void setDescription(string description)
    {
        _description = description;
    }

    public void setType(ItemType type)
    {
        _type = type;
    }

    public void setId(int id)
    {
        _id = id;
    }

    public void setItemType(ItemType type)
    {
        _type = type;
    }



    public static string getRandomDescription()
    {
        return descriptions[UnityEngine.Random.Range(0, descriptions.Length)];
    }

    public Item()
    {
        _id = -1;
    }

    ~Item()
    {
        //Debug.Log("Item " + _name + " destroyed.");
    }

    public int getID()
    {
        return _id;
    }

    public string getName()
    {
        return _name;
    }

    public Sprite getIcon()
    {
        return _icon;
    }

    public ItemType getType()
    {
        return _type;
    }

    /// <summary>
    /// Use the item.
    /// </summary>
    /// <returns>Info string that tells the player what happened</returns>
    virtual public string use()
    {
        Debug.Log("Item " + _name + " used.");
        return "Item " + _name + " used.";
    }

    virtual public string getDescription()
    {
        return "Useless item: " + _description;
    }

    virtual public void printDebugInfo()
    {
        Debug.Log("Item: " + _name + " ID: " + _id + " Description: " + _description + " Type: " + _type + " Icon: " + _icon);
    }

    public bool isConsumable()
    {
        return true;
    }
}
