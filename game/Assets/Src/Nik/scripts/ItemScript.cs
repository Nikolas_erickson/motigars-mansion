using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// script attached to item gameobjects
//
// mostly used for setting item sprite and returning item object when picking up item
public class ItemScript : MonoBehaviour
{
    [SerializeField] private bool _canPickUp = true;
    [SerializeField] private bool _canInteract = true;
    private Item _item;
    private bool _set = false;


    private SpriteRenderer _spriteRenderer;

    public void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public bool setItem(Item item)
    {
        if (_set)
        {
            return false;
        }
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _item = item;
        _set = true;

        if (_spriteRenderer != null)
        {
            _spriteRenderer.sprite = item.getIcon();
        }
        return true;
    }

    public bool canPickUp()
    {
        return _canPickUp;
    }

    public bool canInteract()
    {
        return _canInteract;
    }

    public bool addToInventory()
    {
        if (_canPickUp)
        {
            Inventory inventory = Inventory.getInstance();
            inventory.Add(_item);
            Destroy(this.gameObject);
            return true;
        }
        return false;
    }

    public Item getItem()
    {
        return _item;
    }

    public bool interact()
    {
        if (_canInteract)
        {
            //pop up a message or something
            return true;
        }
        return false;
    }
}
