using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerGoalScript : MonoBehaviour
{
    [SerializeField] string sceneName;
    [SerializeField] string musicName;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Goal")
        {
            Debug.Log("You have beaten the level!!!!");
             SceneManager.LoadScene(sceneName);
            FindObjectOfType<AudioManager>().PlayBackgroundMusic(musicName, Camera.main.transform.position);
        }
    }
}
