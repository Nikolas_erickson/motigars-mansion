using UnityEngine;


// this script is attached to the inventoryUI item buttons
// 
// It is used to set the sprite for the button when the inventory screen is intialized
public class InventoryItemButtonScript : MonoBehaviour
{
    // reference to sprite renderer in the button object
    [SerializeField] private SpriteRenderer itemSprite;
    // stores the item information for the item this button represents
    private Item item;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetItem(Item item)
    {
        this.item = item;
        // set item sprite for button graphic
        if (itemSprite != null)
        {
            //Debug.Log("Setting sprite to " + item.getIcon());
            itemSprite.sprite = item.getIcon();
        }
    }

    public int getItemID()
    {
        return item.getID();
    }

    public string getItemName()
    {
        return item.getName();
    }

    public string getItemDescription()
    {
        return item.getDescription();
    }
}
