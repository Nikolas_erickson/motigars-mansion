using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// This is the main logic for the inventory screen
//
// This script controls:
// - the "use" and "drop" functions
// - the highlighting of selected items in the UI
// - the creation of the buttons for each item in the inventory
// - displaying information for the selected item
public class InventoryUIScript : MonoBehaviour
{
    // a reference to the inventory singleton class
    public Inventory inventory;
    // references to prefab button
    [SerializeField] private GameObject itemButton;
    // references to objects in the invetory UI
    [SerializeField] private GameObject inventoryPanel;
    [SerializeField] private Button dropItemButton;
    [SerializeField] private Button useItemButton;
    [SerializeField] private GameObject textBox;
    [SerializeField] private GameObject useItemResult;
    // list of buttons indexed by the id of the item they represent
    private Dictionary<int, GameObject> buttons = new Dictionary<int, GameObject>();
    private Item selectedItem;
    // spriteArray object
    [SerializeField] private SpriteArrays spriteArrays;


    // Start is called before the first frame update
    void Start()
    {
        // set canvas size to camera
        GetComponent<Canvas>().worldCamera = Camera.main;

        // verify that inventory is accessible
        inventory = Inventory.getInstance();
        if (inventory == null)
        {
            Debug.LogError("Inventory UI could not find Inventory instance");
            Destroy(gameObject);
            return;
        }

        // clear text boxes
        resetItemDescription();
        resetUseItemResult();

        // for each item in the inventory create a button and add it to the inventory panel
        foreach (Item item in inventory.getItems())
        {
            //item.printDebugInfo();
            // instantiate a button for each item in the inventory
            GameObject button = Instantiate(itemButton, inventoryPanel.transform);
            // set layer to UI
            button.layer = 5;
            // store item details in the button
            button.GetComponent<InventoryItemButtonScript>().SetItem(item);
            // debug info
            Debug.Log(item.getName() + " added to inventory with sprite " + item.getIcon());
            // add the button to the list of buttons 
            buttons.Add(item.getID(), button);
            // set the background of the button to show it is interactable
            button.GetComponent<Image>().color = Color.gray;
            // add a listener to the button to show the item description when clicked
            button.GetComponent<Button>().onClick.AddListener(() =>
            {
                this.selectButton(item);
            });
        }

        // for the remainder of the space on the panel add blank buttons
        for (int i = buttons.Count; i < inventory.getSize(); i++)
        {
            this.createBlankButton();

        }

    }

    private void createBlankButton()
    {
        GameObject button = Instantiate(itemButton, inventoryPanel.transform);
        button.GetComponent<Button>().onClick.AddListener(() =>
        {
            clearSelectedItem();
            selectedItem = null;
            // set the text box to empty
            textBox.GetComponent<Text>().text = "";
            // disable the drop item button
            dropItemButton.interactable = false;
            // disable the use item button
            useItemButton.interactable = false;
        });
    }

    private void clearSelectedItem()
    {
        if (selectedItem != null)
        {
            buttons[selectedItem.getID()].GetComponent<Image>().color = Color.white;
        }
    }

    public void selectButton(Item item)
    {
        // deselect any other buttons
        clearSelectedItem();
        selectedItem = item;
        // select the selected button
        GameObject button = buttons[item.getID()];
        // show that it is selected by setting its color
        button.GetComponent<Image>().color = Color.white;
        Debug.Log("Button clicked. Item is " + item.getName());
        // set the text box to show the item description
        textBox.GetComponent<Text>().text = item.getDescription();
        // enable the drop item button
        dropItemButton.interactable = true;
        // enable the use item button
        useItemButton.interactable = true;
    }

    // script that is called when drop item is clicked
    public void dropItem()
    {
        if (selectedItem == null)
        {
            return;
        }

        if (Player.instance != null && Player.instance.GetComponent<PlayerItemPickupScript>() != null)
        {
            Player.instance.GetComponent<PlayerItemPickupScript>().dropItem(selectedItem);
            // remove item from inventory
            inventory.Remove(selectedItem);
            // remove item button and create blank one
            Destroy(buttons[selectedItem.getID()]);
            createBlankButton();
            // remove button from list
            buttons.Remove(selectedItem.getID());
        }
        else
        {
            Debug.LogError("Player object could not be found. Can't call drop item script on player");
        }

        resetItemSelection();
    }

    // script for use item button
    public void useItem()
    {
        if (selectedItem == null)
        {
            return;
        }
        // use the item
        string result = selectedItem.use();

        if (result.StartsWith("E#"))
        {
            // error occurred
            useItemResult.GetComponent<Text>().text = result.Substring(2);
            useItemResult.GetComponent<Text>().color = Color.red;
        }
        else
        {
            // success
            useItemResult.GetComponent<Text>().text = result;
            useItemResult.GetComponent<Text>().color = Color.black;
            
            if (selectedItem.isConsumable())
            {
                inventory.Remove(selectedItem);
                Destroy(buttons[selectedItem.getID()]);
                buttons.Remove(selectedItem.getID());
                selectedItem = null;
                resetItemDescription();
                createBlankButton();
            }
        }
    }


    private void resetItemSelection()
    {
        selectedItem = null;
        // disable the drop item button
        dropItemButton.interactable = false;
        // disable the use item button
        useItemButton.interactable = false;
        // clear the text boxes
        resetItemDescription();
        resetUseItemResult();
    }
    private void resetUseItemResult()
    {
        //clear the text box
        useItemResult.GetComponent<Text>().text = "";
        useItemResult.GetComponent<Text>().color = Color.black;
    }
    private void resetItemDescription()
    {
        // clear the description text box
        textBox.GetComponent<Text>().text = "";
    }


}
