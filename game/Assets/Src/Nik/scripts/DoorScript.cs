using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    [SerializeField] private int _doorId;

    public int getID()
    {
        return _doorId;
    }

    public void open()
    {
        gameObject.transform.localScale = new Vector3(0.1f, 1f, 1f);
    }
}
