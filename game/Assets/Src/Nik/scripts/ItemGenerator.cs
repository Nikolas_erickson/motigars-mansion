using UnityEngine;
using System.Collections.Generic;

struct ItemSpriteAndDescription
{
    public Sprite sprite;
    public string description;
}

public interface IItemGenerator
{
    GameObject generateItem(ItemType type, int keyId = 1);
}

/// <summary>
/// Singleton class for item generator. This class is used to generate items such as keys and batteries.
/// </summary>
public sealed class ItemGenerator : IItemGenerator
{
    System.Random rand = new System.Random();
    // a list of default item descriptions that match each sprite in the default item sprites (one to one correspondence)
    private static string[] defaultItemDescriptions = {
                                                "A small mushroom.",
                                                "A PokeBall.",
                                                "A rare candy.",
                                                "A Master Ball.",
                                                "A bike voucher.",
                                            };

    // singleton instance reference
    private static ItemGenerator _instance;
    private static readonly object padlock = new object();
    // spriteArray object
    [SerializeField] private SpriteArrays spriteArray;


    // sets item scale in game, change to alter size objects appear in world
    private static Vector3 _globalItemScale = new Vector3(3f, 3f, 1f);




    private ItemGenerator()
    {
        if (spriteArray == null)
        {
            //Debug.Log("Warning: SpriteArray not set in ItemGenerator. Setting to default.");
            loadSpriteArray();
            //Debug.Log("SpriteArray now set to " + spriteArray.name + ".");
        }
    }

    public static ItemGenerator getInstance()
    {
        if (_instance == null)
        {
            lock(padlock)
            {
                if(_instance == null)
                {
                    _instance = new ItemGenerator();
                }
            }
        }
        return _instance;
    }

    public void test()
    {
        GameObject myObj = generateItem(ItemType.Key);
        myObj.transform.position = RandomPosition();
    }


    /// <summary>
    /// Generates an item GameObject, such as a key or battery. Optionally specify the type of item to generate.
    /// </summary>
    /// <param name="type">Optional parameter to specify type of item. Use ItemType enum.</param>
    /// <returns>Returns a GameObject for an item that can then be placed wherever needed.</returns>
    public GameObject generateItem(ItemType type = ItemType.Default, int keyId = 1)
    {
        Item item = itemGenerationHelper(type, keyId);
        return placeItemAsObject(item);
    }

    // takes an item and creates a gameobject for it
    public GameObject placeItemAsObject(Item item)
    {
        GameObject itemObject = new GameObject();

        // set up item object
        itemObject.name = "item_" + item.getID();
        itemObject.transform.localScale = _globalItemScale;

        // tag and layer set so the player pickup script can find the items
        itemObject.tag = "Item";
        itemObject.layer = LayerMask.NameToLayer("items");

        // add components

        itemObject.AddComponent<BoxCollider2D>().size = new Vector2(0.1f, 0.1f); // sets size of collider so that it is easier to pick up items
        itemObject.AddComponent<SpriteRenderer>().sortingOrder = 1; // set rendering order so that items are above the floor
        itemObject.AddComponent<Rigidbody2D>().gravityScale = 0; // set gravity scale so items stay put
        itemObject.AddComponent<ItemScript>().setItem(item); // set item script to item



        return itemObject;
    }
    public Item itemGenerationHelper(ItemType type, int keyId = 1)
    {
        // load sprite array if it is not already loaded
        if (spriteArray == null)
        {
            loadSpriteArray();
        }

        // get matching pair of sprite and description
        ItemSpriteAndDescription isd = getRandomItemSpriteAndDescription(type);

        // create item based on type
        Item item;
        switch (type)
        {
            case ItemType.Battery:
                item = new Battery(50);
                break;
            case ItemType.BigBattery:
                Debug.Log("creating big battery");
                item = new Battery(100);
                break;
            case ItemType.Key:
                item = new Key(keyId);
                break;
            case ItemType.Note:
                item = new Note();
                break;
            case ItemType.TrapDisabler:
                item = new TrapDisabler();
                break;
            default:
                item = new Item();
                break;
        }

        // build item
        IItemBuilder itemBuilder = new ItemBuilder(item);
        itemBuilder.setSprite(isd.sprite);
        Debug.Log("using sprite, "+isd.sprite);
        itemBuilder.setDescription(isd.description);
        itemBuilder.setItemType(type);

        // return item
        return itemBuilder.getBuiltItem();
    }



    /// <summary>
    /// Spawns items in the level.
    /// </summary>
    /// <param name="randomItems">number of random items to generate</param>
    /// <param name="totalKeys">number of keys to generate</param>
    /// <param name="totalBatteries">number of batteries to generate</param>
    /// <param name="totalNotes">number of notes to generate</param>
    public void spawnItems(int level = 1, int randomItems = 2, int totalKeys = 2, int totalBatteries = 8, int totalNotes = 1)
    {
        Debug.Log("level = " + level + ", rand = "+randomItems);
        Debug.Log("Spawning Items");

        //generate one trap disabling item per level
        for(int i = 0; i<1; i++)
        {
            GameObject g = generateItem(ItemType.TrapDisabler);
            g.transform.position = getRandomPosition(level);
        }

        for (int i = 0; i < randomItems; i++)
        {
            GameObject g = generateItem(ItemType.Default);
            g.transform.position = getRandomPosition(level);
        }

        for (int i = 0; i < totalKeys; i++)
        {
            GameObject g = generateItem(ItemType.Key);
            g.transform.position = getRandomPosition(level);
        }

        for (int i = 0; i < totalBatteries; i++)
        {
            // generate one big battery or two small batteries
            if (rand.Next(0, 2) == 0)
            {
                // big battery
                GameObject g = generateItem(ItemType.BigBattery);
                g.transform.position = getRandomPosition(level);
            }
            else
            {
                // 2 small batteries
                GameObject g = generateItem(ItemType.Battery);
                g.transform.position = getRandomPosition(level);
                g = generateItem(ItemType.Battery);
                g.transform.position = getRandomPosition(level);
            }
        }

        for (int i = 0; i < totalNotes; i++)
        {
            GameObject g = generateItem(ItemType.Note);
            g.transform.position = getRandomPosition(level);
        }
    }

    // for unit test purposes, makes sure the default item descriptions match the default item sprites
    // placed here beacuse of protection levels
    public bool defaultItemDescriptionsMatchSpriteArray()
    {
        return defaultItemDescriptions.Length == spriteArray.defaultSprites.Length;
    }









    // not used in game, check getRandomPosition function below
    private Vector3 RandomPosition()
    {
        return new Vector3(rand.Next(-10, 10), rand.Next(-10, 10), 0);
    }


    // gets sprites from sprite array and descriptions from class definitions
    private ItemSpriteAndDescription getRandomItemSpriteAndDescription(ItemType itemType)
    {
        ItemSpriteAndDescription isd = new ItemSpriteAndDescription();
        switch (itemType)
        {
            case ItemType.Battery:
                isd.sprite = spriteArray.getRandomSprite(ItemType.Battery);
                isd.description = Battery.getRandomDescription();
                break;
            case ItemType.BigBattery:
                isd.sprite = spriteArray.getRandomSprite(ItemType.BigBattery);
                isd.description = Battery.getRandomDescription();
                break;
            case ItemType.Key:
                isd.sprite = spriteArray.getRandomSprite(ItemType.Key);
                isd.description = Key.getRandomDescription();
                break;
            case ItemType.Note:
                isd.sprite = spriteArray.getRandomSprite(ItemType.Note);
                isd.description = Note.getRandomDescription();
                break;
            case ItemType.TrapDisabler:
                isd.sprite = spriteArray.getRandomSprite(ItemType.TrapDisabler);
                isd.description = TrapDisabler.getRandomDescription();
                break;
            default:
                int num = rand.Next(0, defaultItemDescriptions.Length);
                isd.sprite = spriteArray.getRandomSprite(ItemType.Default, num);
                isd.description = defaultItemDescriptions[num];
                break;
        }
        return isd;
    }

    // a fix for an error that occurred whe the sprite array was not loading properly
    private void loadSpriteArray()
    {
        spriteArray = Resources.Load<SpriteArrays>("ItemSprites");
    }


    public struct Room
    {
        public int x1, x2, y1, y2;
        public Room(int x1, int x2, int y1, int y2)
        {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }
    }
    static private List<Room> basementRooms = new List<Room>();
    static private List<Room> firstFloorRooms = new List<Room>();
    /// <summary>
    /// Returns a random position within the basement level.
    /// If room is specified, returns a random position within that room.
    /// Rooms are numbered 0-7 from left to right, top to bottom.
    /// </summary>
    /// <param name="room">Optional parameter. Returned position will be within the room specified.</param>
    /// <returns></returns>
    private Vector2 getRandomPosition(int level = 1, int roomNum = -1)
    {


        List<Room> rooms;
        if(level == 1)
        {
            if(basementRooms.Count == 0)
            {
                basementRooms.Add(new Room(-38, 52, 210, 337));
                basementRooms.Add(new Room(96, 143, 230, 290));
                basementRooms.Add(new Room(-128, -7, 98, 187));
                basementRooms.Add(new Room(-24, 37, -39, 50));
                basementRooms.Add(new Room(141, 232, -40, 50));
                basementRooms.Add(new Room(-144, -37, -187, -143));
                basementRooms.Add(new Room(51, 148, -219, -156));
                basementRooms.Add(new Room(231, 261, -250, -205));
            }
            rooms = basementRooms;
        }
        else
        {
            if(firstFloorRooms.Count == 0)
            {
                firstFloorRooms.Add(new Room(-133, 0, 0, 131));
                firstFloorRooms.Add(new Room(17, 149, 0, 131));
                firstFloorRooms.Add(new Room(168, 298, 0, 131));
                firstFloorRooms.Add(new Room(17, 149, -145, -20));
                firstFloorRooms.Add(new Room(-133, 0, -296, -170));
                firstFloorRooms.Add(new Room(17, 149, -296, -170));
                firstFloorRooms.Add(new Room(168, 298, -296, -170));
            }
            rooms = firstFloorRooms;
        }


        if (roomNum == -1)
        {
            roomNum = rand.Next(0, rooms.Count);
        }
        Room r = rooms[roomNum];
        return new Vector2((float)rand.Next(r.x1, r.x2) / 10, (float)rand.Next(r.y1, r.y2) / 10);

    }
}
