using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inputTesterInventory : MonoBehaviour
{
    // flag for toggling state
    private bool isInventoryOpen = false;
    // threadsafety object
    private static readonly object padlock = new object();
    // reference to inventoryUI prefab
    [SerializeField] private GameObject inventoryUI;
    // reference to onscreen button to open inventory
    [SerializeField] private Button toggleInventoryButton;
    // reference to created instance
    private GameObject inventoryUIInstance;
    // reference to player inventory singleton
    private Inventory inventory;

    
    // Start is called before the first frame update
    void Start()
    {

        inventory = Inventory.getInstance();
        if (inventory == null)
        {
            Debug.LogError("Inventory is null");
            return;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("i"))
        {
            toggleInventory();
        }

        /*
        // for testing purposes
        // add item to inventory when 'a' is pressed
        if (Input.GetKeyDown("m"))
        {
            ItemGenerator itemGenerator = ItemGenerator.getInstance();
            ItemType type = (ItemType)Random.Range(0, 4);
            GameObject go = itemGenerator.generateItem(type);
            go.GetComponent<ItemScript>().addToInventory();

            //itemGenerator.generateItem(ItemType.Battery).GetComponent<ItemScript>().addToInventory();
            //itemGenerator.generateItem(ItemType.Default).GetComponent<ItemScript>().addToInventory();
            //itemGenerator.generateItem(ItemType.Note).GetComponent<ItemScript>().addToInventory();
            Debug.Log("Item added to inventory");
        }

        // for testing purposes
        // initialize inventory when 'r' is pressed
        if (Input.GetKeyDown("r"))
        {
            Debug.Log("Inventory initialized, size: " + inventory.Count());
        }
        */

    }

    public void openInventoryUI()
    {
        // set flag
        isInventoryOpen = true;
        // create inventoryUI object
        inventoryUIInstance = Instantiate(inventoryUI);
        inventoryUIInstance.transform.position = new Vector3(0, 0, 0);
        inventoryUIInstance.GetComponent<Canvas>().sortingLayerName = "UI";
        inventoryUIInstance.GetComponent<Canvas>().sortingOrder = 1;
        //Debug.Log("Inventory is open");
    }

    public void closeInventoryUI()
    {
        // clear flag
        isInventoryOpen = false;
        // destroy UI
        Destroy(inventoryUIInstance);
        //Debug.Log("Inventory is closed");
    }

    public void toggleInventory()
    {
        lock(padlock)
        {
            if (isInventoryOpen)
            {
                closeInventoryUI();
            }
            else
            {
                openInventoryUI();
            }
        }
    }
}
