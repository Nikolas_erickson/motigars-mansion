using UnityEngine;

public class FlashlightUIScript : MonoBehaviour
{
	[SerializeField] private Flashlight fs;
	public void flashlightIconClick()
	{
		Player p = Player.instance;
		if(p == null)
		{
			Debug.LogError("FlashlightUIScript: player not found");
			return;
		}
		if(fs == null)
		{
			Debug.LogError("FlashlightUIScript: playerFlashlightScript not found");
			return;
		}
		fs.toggleState();
	}
}