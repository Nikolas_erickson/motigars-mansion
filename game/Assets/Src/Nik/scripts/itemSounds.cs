using UnityEngine;

public sealed class ItemSounds
{
	private static ItemSounds _instance;
	private static readonly object padlock = new object();

	private ItemSounds()
	{
	}

	public static ItemSounds getInstance()
	{
        if (_instance == null)
        {
            lock(padlock)
            {
                if(_instance == null)
                {
                    _instance = new ItemSounds();
                }
            }
        }
		return _instance;
	}

	public void playPickUpSound(AudioManager am, ItemType itemType, Vector3 pos)
	{
		if(am == null)
		{
			return;
		}

        switch(itemType)
        {
            case ItemType.Note:
                // play note pickup sound
            	am.Play("letterOpen", pos);
                break;
            case ItemType.Key:
                // play key pickup sound
            	am.Play("keyJingle", pos);
                break;
            case ItemType.Battery:
            case ItemType.BigBattery:
                // play battery pickup sound
            	// TODO - no sound for this
                break;
            case ItemType.Default:
                // play misc item sound
            	// TODO - no sound for this
                break;
        }
	}
}