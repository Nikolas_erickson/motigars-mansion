using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// script for controlling the flashlight on the player
//
// controls
// - flashlight power drain
// - buttons to activate flashlight
// - 
public class playerFlashlightScript : MonoBehaviour
{
    public float drainSpeed = 5;
    float batteryLife = 100;
    bool isOn = true;
    [SerializeField] private GameObject flashlightMask;

    [SerializeField] private Slider batteryChargeIndicator;
    // Start is called before the first frame update
    void Start()
    {
        if(batteryChargeIndicator == null)
        {
            batteryChargeIndicator = BatterChargeIndicatorLocatorScript.instance.GetComponent<Slider>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        // if keypress F, toggle flashlight
        if (Input.GetKeyDown(KeyCode.F))
        {
            toggleFlashlight();
        }

        // drain battery life if flashlight is on
        if (isOn)
        {
            batteryLife -= drainSpeed * Time.deltaTime;
            updateBatteryChargeIndicator();
            if (batteryLife <= 0)
            {
                turnOffFlashlight();
            }
        }

    }

    public void toggleFlashlight()
    {
        if (batteryLife <= 0)
        {
            turnOffFlashlight();
            return;
        }
        if (isOn)
        {
            turnOffFlashlight();
        }
        else
        {
            turnOnFlashlight();
        }
    }

    void turnOffFlashlight()
    {
        isOn = false;
        flashlightMask.SetActive(false);
    }

    void turnOnFlashlight()
    {
        isOn = true;
        flashlightMask.SetActive(true);
    }

    public void addBatteryLife(float amount)
    {
        batteryLife += amount;
        // clamp battery life at 100
        if (batteryLife > 100)
        {
            batteryLife = 100;
        }
        updateBatteryChargeIndicator();
    }

    private void updateBatteryChargeIndicator()
    {
        // update battery charge indicator
        //Debug.Log("Battery life: " + batteryLife);
        batteryChargeIndicator.value = batteryLife;
    }
}
