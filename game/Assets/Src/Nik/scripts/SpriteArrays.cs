using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// An object for storing the sprites for items and returning a random sprite
[CreateAssetMenu(fileName = "SpriteArrays", menuName = "ScriptableObjects/SpriteArrays", order = 1)]
public class SpriteArrays : ScriptableObject
{
    public static SpriteArrays instance;

    public SpriteArrays()
    {
        if (instance != null)
        {
            //Debug.Log("More than one SpriteArrays instance found.");
            return;
        }
        //Debug.Log("SpriteArrays created.");
        instance = this;
    }
    public Sprite[] batterySprites;
    public Sprite[] bigBatterySprites;
    public Sprite[] keySprites;
    public Sprite[] noteSprites;
    public Sprite[] trapDisablerSprites;
    public Sprite[] defaultSprites;
    public Sprite getRandomSprite(ItemType type, int num = -1)
    {
        if (num != -1)
        {
            return defaultSprites[num];
        }
        else
        {
            // seed random number generator
            Random.InitState(System.DateTime.Now.Millisecond);
        }
        switch (type)
        {
            case ItemType.Battery:
                return batterySprites[Random.Range(0, batterySprites.Length)];
            case ItemType.BigBattery:
                return bigBatterySprites[Random.Range(0, bigBatterySprites.Length)];
            case ItemType.Key:
                return keySprites[Random.Range(0, keySprites.Length)];
            case ItemType.Note:
                return noteSprites[Random.Range(0, noteSprites.Length)];
            case ItemType.TrapDisabler:
                return trapDisablerSprites[Random.Range(0, trapDisablerSprites.Length)];
            default:
                return defaultSprites[Random.Range(0, defaultSprites.Length)];
        }
    }
}
