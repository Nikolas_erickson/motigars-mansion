using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// This script is only for location of the slider object
public class BatterChargeIndicatorLocatorScript : MonoBehaviour
{
    private Slider batteryChargeIndicator;
    static public BatterChargeIndicatorLocatorScript instance;
    static public readonly object padlock = new object();
    // Start is called before the first frame update
    void Start()
    {
        if (batteryChargeIndicator == null)
        {
            batteryChargeIndicator = GetComponent<Slider>();
        }
        if (instance == null)
        {
            lock(padlock)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
