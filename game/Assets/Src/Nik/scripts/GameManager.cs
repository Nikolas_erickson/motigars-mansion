using UnityEngine;

// This object is a singleton service provider for the game. It provides access to the game-wide objects and services.
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static readonly object padlock = new object();
    //[SerializeField] private GameObject player;
    //[SerializeField] private GameObject camera;
    //[SerializeField] private GameObject itemGenerator;



    // publicly accessible instance of the player
    public ItemGenerator itemGenerator;
    public Inventory inventory;

    void Awake()
    {
        if (instance == null)
        {
            lock(padlock)
            {
                if(instance == null)
                {
                    instance = this;
                    itemGenerator = ItemGenerator.getInstance();
                    inventory = Inventory.getInstance();
                }
            }
        }
        else
        {
            Debug.Log("Instance already exists, destroying extra game manager!");
            Destroy(gameObject);
            return;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
