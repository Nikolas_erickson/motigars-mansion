using System.Collections.Generic;


/// <summary>
/// Singleton class for the inventory. This class is used to store items that the player has collected.
/// </summary>
public sealed class Inventory
{
    private static Inventory _instance;
    private static readonly object padlock = new object();
    private List<Item> items;
    private int size;

    // private constructor to prevent instantiation
    private Inventory()
    {

        items = new List<Item>();
        size = 10;
    }

    public static Inventory getInstance()
    {
        if (_instance == null)
        {
            lock (padlock)
            {
                if (_instance == null)
                {
                    _instance = new Inventory();
                }
            }
        }
        return _instance;
    }

    // Delete the instance of the inventory
    // for testing purposes
    public void Delete()
    {
        _instance = null;
    }
    ~Inventory()
    {
        //Debug.Log("Inventory destroyed.");
    }
    public bool Add(Item item)
    {
        if (items.Count >= size)
        {
            //Debug.Log("Not enough room.");
            return false;
        }
        items.Add(item);
        return true;
    }

    public bool Remove(Item item)
    {
        if (items.Contains(item))
        {
            items.Remove(item);
            return true;
        }
        return false;
    }

    public void clearInventory()
    {
        items.Clear();
    }

    public int Count()
    {
        return items.Count;
    }

    public int SpaceAvailable()
    {
        return size - items.Count;
    }

    public List<Item> getItems()
    {
        return items;
    }

    public int getSize()
    {
        return size;
    }

    public bool hasTrapDisablerItem()
    {
        foreach(Item i in items)
        {
            if(i.getType() == ItemType.TrapDisabler)
            {
                return true;
            }
        }
        return false;
    }

    public bool removeOneTrapDisablerItem()
    {
        foreach(Item i in items)
        {
            if(i.getType() == ItemType.TrapDisabler)
            {
                items.Remove(i);
                return true;
            }
        }
        return false;
    }
}