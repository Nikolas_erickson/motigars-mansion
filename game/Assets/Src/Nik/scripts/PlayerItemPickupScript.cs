using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItemPickupScript : MonoBehaviour
{

    // Check for picking up items
    private void Update()
    {
        /* no longer in use, not mobile friendly, also chasing objects around is not fun
        // on keydown e pick up items near player
        if (Input.GetKeyDown(KeyCode.E))
        {
            pickUpNearItems();
        }
        */
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.tag);
        if(other.gameObject.tag == "Item")
        {
            // verify the reference to inventory is valid
            Inventory inv = Inventory.getInstance();
            if(inv == null)
            {
                return;
            }
            // check for item script, if not there do nothing
            ItemScript otherItemScript = other.gameObject.GetComponent<ItemScript>();
            if(otherItemScript == null)
            {
                return;
            }
            // get the Item object if we can pick it up
            if(!otherItemScript.canPickUp())
            {
                return;
            }
            Item otherItem = otherItemScript.getItem();
            // add to inventory, if successful remove gameobject
            if(inv.Add(otherItem))
            {
                // if ItemSounds object available, use it to play pick up sound
                if(ItemSounds.getInstance() != null && Player.instance != null)
                {
                    AudioManager am = FindObjectOfType<AudioManager>();
                    ItemSounds.getInstance().playPickUpSound(am, otherItem.getType(), Player.instance.gameObject.transform.position);
                }
                // destroy object that was picked up
                Destroy(other.gameObject);
            }
        }
    }


    // no longer in use
    private void pickUpNearItems()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.8f);
        Debug.Log("Picking up items, checking " + colliders.Length + " colliders.");
        foreach (Collider2D collider in colliders)
        {
            Debug.Log("Checking collider " + collider.name + " with tag " + collider.tag + ".");
            if (collider.CompareTag("Item"))
            {
                Debug.Log("Item found");
                Item item = collider.GetComponent<ItemScript>().getItem();
                if (item != null)
                {
                    if (Inventory.getInstance().Add(item))
                    {
                        Destroy(collider.gameObject);
                    }
                }
            }
        }
    }

    public void dropAllItemsAndRemoveFromInventory()
    {
        Inventory inv = Inventory.getInstance();
        List<Item>  allItems = inv.getItems();
        
        foreach(Item i in allItems)
        {
            Debug.Log("Drop item");
            dropItem(i);
        }

        inv.clearInventory();
    }

    // called from the inventory screen when dropping items
    public GameObject dropItem(Item item)
    {
        GameObject itemObj = ItemGenerator.getInstance().placeItemAsObject(item);
        itemObj.transform.position = transform.position + new Vector3(1, 0, 0);
        return itemObj;
    }
}
