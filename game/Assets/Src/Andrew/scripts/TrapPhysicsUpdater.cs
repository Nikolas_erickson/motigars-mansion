/*
 * TrapPhysicsUpdater.cs
 * Andrew VanCorbach
 * This class updates whether a trap is colliding with a player or not
 * Each trap GameObject has this script added to it
 */

using UnityEngine;

public class TrapPhysicsUpdater : MonoBehaviour
{
    private bool colliding = false;                     // set if trap is colliding with player

    // set colliding to true when player is on trap
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            colliding = true;
        }
    }

    // set colliding to false when player leaves trap
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            colliding = false;
        }
    }

    // return colliding
    public bool IsColliding()
    {
        return colliding;
    }
}
