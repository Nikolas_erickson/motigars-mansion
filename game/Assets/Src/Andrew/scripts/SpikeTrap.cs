/*
 * SpikeTrap.cs
 * Andrew VanCorbach
 * The class for spike traps
 * It raises and lowers based on a timer and kills the player when raised and colliding
 * It has a custom constructor, and override methods for the Trap superclass
 */

using UnityEngine;
using UnityEngine.SceneManagement;

public class SpikeTrap : Trap
{
    private bool isRaised;                     // bool for if trap is raised or not
    private float cooldown;                    // float how long the trap is in a state before changing
    private float cooldownTimer;               // float for how much time is left before the trap changes

    // class constructor
    public SpikeTrap(float cdown, float cTimer, float xPos, float yPos)             // need to change this to increase cohesion
    {
        // create trap object and assign componenets
        trap = new GameObject();
        collider = trap.AddComponent<BoxCollider2D>();
        spriteRenderer = trap.AddComponent<SpriteRenderer>();
        anim = trap.AddComponent<Animator>();
        trapPhysicsUpdater = trap.AddComponent<TrapPhysicsUpdater>();

        // get animations for open and closed
        openAnim = Resources.Load<RuntimeAnimatorController>("Spike/SpikeTrapOpen");
        closedAnim = Resources.Load<RuntimeAnimatorController>("Spike/SpikeTrapClosed");

        // modify components as needed
        trap.tag = "Trap";
        trap.name = "spikeTrap";
        collider.isTrigger = true;
        collider.size = new Vector2(2.671912f, 3.854107f);
        collider.offset = new Vector2(0f, 0.1479467f);
        spriteRenderer.sortingLayerName = "Traps";
        Open = openAnim;
        Closed = closedAnim;
        trap.transform.position = new Vector2(xPos, yPos);

        // set trap variables
        isRaised = false;
        cooldown = cdown;
        cooldownTimer = cTimer;
        updateFunc = () => UpdateTrap();
        currentState = closedState;
        currentState.EnterState(this);

        // add update function callback
        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // updates cooldown and trigger detection every frame
    protected override void UpdateTrap()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer < 0)                                      // switches trap state based on cooldown
        {
            isRaised = !isRaised;
            cooldownTimer = cooldown;

            if (isRaised)                                           
            {
                PlayTrapSound("spikeTrapUp");
                currentState = openState;
            }
            else
            {
                PlayTrapSound("spikeTrapDown");
                currentState = closedState;
            }
            currentState.EnterState(this);
        }

        if(trapPhysicsUpdater.IsColliding() && isRaised)            // if player walks over and trap is raised
        {
            TrapHitPlayer();
        }
    }
    
    // kills player
    protected override void TrapHitPlayer()
    {
        PlayTrapSound("spikeTrapHitPlayer");
        PlayTrapMusic("GameOver");

        SceneManager.LoadScene("GameOverScene");
    }

    // play death music on trap hitting player
    private void PlayTrapMusic(string music)
    {
        if (AudioManager.instance != null)
        {
            AudioManager.instance.PlayBackgroundMusic(music, Camera.main.transform.position);
        }
        else
        {
            Debug.Log("Audio Manager not found");
        }
    }

    // sets cooldown and cooldown timer of trap
    public override void SetParams(float cd, float timer)
    {
        cooldown = cd;
        cooldownTimer = timer;
    }

    // return whether the trap is raised or not
    public override bool CheckTrapStatus()
    {
        return isRaised;
    }

    // Deletes Trap
    public override void DeleteTrap()
    {
        Debug.Log("Delete Trap");
        TrapUpdater.RemoveUpdateCallback(updateFunc);
        isRaised = false;
        UnityEngine.Object.Destroy(trap);
    }
}
