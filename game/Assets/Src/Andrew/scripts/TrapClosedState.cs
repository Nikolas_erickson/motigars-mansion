using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapClosedState : TrapState
{
    public override void EnterState(Trap trap)
    {
        trap.ChangeClosedState();
    }
}
