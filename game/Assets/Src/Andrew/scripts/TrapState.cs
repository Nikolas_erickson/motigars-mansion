using System.Xml;
using UnityEngine;

public abstract class TrapState         // will probably eventually move to trap superclass
{
    public abstract void EnterState(Trap trap);
}