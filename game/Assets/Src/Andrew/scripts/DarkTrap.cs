/*
 * DarkTrap.cs
 * Andrew VanCorbach
 * The class for dark traps
 * When the player enters, a call is made to activate darkness, then after a countdown, the darkness is removed
 * It has a custom constructor, and override methods for the Trap and ActiveTrap superclass
 */

using UnityEngine;

public class DarkTrap : ActiveTrap
{
    
    private float darkRadius;                       // doesn't do anything right now
    private float darkTime;                         // amount of time that the darkness will be active for
    private bool darkActive;                        // bool whether the darkness is active or not

    // class constructor
    public DarkTrap(float dRadius, float dTime, float xPos, float yPos)
    {
        // create trap object and assign componenets
        trap = new GameObject();
        collider = trap.AddComponent<BoxCollider2D>();
        spriteRenderer = trap.AddComponent<SpriteRenderer>();
        anim = trap.AddComponent<Animator>();
        trapPhysicsUpdater = trap.AddComponent<TrapPhysicsUpdater>();

        // get animations for open and closed
        openAnim = Resources.Load<RuntimeAnimatorController>("Dark/DarkTrapOpen");
        closedAnim = Resources.Load<RuntimeAnimatorController>("Dark/DarkTrapClosed");

        // modify components as needed
        trap.tag = "Trap";
        trap.name = "darkTrap";
        collider.isTrigger = true;
        collider.size = new Vector2(3f, 3f);
        collider.offset = new Vector2(0f, 0f);
        spriteRenderer.sortingLayerName = "Traps";
        Open = openAnim;
        Closed = closedAnim;
        trap.transform.position = new Vector2(xPos, yPos);

        // set trap variables
        darkActive = false;
        SetParams(dRadius, dTime);
        updateFunc = () => UpdateTrap();
        currentState = closedState;
        currentState.EnterState(this);

        // add update function callback
        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // update trap every frame
    protected override void UpdateTrap()
    {
        if ((darkActive) && (darkTime > 0))         // countdown until darkness deactivates after player collision
        {
            darkTime -= Time.deltaTime;
        }

        if ((isActivated) && (darkTime <= 0))           // deactivates trap
        {
            PlayTrapSound("darkTrapOff");

            currentState = closedState;
            currentState.EnterState(this);

            darkActive = false;
            isActivated = false;

            TriggerCall();

            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }

        if ((trapPhysicsUpdater.IsColliding()) && (isActivated) && (!darkActive))
        {
            TrapHitPlayer();
        }
    }

    // if trap activates on player activate darkness
    protected override void TrapHitPlayer()
    {
        if (CheckTrapDisabler())
        {
            isActivated = false;
            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }
        else
        {
            darkActive = true;

            currentState = openState;
            currentState.EnterState(this);

            TriggerCall();
        }
    }

    // sets trap parameters
    public override void SetParams(float dRadius, float dTime)
    {
        darkRadius = dRadius;
        darkTime = dTime;
    }

    // calls camera script to activate and deactivate darkness
    protected override void TriggerCall()
    {
        PlayTrapSound("darkTrapTrigger");

        if(ObjectManager.Instance != null)
        {
            ObjectManager.Instance.TurnLightsOff(darkActive);
        }
        else
        {
            Debug.Log("Object Manager not found");
        }
    }

    // resets trap to given parameters
    public override void ResetTrap()
    {
        darkActive = false;
        isActivated = true;

        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // return whether the trap is sending out darkness command
    public override bool CheckTrapStatus()
    {
        return darkActive;
    }
}