/*
 * ItemTrap.cs
 * Andrew VanCorbach
 * The class for item traps
 * When the player enters, a call is made to drop all items from the player inventory
 * It has a custom constructor, and override methods for the Trap and ActiveTrap superclass
 */

using UnityEngine;

public class ItemTrap : ActiveTrap
{
    private float scatterRadius;                    // doesn't do anything right now
    private float itemTime;                         // time until trap deactivation
    private bool itemsDropped;                      // bool if items have been dropped
    private GameObject player;                      // player gameobject for getting inventory
    private PlayerItemPickupScript inv;             // script to drop items from player inventory

    // class constructor
    public ItemTrap(float sRadius, float iTime, float xPos, float yPos)
    {
        // create trap object and assign componenets
        trap = new GameObject();
        collider = trap.AddComponent<BoxCollider2D>();
        spriteRenderer = trap.AddComponent<SpriteRenderer>();
        anim = trap.AddComponent<Animator>();
        trapPhysicsUpdater = trap.AddComponent<TrapPhysicsUpdater>();
        player = GameObject.FindWithTag("Player");
        inv = player.GetComponent<PlayerItemPickupScript>();

        // get animations for open and closed
        openAnim = Resources.Load<RuntimeAnimatorController>("Item/ItemTrapOpen");
        closedAnim = Resources.Load<RuntimeAnimatorController>("Item/ItemTrapClosed");

        // modify components as needed
        trap.tag = "Trap";
        trap.name = "itemTrap";
        collider.isTrigger = true;
        collider.size = new Vector2(3f, 3f);
        collider.offset = new Vector2(0f, 0f);
        spriteRenderer.sortingLayerName = "Traps";
        Open = openAnim;
        Closed = closedAnim;
        trap.transform.position = new Vector2(xPos, yPos);

        // set trap variables
        itemsDropped = false;
        SetParams(sRadius, iTime);
        updateFunc = () => UpdateTrap();
        currentState = closedState;
        currentState.EnterState(this);

        // add update function callback
        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // update trap every frame
    protected override void UpdateTrap()
    {
        if ((itemsDropped) && (itemTime > 0))         // countdown until items drop after player collision
        {
            itemTime -= Time.deltaTime;
        }

        if ((isActivated) && (itemTime <= 0))       // deactivates trap
        {
            currentState = closedState;
            currentState.EnterState(this);

            itemsDropped = false;
            isActivated = false;

            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }

        if ((trapPhysicsUpdater.IsColliding()) && (isActivated) && (!itemsDropped))
        {
            TrapHitPlayer();
        }
    }

    // if trap activates on player activate darkness
    protected override void TrapHitPlayer()
    {
        if (CheckTrapDisabler())
        {
            isActivated = false;
            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }
        else
        {
            itemsDropped = true;

            currentState = openState;
            currentState.EnterState(this);

            TriggerCall();
        }
    }

    // sets trap parameters
    public override void SetParams(float sRadius, float iTime)
    {
        scatterRadius = sRadius;
        itemTime = iTime;
    }

    // calls camera script to activate and deactivate darkness
    protected override void TriggerCall()
    {
        PlayTrapSound("itemTrapTrigger");

        if(inv != null)
        {
            inv.dropAllItemsAndRemoveFromInventory();
        }
        else
        {
            Debug.Log("Inventory not found");
        }
    }

    // resets trap to given parameters
    public override void ResetTrap()
    {
        itemsDropped = false;
        isActivated = true;

        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // return whether the trap is sending out darkness command
    public override bool CheckTrapStatus()
    {
        return itemsDropped;
    }
}