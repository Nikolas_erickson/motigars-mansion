/*
 * TrapUpdater.cs
 * Andrew VanCorbach
 * This class is a lazy external updater for all traps
 * It uses a singleton pattern to ensure that only one TrapUpdater exists and is accessible by all traps
 */

using System;
using UnityEngine;

public class TrapUpdater : MonoBehaviour
{
    private static TrapUpdater instance;            // unique instance of the TrapUpdater
    private Action updateCallback;                  // encapsulated method to callback
    TrapUpdater() { }                               // private constructor

    // updates requested methods every frame
    private void Update()
    {
        if(instance.updateCallback != null)
        {
            updateCallback();
        }
    }

    // adds a method to the update list
    public static void AddUpdateCallback(Action updateMethod)
    {
        if (instance == null)                       // adds instance of TrapUpdater if one doesn't already exist
        {
            instance = new GameObject("[TrapUpdater]").AddComponent<TrapUpdater>();
        }

        instance.updateCallback += updateMethod;
    }

    // removes a method from the update list
    public static void RemoveUpdateCallback(Action updateMethod)
    {
        if (instance == null)                       // checks if instance of TrapUpdater exists
        {
            Debug.Log("Updater instance does not exist");
            return;
        }

        Debug.Log("Removed Update Callback");
        // probably should add a check to make sure that it doesn't unsub something twice
        instance.updateCallback -= updateMethod;
    }
}