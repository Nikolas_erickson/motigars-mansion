using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapOpenState : TrapState
{
    public override void EnterState(Trap trap)
    {
        trap.ChangeOpenState();
    }
}
