/*
 * TrapFactory.cs
 * Andrew VanCorbach
 * The factory pattern implementation for trap construction
 * It takes the type of trap and any required values and creates a corresponding trap
 */

using UnityEngine;

public static class TrapFactory
{
    public static Trap CreateTrap(string type, float time1, float time2, float xPos, float yPos)
    {
        switch (type)
        {
            case "Spike":
                return new SpikeTrap(time1, time2, xPos, yPos);

            case "Dark":
                return new DarkTrap(time1, time2, xPos, yPos);

            case "Alert":
                return new AlertTrap(time1, time2, xPos, yPos);

            case "Item":
                return new ItemTrap(time1, time2, xPos, yPos);

            default:
                Debug.Log("TrapFactory: Invalid trap type");
                return null;
        }
    }
}
