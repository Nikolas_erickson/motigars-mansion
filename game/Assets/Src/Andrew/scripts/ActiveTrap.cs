/*
 * ActiveTrap.cs
 * Andrew VanCorbach
 * The superclass for all active traps
 * It has any variables and methods common to all active traps
 */

using UnityEngine;

public class ActiveTrap : Trap
{
    protected bool isActivated = true;      // bool for if a trap can be activated by the player

    // method for returning if the trap is active or not
    public bool IsTrapActive()
    {
        return isActivated;
    }

    // virtual
    protected virtual void TriggerCall()
    {
        Debug.Log("Trigger called");
    }

    protected bool CheckTrapDisabler()
    {
        Inventory inv = Inventory.getInstance();
        if (inv.removeOneTrapDisablerItem())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Deletes Trap
    public override void DeleteTrap()
    {
        Debug.Log("Delete Trap");
        TrapUpdater.RemoveUpdateCallback(updateFunc);
        isActivated = false;
        UnityEngine.Object.Destroy(trap);
    }
}
