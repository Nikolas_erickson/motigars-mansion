/*
 * Trap.cs
 * Andrew VanCorbach
 * The superclass for all traps
 * It has any variables and methods common to all traps
 */

using System;
using UnityEngine;

public abstract class Trap
{   // probably eventually add a trapID field here that corresponds to the trapID dictionary
    protected GameObject trap;                                          // GameObject for trap components to attach to
    protected TrapPhysicsUpdater trapPhysicsUpdater;                    // Physics Updater script for trigger detection
    protected RuntimeAnimatorController openAnim;                       // animator controller for open state
    protected RuntimeAnimatorController closedAnim;                     // animatior controller for closed state
    protected BoxCollider2D collider;                                   // collider for trap
    protected SpriteRenderer spriteRenderer;                            // sprite renderer for trap
    protected Action updateFunc;                                        // Action for adding and removing update callbacks
    protected TrapState currentState;                                   // current state of the trap
    protected TrapOpenState openState = new TrapOpenState();            // create new open state for trap
    protected TrapClosedState closedState = new TrapClosedState();      // create new closed state for trap
    protected RuntimeAnimatorController Open;                           // runtime animation controller for open state
    protected RuntimeAnimatorController Closed;                         // runtime animation controller for closed state
    protected Animator anim;                                            // animator for trap

    // trap destructor
    ~Trap()
    {
        Debug.Log("Trap Destroyed");
    }

    // change trap animation state to open
    public void ChangeOpenState()
    {
        anim.runtimeAnimatorController = Open as RuntimeAnimatorController;
    }

    // change trap animation state to closed
    public void ChangeClosedState()
    {
        anim.runtimeAnimatorController = Closed as RuntimeAnimatorController;
    }

    // update trap every frame
    protected virtual void UpdateTrap()
    {
        Debug.Log("Trap Update");
    }

    // if trap activates on player
    protected virtual void TrapHitPlayer()
    {
        Debug.Log("Trap Triggered");
    }

    // return the trap parameter
    public virtual bool CheckTrapStatus()
    {
        return false;
    }

    // set trap parameters
    public virtual void SetParams(float x, float y)
    {
        Debug.Log("Trap Params Changed");
    }

    // resets trap
    public virtual void ResetTrap()
    {
        Debug.Log("Reset Trap");
    }

    // returns the GameObject associated with the Trap instance
    public GameObject GetTrapGameObject()
    {
        return trap;
    }

    // checks that the audio manager is valid and then plays a sound
    protected void PlayTrapSound(string sound)
    {
        if(AudioManager.instance != null)
        {
            AudioManager.instance.Play(sound, trap.transform.position);
        }
        else
        {
            Debug.Log("Audio Manager not found");
        }
    }

    // deletes trap
    public virtual void DeleteTrap()
    {
        Debug.Log("Delete Attached GameObject");
        TrapUpdater.RemoveUpdateCallback(updateFunc);
        UnityEngine.Object.Destroy(trap);
    }
}
