/*
 * TrapManager.cs
 * Andrew VanCorbach
 * This class manages all traps
 * It uses a singleton pattern to ensure that only one TrapUpdater exists
 * It also contains the interface that are used to get, modify, create, and destroy traps
 */

using System.Collections.Generic;
using UnityEngine;

// interface for generating traps, 
public interface ITrapManager
{
    void CreateTrapManager();
    void DeleteTrapManager();
    int CreateTrap(string type, float time1, float time2, float xPos, float yPos);
    int CreateRandomTrap(float xPos, float yPos);
    int CreateRandomPassableTrap(float xPos, float yPos);
    Trap AccessTrap(int trapID);
    GameObject GetTrapGameObject(int trapID);
    bool GetTrapStatus(int trapID);
    bool ResetTrap(int trapID);
    bool ModifyTrapParams(int TrapID, float time1, float time2);
    void DeleteTrap(int trapID);
}

public class TrapManager
{
    private static int trapID = 0;                                                              // static int for trap dictionary key
    private static Dictionary<int, Trap> trapDictionary = new Dictionary<int, Trap>();          // dictionary for storing traps
    private TrapManager() { }                                                                   // private constructor

    public static TrapManager Instance { get; private set; }                                    // unique instance of trap manager

    // creates the trap manager if one exists
    public static void CreateTrapManager()
    {
        if (Instance == null)
        {
            Instance = new TrapManager();
            trapID = 0;
            trapDictionary.Clear();
        }
        else
        {
            Debug.Log("An instance of Trap Manager already exists");
        }
    }

    // Deletes the trap Manager if one exists
    public static void DeleteTrapManager()
    {
        if (Instance != null)
        {
            Instance = null;
        }
        else
        {
            Debug.Log("An instance of Trap Manager does not exist");
        }
    }

    // generate a specific trap at a position and return the ID number of the trap
    public int CreateTrap(string type, float time1, float time2, float xPos, float yPos)
    {
        trapID++;
        Trap temp = TrapFactory.CreateTrap(type, time1, time2, xPos, yPos);
        trapDictionary.Add(trapID, temp);
        return trapID;
    }

    // creates a completely random trap and return the ID number of the trap
    public int CreateRandomTrap(float xPos, float yPos)
    {
        trapID++;
        Trap temp = TrapFactoryRandom.CreateTrap(xPos, yPos);

        if(temp != null)
        {
            trapDictionary.Add(trapID, temp);
            return trapID;
        }
        else
        {
            return -1;
        }
    }

    // creates a random trap that the player can pass under any circumstance and return the ID number of the trap
    public int CreateRandomPassableTrap(float xPos, float yPos)
    {
        trapID++;
        Trap temp = TrapFactoryRandomPassable.CreateTrap(xPos, yPos);

        if (temp != null)
        {
            trapDictionary.Add(trapID, temp);
            return trapID;
        }
        else
        {
            return -1;
        }
    }

    // returns the trap specified by the trapID
    public Trap AccessTrap(int trapID)
    {
        if (trapDictionary.TryGetValue(trapID, out Trap temp))
        {
            return temp;
        }
        else
        {
            Debug.Log("Trap with trapID " + trapID + " not found");
            return null;
        }
    }

    // returns the GameObject of the trap specified by the trapID
    public GameObject GetTrapGameObject(int trapID)
    {
        if (trapDictionary.TryGetValue(trapID, out Trap temp))
        {
            return temp.GetTrapGameObject();
        }
        else
        {
            Debug.Log("Trap with trapID " + trapID + " not found");
            return null;
        }
    }

    // returns the status of the rap associated with the given trapID
    public bool GetTrapStatus(int trID)
    {
        if (trapDictionary.TryGetValue(trID, out Trap temp))
        {
            return temp.CheckTrapStatus();
        }
        else
        {
            Debug.Log("Trap with trapID " + trID + " not found");
            return false;
        }
    }

    // resets the trap with the given trapID
    public int ResetTrap(int resetID)
    {
        if (trapDictionary.TryGetValue(resetID, out Trap temp))
        {
            temp.ResetTrap();
            return 0;
        }
        else
        {
            Debug.Log("Trap with trapID " + resetID + " not found");
            return -1;
        }
    }

    // resets the trap with the given trapID
    public int ModifyTrapParams(int modID, float time1, float time2)
    {
        if (trapDictionary.TryGetValue(modID, out Trap temp))
        {
            temp.SetParams(time1, time2);
            return 0;
        }
        else
        {
            Debug.Log("Trap with trapID " + modID + " not found");
            return -1;
        }
    }

    // deletes a specific trap by trapID
    public int DeleteTrap(int deleteID)
    {
        if (trapDictionary.ContainsKey(deleteID) )
        {
            if (trapDictionary[deleteID] == null)
            {
                Debug.Log("Trap with trapID " + deleteID + " not in dictionary");
                trapDictionary.Remove(deleteID);
                return -2;
            }
            else
            {
                Trap toDelete = trapDictionary[deleteID];
                toDelete.DeleteTrap();
                trapDictionary.Remove(deleteID);
                return 0;
            }
        }
        else
        {
            Debug.Log("Trap with trapID " + deleteID + " not found");
            return -1;
        }
    }
}