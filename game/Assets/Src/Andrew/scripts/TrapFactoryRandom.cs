/*
 * TrapFactoryRandom.cs
 * Andrew VanCorbach
 * The factory pattern implementation for trap construction
 * It takes the position of the trap and creates one of any trap
 */

using UnityEngine;

public static class TrapFactoryRandom
{
    public static Trap CreateTrap(float xPos, float yPos)
    {
        int randomTemp = Random.Range(0, 4);
        float time1;
        float time2;

        switch (randomTemp)
        {
            case 0:
                time1 = Random.Range(5f, 10f);
                time2 = Random.Range(1f, 3f);
                return new SpikeTrap(time1, time2, xPos, yPos);

            case 1:
                time1 = Random.Range(5f, 30f);
                time2 = Random.Range(5f, 10f);
                return new DarkTrap(time1, time2, xPos, yPos);

            case 2:
                time1 = Random.Range(5f, 20f);
                time2 = Random.Range(5f, 10f);
                return new AlertTrap(time1, time2, xPos, yPos);

            case 3:
                time1 = Random.Range(1f, 2f);
                time2 = Random.Range(2f, 5f);
                return new ItemTrap(time1, time2, xPos, yPos);

            default:
                Debug.Log("TrapFactory: Invalid trap type");
                return null;
        }
    }
}
