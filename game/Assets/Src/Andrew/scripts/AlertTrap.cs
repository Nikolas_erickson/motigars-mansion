/*
 * AlertTrap.cs
 * Andrew VanCorbach
 * The class for alert traps
 * When the player enters, a countdown is started, after which an enemy is alerted to the player's position
 * It has a custom constructor, and override methods for the Trap and ActiveTrap superclass
 */

using UnityEngine;

public class AlertTrap : ActiveTrap
{
    private float alertTime;            // float for the time that the monsters will be chasing the player
    private float alertDelay;           // float for the amount of time between the player stepping on the trap and the alert being sent out
    private bool alertActive;           // bool for if the alert is is going to be sent out at the end of the countdown

    // class constructor
    public AlertTrap(float tTime, float dTime, float xPos, float yPos)
    {
        // create trap object and assign componenets
        trap = new GameObject();
        collider = trap.AddComponent<BoxCollider2D>();
        spriteRenderer = trap.AddComponent<SpriteRenderer>();
        anim = trap.AddComponent<Animator>();
        trapPhysicsUpdater = trap.AddComponent<TrapPhysicsUpdater>();

        // get animations for open and closed
        openAnim = Resources.Load<RuntimeAnimatorController>("Alert/AlertTrapOpen");
        closedAnim = Resources.Load<RuntimeAnimatorController>("Alert/AlertTrapClosed");

        // ensure that enemy interface is created
        if (EnemyInterface.Instance == null)
        {
            EnemyInterface.CreateEnemyInterface();
        }

        // modify components as needed
        trap.tag = "Trap";
        trap.name = "alertTrap";
        collider.isTrigger = true;
        collider.size = new Vector2(3f, 3f);
        collider.offset = new Vector2(0f, 0f);
        spriteRenderer.sortingLayerName = "Traps";
        Open = openAnim;
        Closed = closedAnim;
        trap.transform.position = new Vector2(xPos, yPos);

        // set trap variables
        alertActive = false;
        SetParams(tTime, dTime);
        updateFunc = () => UpdateTrap();
        currentState = closedState;
        currentState.EnterState(this);

        // add update function callback
        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // update trap every frame
    protected override void UpdateTrap()
    {
        if ( (alertActive) && ((alertDelay > 0) || (alertTime > 0)) )
        {
            if(alertDelay <= 0f)
            {
                alertTime -= Time.deltaTime;                // second countdown for time until alert is deactivated
            }
            else
            {
                alertDelay -= Time.deltaTime;               // first countdown for time until alert is triggered
            }
        }

        if( (isActivated) && (alertDelay <= 0) && (alertTime >= 0) )            // will activate when the delay is over
        {
            TriggerCall();                                                      // first trigger call to activate monster alert
            isActivated = false;
        }

        if ( (alertActive) && (alertDelay <= 0) && (alertTime <= 0) )           // will activate when the timer is over
        {
            currentState = closedState;
            currentState.EnterState(this);

            alertActive = false;

            TriggerCall();                                                      // second trigger call to deactivate monster alert

            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }

        if ((trapPhysicsUpdater.IsColliding()) && (isActivated) && !(alertActive))
        {
            TrapHitPlayer();
        }
    }

    // if trap activates on player activate darkness
    protected override void TrapHitPlayer()
    {
        if (CheckTrapDisabler())
        {
            isActivated = false;
            TrapUpdater.RemoveUpdateCallback(updateFunc);
        }
        else
        {
            alertActive = true;                                             // sets alertActive to true so the countdown to the alert being sent starts

            currentState = openState;
            currentState.EnterState(this);
        }
    }

    // sets trap parameters
    public override void SetParams(float aTime, float aDelay)
    {
        alertTime = aTime;
        alertDelay = aDelay;
    }

    // calls camera script to activate and deactivate darkness
    protected override void TriggerCall()
    {
        PlayTrapSound("alertTrapTrigger");

        if(EnemyInterface.Instance != null)
        {
            Debug.Log("Enemy alert message sent: " + alertActive);
            EnemyInterface.Instance.TriggerForcedDetect(alertActive);
        }
        else
        {
            Debug.Log("Enemy Interface not found");
        }
    }

    // resets trap to given parameters
    public override void ResetTrap()
    {
        currentState = closedState;
        currentState.EnterState(this);

        alertActive = false;
        isActivated = true;

        TrapUpdater.AddUpdateCallback(updateFunc);
    }

    // return whether the trap is in alert countdown
    public override bool CheckTrapStatus()
    {
        return alertActive;
    }
}
