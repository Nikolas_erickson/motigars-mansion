using UnityEngine;

public static class ObjectFactory
{
    public static Object createObject(string objectName, Vector2 position, Quaternion rotation, int objID)
    {
        switch(objectName)
        {
            case "Crate":
                return new Crate(position, rotation, objID);

            case "Table":
                return new Table(position, rotation, objID);

            default:
                Debug.Log("Invalid Object: Couldn't find a matching object name.");
                return null;
        }
    }

    public static Object createObject(string objectName)
    {
        switch (objectName)
        {
            case "Crate":
                return new Crate();

            case "Table":
                return new Table();

            default:
                Debug.Log("Invalid Object: Couldn't find a matching object name.");
                return null;
        }
    }
}
