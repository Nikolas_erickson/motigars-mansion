using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Object
{
    //Replace this with the name of the prefab.
    private string prefabName = "";

    public Door()
    {
        envObject = GameObject.Instantiate(Resources.Load(prefabName, typeof(GameObject))) as GameObject;
    }

    public Door(Vector2 position, Quaternion rotation, int objID)
    {
        envObject = GameObject.Instantiate(Resources.Load(prefabName, typeof(GameObject)), position, rotation) as GameObject;
        objInfoHolder = envObject.AddComponent<ObjectInfoHolder>();

        objInfoHolder.setIDHolder(objID);
        objInfoHolder.setName("Door");

        GameObject.Instantiate(envObject, position, rotation);
    }

    public override void onUse(Vector2 position)
    {
        throw new System.NotImplementedException();
    }
}
