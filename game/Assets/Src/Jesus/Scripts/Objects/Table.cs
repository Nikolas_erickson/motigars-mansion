using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : Object
{
    public Table()
    {
        envObject = GameObject.Instantiate(Resources.Load("TablePrefab", typeof(GameObject))) as GameObject;
    }

    public Table(Vector2 position, Quaternion rotation, int objID)
    {
        envObject = GameObject.Instantiate(Resources.Load("TablePrefab", typeof(GameObject)), position, rotation) as GameObject;
        objInfoHolder = envObject.AddComponent<ObjectInfoHolder>();

        objInfoHolder.setIDHolder(objID);
        objInfoHolder.setName("Table");

        //GameObject.Instantiate(envObject, position, rotation);
    }

    public override void onUse(Vector2 position)
    {
        Debug.Log("This is a table");
    }
}
