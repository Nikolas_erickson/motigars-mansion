using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfoHolder : MonoBehaviour
{
    private string Name;
    private int IDHolder;

    public void setName(string objName)
    {
        Name = objName;
    }

    public string getName()
    {
        return Name;
    }

    public void setIDHolder(int objID)
    {
        IDHolder = objID;
    }

    public int getIDHolder()
    {
        return IDHolder;
    }
}
