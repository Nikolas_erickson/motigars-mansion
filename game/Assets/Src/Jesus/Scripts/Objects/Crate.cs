using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : Object
{
    public static bool ifUsed = false;
    public static bool keyCrateFound = false;

    private Sprite crateOpened;

    public Crate()
    {
        envObject = GameObject.Instantiate(Resources.Load("CratePrefab", typeof(GameObject))) as GameObject;
    }

    public Crate(Vector2 position, Quaternion rotation, int objID)
    {
        envObject = GameObject.Instantiate(Resources.Load("CratePrefab", typeof(GameObject)), position, rotation) as GameObject;
        spriteRenderer = envObject.GetComponent<SpriteRenderer>();
        objInfoHolder = envObject.AddComponent<ObjectInfoHolder>();

        hasBeenUsed = ifUsed;
        crateOpened = Resources.Load<Sprite>("crateOpened");

        objInfoHolder.setIDHolder(objID);
        objInfoHolder.setName("Crate");

        //Debug.Log("Random Crate: " + ObjectManager.Instance.getRandomCrate());
    }

    /*
     * The onUse() function used to interact with the crates. This is to allow the player to open the crates
     * and see what is inside. Eventually, it would randomly assign one of the 8 crates to contain the key(s)
     * and allow it so that only that one specified crate will drop it.
     */
    public override void onUse(Vector2 position)
    {
        Debug.Log("This is the crate.");

        Debug.Log(hasBeenUsed);

        if (!keyCrateFound && objInfoHolder.getIDHolder() == ObjectManager.Instance.getRandomCrate())
        {
            Debug.Log("This ran");
            GameObject key = ItemGenerator.getInstance().generateItem(ItemType.Key);
            key.transform.position = position;

            keyCrateFound = true;
        }

        if (!hasBeenUsed)
        {
            GameObject.FindObjectOfType<AudioManager>().Play("chestOpen", envObject.transform.position);
            Debug.Log(objInfoHolder.getIDHolder());
            spriteRenderer.sprite = crateOpened;

            hasBeenUsed = true;
        }
    }

}
