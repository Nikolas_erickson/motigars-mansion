using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Object
{
    public GameObject envObject;
    public SpriteRenderer spriteRenderer;
    public ObjectInfoHolder objInfoHolder;

    public bool hasBeenUsed;

    public virtual void onUse(Vector2 position)
    {
        Debug.Log("This is an object that has a onUse function");
    }


    public void interactWithObject(Vector2 position)
    {
        onUse(position);
    }

    public GameObject getGameObject()
    {
        return envObject;
    }
}
