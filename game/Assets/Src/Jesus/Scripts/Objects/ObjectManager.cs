using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public interface IObjectManager
{
    void createManager();
    void deleteManager();
    void AwakeLogic();
    void UpdateLogic();
    void crateObject(string objectName, Vector2 position, Quaternion rotation);
    Object createObject(string objectName);
    int destroyObject(int objectID);
    int updateObjectPosition(int objectID, Vector2 newPosition, Quaternion newRotation);
    void clearAllObjects();
    void destroyAllCertainObjects(string objName);
    GameObject getGameObject(int objectID);
    int findObjectNearby();
    Dictionary<int, Object> getObjectDictionary();
    int getTotalObjects();
    int getRandomCrate();
}

public class ObjectManager
{
    public static ObjectManager Instance { get; private set; }
    private static int objID = 0;
    private static Dictionary<int, Object> ObjectDictionary = new Dictionary<int, Object>();

    private static int randomCrate;

    /*
     * Call this function to create an ObjectManager in order to start creating objects and this way you don't have to
     * make a GameObject for this in every scene.
     */
    public static void createManager()
    {
        if (Instance == null)
        {
            Instance = new ObjectManager();
            ObjectDictionary.Clear();
        } else
        {
            Debug.Log("An Instance of ObjectManager already exists.");
        }
    }

    /*
     * Deletes the ObjectManager when called.
     * 
     */
    public static void deleteManager()
    {
        if (Instance == null)
        {
            Debug.Log("An Instance of ObjectManager doesn't exist.");
        } else
        {
            Instance = null;
        }
    }

    /*
     * 
     * 
     */
    public void AwakeLogic()
    {
        randomCrate = Random.Range(2, 8);
    }

    /*
     * Deal with getting receiving the input to try and find an object nearby and if it does find an object,
     * it returns that object and calls the interactWithObject().
     * This is called in the FloorManager script in order to correctly allow the player to interact with the objects
     * since this script does not reference MonoBehaviour
     */
    public void UpdateLogic()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            int objNearbyInt = findObjectNearby(Player.instance.transform.position);

            if (objNearbyInt != -1)
            {
                ObjectDictionary.TryGetValue(findObjectNearby(Player.instance.transform.position), out Object temp);
                temp.interactWithObject(Player.instance.getPlayerPos());
            } else
            {
                Debug.Log("Could not find an object.");
            }
        }
    }

    /*
     * Create object based on the Name, position, and rotation.
     * This also adds that specific object to the dictionary to keep track of all the different objects.
     */
    public int createObject(string objectName, Vector2 position, Quaternion rotation)
    {
        Debug.Log("ObjectManager: Creating Object");
        objID++;
        Object tempObj = ObjectFactory.createObject(objectName, position, rotation, objID);
        ObjectDictionary.Add(objID, tempObj);
        return objID;
    }

    /*
     * Create object based on just the Name.
     * This is used only for the tests as that is what they were based on originally.
     */
    public Object createObject(string objectName)
    {
        //Debug.Log("ObjectManager: Creating Object with no Location");
        objID++;
        Object tempObj = ObjectFactory.createObject(objectName);
        ObjectDictionary.Add(objID, tempObj);
        return tempObj;
    }

    /*
     * This deletes the specified item in the dictionary when given the an objectID.
     * Returns 1 if successful and returns -1 if Object ID was not found in the Object Dictionary.
     */
    public int destroyObject(int objectID)
    {
        if(ObjectDictionary.ContainsKey(objectID))
        {
            ObjectDictionary.TryGetValue(objectID, out Object tempObj);

            ObjectDictionary.Remove(objectID);

            GameObject.DestroyImmediate(tempObj.getGameObject());

            return 1;
        } else
        {
            Debug.Log("Object ID does not exist.");
            return -1;
        }
    }

    /*
     * If you only have the objectID adn not anything else, you can use the updateObjectPosition() to
     * change either the position of the object or the rotation as well. 
     */
    public int updateObjectPosition(int objectID, Vector2 newPosition, Quaternion newRotation)
    {
        if (ObjectDictionary.ContainsKey(objectID))
        {
            ObjectDictionary[objectID].envObject.transform.position = newPosition;
            ObjectDictionary[objectID].envObject.transform.rotation = newRotation;

            return 1;
        } else
        {
            Debug.Log("Object ID does not exist.");
            return -1;
        }
    }

    /*
     * Clear all objects in the given dictionary.
     * This functions gets the objID for each object in the dictionary and then proceeds with deleting it.
     */
    public void clearAllObjects()
    {
        List<int> keysToDelete = new List<int>(ObjectDictionary.Keys);
        foreach (int objID in keysToDelete)
        {
            destroyObject(objID);
        }

        ObjectDictionary.Clear();
    }

    /*
     * 
     * 
     */
    public void destroyAllCertainObjects(string objName)
    {
        List<int> keysToDelete = new List<int>(ObjectDictionary.Keys);
        foreach(int objID in keysToDelete)
        {
            ObjectDictionary.TryGetValue(objID, out Object tempObj);
            ObjectInfoHolder objectInfoHolder = tempObj.envObject.GetComponent<ObjectInfoHolder>();

            if (objectInfoHolder.getName() == objName)
            {
                destroyObject(objID);
            }
        }
    }

    /*
     * If you only have the objectID and not the actual object itself, you can get the gameObject by using this 
     * function to interact with the object.
     */
    public GameObject getGameObject(int objectID)
    {
        if (ObjectDictionary.ContainsKey(objectID))
        {
            ObjectDictionary.TryGetValue(objectID, out Object tempObj);
            GameObject tempGameObj = tempObj.envObject;

            if (tempGameObj == null)
            {
                return null;
            }
            else
            {
                return tempGameObj;
            }
        } else
        {
            return null;
        }
    }

    /*
     * This is used to check all around the player and see if it finds any objects,
     * if it does, then it returns the objectID.
     */
    public int findObjectNearby(Vector2 position)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(position, 0.8f);
        Debug.Log("Looking for objects nearby.");

        foreach (Collider2D collider in colliders)
        {
            Debug.Log("Looking for tag Object.");
            if (collider.CompareTag("Object"))
            {
                Debug.Log("Object found");
                GameObject gameObject = collider.gameObject;
                int temp = gameObject.GetComponent<ObjectInfoHolder>().getIDHolder();

                return temp;
            } else
            {
                Debug.Log("Could not find object");
            }
        }

        return -1;
    }

    /*
     * Since ObjectManager is a singleton, I know that this will for sure always return the same
     * number everytime and allow the Crate object to always check the correct value when checking
     * whether the Crate has the key. 
     */
    public int getRandomCrate()
    {
        return randomCrate;
    }

    /*
     * This returns the ObjectDictionary so that we can interact with the dictionary outside of this script.
     * This has mainly been used for the EditMode tests.
     */
    public Dictionary<int, Object> getObjectDictionary()
    {
        return ObjectDictionary;
    }

    /*
     * 
     * 
     */
    public int getTotalObjects()
    {
        return ObjectDictionary.Count;
    }

    // turn all lights tagged with "Torch" to the specified bool
    public void TurnLightsOff(bool darkOn)
    {
        GameObject[] torchList = GameObject.FindGameObjectsWithTag("Torch");
        Light2D[] playerLightList = Player.instance.gameObject.GetComponentsInChildren<Light2D>(true);

        foreach(Light2D l2d in playerLightList)
        {
            if(l2d.tag == "Torch")
            {
                if(darkOn)
                {
                    l2d.intensity = 0f;
                }
                else
                {
                    l2d.intensity = 1f;
                }
            }
        }

        if (darkOn)
        {
            foreach (GameObject torch in torchList)
            {
                torch.GetComponent<Light2D>().intensity = 0f;
            }
        }
        else
        {
            foreach (GameObject torch in torchList)
            {
                torch.GetComponent<Light2D>().intensity = 1f;
            }
        }
    }
}
