using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FloorGeneration : MonoBehaviour
{
    [SerializeField]
    protected GameObject[] floorObjects;
    protected static bool hasEntered;

    /* Does the job of "spawning" objects
     * It doesn't really technically spawn them as those objects are already in the Scene
     * but for right now lets just pretend they do.
     */
    protected abstract void spawnObjects();

    /*
     * Does the Job of "spawning" the enemies
     * The enemies may or may not already be in the scene depending on how the
     * enemies pathfinding is dealt with but eventually we can have them pre spawned or 
     * already spawned in somewhere off the visuals of the player.
     */
    protected abstract void spawnEnemies();

    /*
     * 
     * 
     */
    protected abstract void spawnTraps();

    /*
     * This will be use to setup the floor at very start of each level. This should only
     * be used once and then going forward the resetFloor() function should be used from 
     * now on to adjust the floor as necessary.
     */
    public abstract void setupFloor();

    /*
     * This will deal with reseting the floor when necessary. This could be used to reset
     * floor once the player has completed it and has decided to retrace their steps. We
     * could use it to reset the enemies and things like that depending on the difficulty
     * of the game.
     */
    public abstract void resetFloor();
}
