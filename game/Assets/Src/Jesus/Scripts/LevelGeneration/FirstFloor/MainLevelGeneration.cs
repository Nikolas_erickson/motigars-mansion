using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLevelGeneration : FloorGeneration
{
    [SerializeField] private Vector2[] crateLocations = new Vector2[6];
    [SerializeField] private Vector2[] tableLocations = new Vector2[6];

    [SerializeField] private GameObject[] enemyTypes;
    [SerializeField] private Vector3[] enemyLocations;
    [SerializeField] Pathfinder mainPath;
    [SerializeField] Pathfinder roomsPath;


    public override void resetFloor()
    {
        
    }

    public override void setupFloor()
    {
        // Managers
        TrapManager.CreateTrapManager();
        ObjectManager.createManager();

        // Spawning of individual parts.

        spawnObjects();
        spawnTraps();
        spawnEnemies();

        ItemGenerator.getInstance().spawnItems(2);
    }

    protected override void spawnEnemies()
    {
        bool basicSpawned = false;
        int enemyType = 0;
        GameObject tempEnemy;
        PatternedEnemy enemyAccess;
        EnemyInterface.CreateEnemyInterface();

        //Debug.Log(enemyLocations.Length);
        for (int i = 0; i < enemyLocations.Length; i++)
        {
            
            if (basicSpawned == false && (i == enemyLocations.Length - 1))
            {
                enemyType = 0;
                tempEnemy = Instantiate(enemyTypes[enemyType], enemyLocations[i], Quaternion.identity);
            }
            else
            {
                enemyType = (int)UnityEngine.Random.Range(0, enemyTypes.Length);
                tempEnemy = Instantiate(enemyTypes[enemyType], enemyLocations[i], Quaternion.identity);
                //tempEnemy.GetComponent<PatternedEnemy>().pathPrefab = enemyTLRooms;
            }
            enemyAccess = tempEnemy.GetComponent<PatternedEnemy>();
            if (enemyTypes[enemyType].name == "EnemyBasic")
            {
                if (basicSpawned == false)
                {
                    enemyAccess.pathfinder = mainPath;
                }
                else
                {
                    enemyAccess.pathfinder = roomsPath;
                }
                basicSpawned = true;
            }
            else
            {
                enemyAccess.pathfinder = roomsPath;
            }
            enemyAccess.levelSound = "GroundFloor";
        } 
        roomsPath.GenerateTiles();
        mainPath.GenerateTiles();
    }

    protected override void spawnObjects()
    {
        Debug.Log("Spawning Objects");

        int numObjects = 13;
        int[] crateLocationsArray = new int[numObjects];
        bool found = false;

        ObjectManager objectManager = ObjectManager.Instance;

        // Initialize the int array.
        for (int i = 0; i < numObjects; i++)
        {
            crateLocationsArray[i] = -1;
        }

        //Spawn numObjects around the map while making sure that they do not repeat.
        for (int i = 0; i < numObjects; i++)
        {
            found = false;
            int randNum = Random.Range(0, crateLocations.Length);

            for (int j = 0; j < numObjects; j++)
            {
                if (crateLocationsArray[j] == randNum)
                {
                    found = true;
                    i--;
                    break;
                }
            }

            if (!found)
            {
                objectManager.createObject("Crate", crateLocations[randNum], Quaternion.identity /*Quaternion.Euler(Vector3.forward * 45)*/);
                crateLocationsArray[i] = randNum;
            }
        }

        for(int k = 0; k < 6; k++)
        {
            objectManager.createObject("Table", tableLocations[k], Quaternion.identity);
        }
    }

    protected override void spawnTraps()
    {
        TrapManager.Instance.CreateRandomTrap(-10.4f, 1.7f);
        TrapManager.Instance.CreateRandomTrap(-10.41f, -18.09f);
        TrapManager.Instance.CreateRandomTrap(4.53f, -28.45f);
        TrapManager.Instance.CreateRandomTrap(27.14f, -22.53f);
        TrapManager.Instance.CreateRandomTrap(10.32f, -7.94f);
    }
}
