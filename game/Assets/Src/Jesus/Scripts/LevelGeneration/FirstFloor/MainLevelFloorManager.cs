using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLevelFloorManager : MonoBehaviour
{
    public MainLevelGeneration mainLevelGen;

    public static MainLevelFloorManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
    }

    void Start()
    {
        mainLevelGen.setupFloor();
    }
}
