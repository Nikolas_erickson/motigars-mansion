using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    public BasementGeneration basementGen;

    public static FloorManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(this);
            return;
        }
    }
    void Start()
    {
        basementGen.setupFloor();

        ObjectManager.Instance.AwakeLogic();
    }

    private void Update()
    {
        ObjectManager.Instance.UpdateLogic();
    }
}
