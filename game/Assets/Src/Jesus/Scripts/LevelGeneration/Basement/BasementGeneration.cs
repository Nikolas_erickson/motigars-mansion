using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasementGeneration : FloorGeneration
{
    [SerializeField] private Vector2[] crateLocations = new Vector2[6];

    [SerializeField] private GameObject[] enemyTypes;
    [SerializeField] private Vector3[] enemyLocations;
    [SerializeField] Pathfinder mainPath;
    [SerializeField] Pathfinder roomsPath;

    public override void resetFloor()
    {

    }

    public override void setupFloor()
    {
        Application.targetFrameRate = 60;

        // Managers
        TrapManager.CreateTrapManager();
        ObjectManager.createManager();

        // Spawning of individual parts.

        spawnObjects();
        spawnTraps();
        spawnEnemies();

        ItemGenerator.getInstance().spawnItems(1);

    }

    protected override void spawnEnemies()
    {
        bool basicSpawned = false;
        int enemyType = 0;
        GameObject tempEnemy;
        PatternedEnemy enemyAccess;
        EnemyInterface.CreateEnemyInterface();

        //Debug.Log(enemyLocations.Length);
        for (int i = 0; i < enemyLocations.Length; i++)
        {
            
            if (basicSpawned == false && (i == enemyLocations.Length - 1))
            {
                enemyType = 0;
                tempEnemy = Instantiate(enemyTypes[enemyType], enemyLocations[i], Quaternion.identity);
            }
            else
            {
                enemyType = (int)UnityEngine.Random.Range(0, enemyTypes.Length);
                tempEnemy = Instantiate(enemyTypes[enemyType], enemyLocations[i], Quaternion.identity);
                //tempEnemy.GetComponent<PatternedEnemy>().pathPrefab = enemyTLRooms;
            }
            enemyAccess = tempEnemy.GetComponent<PatternedEnemy>();
            if (enemyTypes[enemyType].name == "EnemyBasic")
            {
                if (basicSpawned == false)
                {
                    enemyAccess.pathfinder = mainPath;
                }
                else
                {
                    enemyAccess.pathfinder = roomsPath;
                }
                basicSpawned = true;
            }
            else
            {
                enemyAccess.pathfinder = roomsPath;
            }
        } 
    }

    protected override void spawnTraps()
    {
        Debug.Log("Spawning Traps");
        TrapManager.Instance.CreateRandomTrap(4.3f, 23.7f);
        TrapManager.Instance.CreateRandomTrap(24.75f, -21.5f);
        TrapManager.Instance.CreateRandomTrap(-13.5f, -15f);
    }

    protected override void spawnObjects()
    {
        Debug.Log("Spawning Objects");

        int numObjects = 8;
        int[] crateLocationsArray = new int[numObjects];
        bool found = false;

        ObjectManager objectManager = ObjectManager.Instance;

        // Initialize the int array.
        for (int i = 0; i < numObjects; i++)
        {
            crateLocationsArray[i] = -1;
        }

        objectManager.createObject("Table", new Vector2(-11.25f, 18.65f), Quaternion.identity);

        //Spawn numObjects around the map while making sure that they do not repeat.
        for (int i = 0; i < numObjects; i++)
        {
            found = false;
            int randNum = Random.Range(0, crateLocations.Length);

            for (int j = 0; j < numObjects; j++)
            {
                if (crateLocationsArray[j] == randNum)
                {
                    found = true;
                    i--;
                    break;
                }
            }

            if (!found)
            {
                objectManager.createObject("Crate", crateLocations[randNum], Quaternion.identity /*Quaternion.Euler(Vector3.forward * 45)*/);
                crateLocationsArray[i] = randNum;
            }
        }
    }

}
