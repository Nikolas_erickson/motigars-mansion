/*
 * EnemySimplest.cs
 * Ian Finnigan
 * This script is for the simplest possible enemy
 * It is used for debug purposes
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EnemySimplest : MonoBehaviour
{
    //[SerializeField] Tilemap tilemap;
    [SerializeField] GameObject player;
    [SerializeField] Pathfinder pathfinder;
    public List<MyTile> path;
    UnityEngine.Vector3 oldPlayerPos;
    float tileMapScale = 1.5f;
    int i = 0;
    float speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp;
        if (((int)player.transform.position.x != (int)oldPlayerPos.x) || ((int)player.transform.position.y != (int)oldPlayerPos.y))    //has player moved?
        { 
            oldPlayerPos = player.transform.position;
            temp = new Vector3(transform.position.x, transform.position.y, 0); //Don't move while updating
            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * 0);//don't move while updating
            path = pathfinder.UpdatePath(player, gameObject); //update path
            if (path == null)
            {
                return;
            }

            i = (path.Count-2); //minus two skips current spot and next to smooth movement
        }
        if ((path != null) && (i >= 0)) //changed from >= to > to account for argument out of range error
        {
            temp = new Vector3((float)((float)(path[i].X+.5)*tileMapScale), (float)((float)(path[i].Y+.5)*tileMapScale), 0); //tell enemy where to move to, targetting next tile

            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * speed); //perform movement

            if ((transform.position.x == (float)((float)(path[i].X+.5)*tileMapScale)) && (transform.position.y == (float)((float)(path[i].Y+.5) * tileMapScale)))
            { //check whether or not we have completely reached the next tile
                if (i > 0)
                {
                    i--; //update the next tile
                }
                
            }
        } 
    }
}
