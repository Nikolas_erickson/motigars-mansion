/*
 * Pathfinder.cs
 * Ian Finnigan
 * This script implements convenient access to critical parts of the pathfinding algorithm
 * It is used for implementing enemy movement and connecting the tilemap game object to the algorithm.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class Pathfinder : MonoBehaviour
{
    [SerializeField] public Tilemap tilemap;
    [SerializeField] public Tilemap roadMap;
    [SerializeField] TileBase roadTile;
    [SerializeField] Vector3Int[,] spots;

    Astar astar;
    BoundsInt bounds;

    void Awake()
    {
        tilemap.CompressBounds();
        roadMap.CompressBounds();
        bounds = tilemap.cellBounds;

        GenerateTiles();
        astar = new Astar(spots, bounds.size.x, bounds.size.y);
    }

    //creates a refrenceable tile corresponding to every tile in the tilemap gameobject
    public void GenerateTiles()
    {
        spots = new Vector3Int[bounds.size.x, bounds.size.y];
        
        for (int x = bounds.xMin, i = 0; i < (bounds.size.x); x++, i++)
        {
            for (int y = bounds.yMin, j = 0; j < (bounds.size.y); y++, j++)
            {
                if (tilemap.HasTile(new Vector3Int(x, y, 0)))
                {
                    spots[i, j] = new Vector3Int(x, y, 0);
                }
                else 
                {
                    spots[i, j] = new Vector3Int(x, y, 1);
                }
            }
        }
    }

    //Simple code to modify a path
    public List<MyTile> UpdatePath(GameObject target, GameObject enemy)
    {
            List<MyTile> roadPath = new List<MyTile>();
            GenerateTiles();
            
            Vector2Int start = (Vector2Int)tilemap.WorldToCell(enemy.transform.position);
            Vector2Int end = (Vector2Int)tilemap.WorldToCell(target.transform.position);

            if ((roadPath != null) && (roadPath.Count > 0))
            {
                roadPath.Clear();
            }

            if (astar != null)
            {
                roadPath = astar.CreatePath(spots, start, end, 1000);
            }
            
            if (roadPath == null)
            {
                //invalid path processing
            }
            return roadPath;
    }
}