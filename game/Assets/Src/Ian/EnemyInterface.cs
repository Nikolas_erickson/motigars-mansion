/*
 * EnemyInterface.cs
 * Ian Finnigan
 * This script is for sending information to every enemy in a scene
 * It is uses a singleton pattern to ensure there is only one
 */

public interface IEnemyInterface
{
    void TriggerForcedDetect();
}
public class EnemyInterface
{
    public static EnemyInterface Instance { get; private set; }
    public bool drBCMode = false;
    public bool forcedDetect = false;

    private EnemyInterface() { } 

    public static void CreateEnemyInterface()
    {

        if (Instance == null)
        {
            Instance = new EnemyInterface();
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            //nothing here yay
        }
    }

    public void ChangeBCMode(bool setVal)
    {
        drBCMode = setVal;
    }
    public void TriggerForcedDetect(bool status)
    {
        forcedDetect = status;
        //forcedDetTimer = forcedDetCooldown;
    }

}
