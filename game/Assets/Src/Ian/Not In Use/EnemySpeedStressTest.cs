/*
 * EnemySpeedStressTest.cs
 * Ian Finnigan
 * This script is for performing a simple stress test
 * It find the speed where the enemy pathfinding no longer performs correctly.
 */
/*
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Collections;
using Unity.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class EnemySpeedStressTest
{

    GameObject enemy;
    PatternedEnemy enemyScrpt;
    GameObject player;

    [UnityTest]
    public IEnumerator EnemySpeedStressTestWithEnumeratorPasses()
    {
        //SceneManager.LoadScene("EnemyTestingScene1");
        yield return new WaitForSeconds(1);
        enemy = GameObject.FindGameObjectWithTag("enemy");
        enemyScrpt = enemy.GetComponent<PatternedEnemy>();
        enemyScrpt.killPlayer = false;
        player = GameObject.FindGameObjectWithTag("Player");
        //enemyScrpt.SetTestMode(true);
        int testSpeed = 1;
        int incremenents = 1;
        Vector3 startPos;
        startPos = enemy.transform.position;

        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), player.transform.rotation);
        Time.timeScale = 100f;
        yield return new WaitForSeconds(20);

        while (enemyScrpt.CheckOnTile())
        {
            Debug.Log(enemyScrpt.CheckOnTile());
            testSpeed = (testSpeed + 1) * 2;
            incremenents ++;
            enemyScrpt.SetSpeed(testSpeed);
            enemy.transform.SetPositionAndRotation(startPos, enemy.transform.rotation);
            player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + (float)Math.Pow(-1, incremenents), player.transform.position.y, player.transform.position.z), player.transform.rotation);
            yield return new WaitForSeconds(10);
            Debug.Log(testSpeed);
        }
        Time.timeScale = 1;
        Assert.AreEqual(0, 0);
    }
}
*/