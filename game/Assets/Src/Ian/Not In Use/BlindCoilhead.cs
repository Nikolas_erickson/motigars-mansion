using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindCoilhead : EnemyCons
{
    EnemyCons enemy;
    // Start is called before the first frame update
    /*void Start()
    {
        enemy = new Blind(this);
        enemy = new Coilhead(enemy);
    }*/

    override public bool SpecialConDetect()
    {
        enemy = new Blind(this);
        
        enemy = new Coilhead(enemy);
        //enemy.Blind(enemy);
        //enemy = new Coilhead(enemy);
        return (enemy.SpecialConDetect());
    }
}
