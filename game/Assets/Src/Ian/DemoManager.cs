/*
 * DemoManager.cs
 * Ian Finnigan
 * This script is for controlling the demo mode
 * It switches whether or not the demo mode is active
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoManager : MonoBehaviour
{
    [SerializeField] bool demoMode = false;
    [SerializeField] int demoWait = 10000;

    private DemoPlayer demoPlayer;
    private int demoTimer; 
    private Player mainPlayer;

    void Awake()
    {
        mainPlayer = GetComponent<Player>();
        demoPlayer = GetComponent<DemoPlayer>();
        demoPlayer.enabled = false;
        demoTimer = demoWait;
    }

    // Update is called once per frame
    void Update()
    {
        demoTimer --;
        if ((Input.GetAxisRaw("Horizontal") != 0) || (Input.GetAxisRaw("Vertical") != 0) || Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.R))
        {
            demoTimer = demoWait;
            demoMode = false;
        }
        if (demoTimer <= 0)
        {
            demoMode = true;
        }
        if (!demoMode)
        {
            EndDemo();
        }
        else
        {
            BeginDemo();
        }
    }

    public void BeginDemo()
    {
        demoPlayer.enabled = true;
        mainPlayer.enabled = false;
    }

    void EndDemo()
    {
        mainPlayer.enabled = true;
        demoPlayer.enabled = false;
    }
}
