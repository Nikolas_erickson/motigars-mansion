/*
 * Coilhead.cs
 * Ian Finnigan
 * This script is for the coilhead decorator
 * This modifies SpecialConMove so that the enemy can only move while not "being seen" by the player
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coilhead : EnemyCons
{
    EnemyCons enemy;

    public Coilhead(EnemyCons enemy)
    {
        this.enemy = enemy;
    }

    override public bool SpecialConMove()
    {

        if (enemy == null)
        {
            //Debug.Log("enemy is null");
            return !(beingSeen && inView);
        }
        else
        {   
            //Debug.Log("Enemy Cons:" + enemy.SpecialConMove() + " Our Cons: " + !(beingSeen && inView));                   
            return (enemy.SpecialConMove() && !(beingSeen && inView));
        }
        //return (base.SpecialConMove() && !beingSeen);
    }

    override public bool SpecialConDetect()
    {
        return enemy.SpecialConDetect();
    }

}
