/*
 * Blind.cs
 * Ian Finnigan
 * This script is for the blind decorator
 * This modifies SpecialConDetect so that the player must move to be detected
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blind : EnemyCons
{
    EnemyCons enemy;
    
    Vector3 oldPos;
    Vector3 currPos;
    GameObject ourPlayer;

    public Blind(EnemyCons enemy)
    {
        this.enemy = enemy;
        ourPlayer = enemy.player;
        //Debug.Log(enemy.GetType());
    }

    override public bool SpecialConDetect()
    {
        Debug.Log("Blind is being checked");
        if (ourPlayer == null)
        {
            ourPlayer = GameObject.FindGameObjectWithTag("Player");
        }

        float yDiff = Math.Abs(currPos.y - oldPos.y);
        float xDiff = Math.Abs(currPos.x - oldPos.x);

        bool playerMoving = false;

        if (currPos != null)
        {
            oldPos = currPos;
            currPos = ourPlayer.transform.position;
        } 
        else 
        {
            oldPos = ourPlayer.transform.position;
            currPos = ourPlayer.transform.position;    
        }

        if ((yDiff < .01) && (xDiff < .01))
        {
            playerMoving = false;
        } 
        else 
        {
            playerMoving = true;
        } 
        //Debug.Log("Running Blind Cons");
        Debug.Log("Blind Movement: " + playerMoving);
        if (enemy == null)
        {
            return (playerMoving);
        }
        else
        {
            return (enemy.SpecialConDetect() && playerMoving);
        }
    }
}
