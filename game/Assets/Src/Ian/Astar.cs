/*
 * Astar.cs
 * Ian Finnigan
 * This script is for the pathfinding algorithm
 * It is used for enemy movement and the demo mode
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class Astar
{
    public MyTile[,] myTiles;

    public Astar(Vector3Int[,] grid, int columns, int rows)
    {
        myTiles = new MyTile[columns, rows];
    }

    //Generates the path between start and end as a list of times
    public List<MyTile> CreatePath(Vector3Int[,] grid, Vector2Int start, Vector2Int end, int length)
    {
        MyTile End = null;
        MyTile Start = null;
        var columns = myTiles.GetUpperBound(0)+1;
        var rows = myTiles.GetUpperBound(1) + 1;
        myTiles = new MyTile[columns, rows];

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                myTiles[i, j] = new MyTile(grid[i, j].x, grid[i, j].y, grid[i, j].z);
            }
        }

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                myTiles[i, j].AddNeighbors(myTiles, i, j);
                if ((myTiles[i, j].X == start.x) && (myTiles[i, j].Y == start.y))
                {
                    Start = myTiles[i, j];
                }
                else if ((myTiles[i, j].X == end.x) && (myTiles[i, j].Y == end.y))
                {
                    End = myTiles[i, j];
                }
            }
            
        }

        if (!IsValidPath(grid, Start, End))
        {
            return null;
        }
        List<MyTile> OpenSet = new List<MyTile>();
        List<MyTile> ClosedSet = new List<MyTile>();

        OpenSet.Add(Start);

        while (OpenSet.Count > 0)
        {
            //Find shortest step distance in the direction of your goal within the open set
            int winner = 0;
            for (int i = 0; i < OpenSet.Count; i++)
                if (OpenSet[i].F < OpenSet[winner].F)
                    winner = i;
                else if (OpenSet[i].F == OpenSet[winner].F)//tie breaking for faster routing
                    if (OpenSet[i].H < OpenSet[winner].H)
                        winner = i;

            var current = OpenSet[winner];

            //Found the path, creates and returns the path
            if ((End != null) && (OpenSet[winner] == End))
            {
                List<MyTile> Path = new List<MyTile>();
                var temp = current;
                Path.Add(temp);
                while (temp.previous != null)
                {
                    Path.Add(temp.previous);
                    temp = temp.previous;
                }
                if ((length - (Path.Count - 1)) < 0)
                {
                    Path.RemoveRange(0, (Path.Count - 1) - length);
                }
                return Path;
            }

            OpenSet.Remove(current);
            ClosedSet.Add(current);


            //Finds the next closest step on the grid
            var neighbors = current.Neighbors;
            for (int i = 0; i < neighbors.Count; i++)//look threw our current myTiles neighbors (current MyTile is the shortest F distance in openSet
            {
                var n = neighbors[i];
                if (!ClosedSet.Contains(n) && (n.Height < 1))//Checks to make sure the neighboor of our current tile is not within closed set, and has a height of less than 1
                {
                    var tempG = current.G + 1;//gets a temp comparison integer for seeing if a route is shorter than our current path

                    bool newPath = false;
                    if (OpenSet.Contains(n)) //Checks if the neighboor we are checking is within the openset
                    {
                        if (tempG < n.G)//The distance to the end goal from this neighboor is shorter so we need a new path
                        {
                            n.G = tempG;
                            newPath = true;
                        }
                    }
                    else//if its not in openSet or closed set, then it IS a new path and we should add it too openset
                    {
                        n.G = tempG;
                        newPath = true;
                        OpenSet.Add(n);
                    }
                    if (newPath)//if it is a newPath caclulate the H and F and set current to the neighbors previous
                    {
                        n.H = Heuristic(n, End);
                        n.F = n.G + n.H;
                        n.previous = current;
                    }
                }
            }

        }
        return null;

    }

    //Calculates Distance between two tiles
    private int Heuristic(MyTile a, MyTile b)
    {
        var dx = Math.Abs(a.X - b.X);
        var dy = Math.Abs(a.Y - b.Y);
        return 1 * (dx + dy);
    } 

    //Checks to make sure there is a valid path between start and end
    private bool IsValidPath(Vector3Int[,] grid, MyTile start, MyTile end)
    {
        if (end == null)
            return false;
        if (start == null)
            return false;
        if (end.Height >= 1)
            return false;
        return true;
    }

}

public class MyTile{
    public int X;
    public int Y;
    public int F;
    public int G;
    public int H;
    public int Height = 0;
    public List<MyTile> Neighbors;
    public MyTile previous = null;
    public MyTile(int x, int y, int height)
    {
        X = x;
        Y = y;
        F = 0;
        G = 0;
        H = 0;
        Neighbors = new List<MyTile>();
        Height = height;
    }

    //Assigns all the neighbor values to the tile
    public void AddNeighbors(MyTile[,] grid, int x, int y)
    {
        if (x < grid.GetUpperBound(0))
            Neighbors.Add(grid[x + 1, y]);
        if (x > 0)
            Neighbors.Add(grid[x - 1, y]);
        if (y < grid.GetUpperBound(1))
            Neighbors.Add(grid[x, y + 1]);
        if (y > 0)
            Neighbors.Add(grid[x, y - 1]);
    }


}
