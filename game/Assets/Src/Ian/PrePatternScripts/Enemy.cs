/*
 * Enemy.cs
 * Ian Finnigan
 * This script is for the basic enemy class
 * It is used for all common enemy behavior, mainly movement.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class Enemy : MonoBehaviour
{
    public GameObject player;
    public Vector2 wanderDir;

    [SerializeField] int wanderRangeX = 1;
    [SerializeField] int wanderRangeY = 1;
    [SerializeField] Rigidbody2D rigidBody;
    

    int i = 0;

    UnityEngine.Vector3 oldPlayerPos;
    Rigidbody2D rb;
    List<MyTile> path;
    float startX;
    float startY;
    Pathfinder pathfinder;
    GameObject pathFinder;
    int wanderDirx = 0;
    int wanderDiry = 0;

    bool wandering = true;


    public float wanderSpeed = 0;
    public GameObject pathPrefab;
    void Start()
    {
        pathFinder = Instantiate(pathPrefab);
        pathfinder = pathFinder.GetComponent<Pathfinder>();
        rb = GetComponent<Rigidbody2D>();
        oldPlayerPos = player.transform.position;
        SetWanderDirs();
        startX = transform.position.x;
        startY = transform.position.y;
    }

    //Control what direction an enemy wanders in when the wander function is running
    void SetWanderDirs()
    {
        
        wanderDirx = (int)UnityEngine.Random.Range(-1, 1);

        if (wanderDirx == 0)
        {
            if (UnityEngine.Random.Range(-1,1) > 0)
            {
                wanderDiry = 1;
            } 
            else 
            {
                wanderDiry = -1;
            }
        } 
        else 
        {
            wanderDiry = 0;
        }
        wanderDir = new Vector2(wanderDirx, wanderDiry);

        wanderSpeed = 1;
    }

    //Have the enemy follow their pathfound path at a specific speed, and update the path to reflect changes in position.
    public void Approach(float speed)
    {
        Vector3 temp;
        if (wandering == true)
        {
            wandering = false;
            transform.Translate (wanderDir * 0 * Time.deltaTime);
        }
        
        if (((int)player.transform.position.x != (int)oldPlayerPos.x) || ((int)player.transform.position.y != (int)oldPlayerPos.y) )    //has player moved?
        { 
            oldPlayerPos = player.transform.position;
            temp = new Vector3(transform.position.x, transform.position.y, 0); //Don't move while updating
            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * 0);//don't move while updating
            path = pathfinder.UpdatePath(player, gameObject); //update path
            if (path == null)
            {
                return;
            }

            i = (path.Count-2); //minus two skips current spot and next to smooth movement
        }
        if (path != null)
        {
            temp = new Vector3((float)((float)path[i].X+.5), (float)((float)path[i].Y+.5), 0); //tell enemy where to move to, targetting next tile

            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * speed); //perform movement
            if (transform.position.x == (float)((float)path[i].X+.5) && transform.position.y == (float)((float)path[i].Y+.5))
            { //check whether or not we have completely reached the next tile
                if (i > 0)
                {
                    i--; //update the next tile
                }
                
            }
        } 
        else 
        {
            //Wander(speed);
            return;
        }

        
    }

    //Have the enemy move in a given direction until they hit a tilemap edge
    public void Wander(float speed)
    {
        wandering = true;
        transform.Translate (wanderDir * wanderSpeed * Time.deltaTime);
        if (((wanderDiry == 1) || (wanderDiry == 0))&& !pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x), (int)(transform.position.y+.5), 0)))
        {
            //no tile at this y
            wanderDiry = -1;
            wanderDirx = (int)UnityEngine.Random.Range(-1, 1);
        } 
        else if (((wanderDiry == -1) || (wanderDiry == 0))&& !pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x), (int)(transform.position.y-1.5), 0)))
        {
            wanderDiry = 1;
            wanderDirx = (int)UnityEngine.Random.Range(-1, 1);
        } 

        if ((wanderDirx == 1) && !pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x+.5), (int)(transform.position.y), 0)))
        {
            //no tile at this y
            wanderDirx = -1;
            wanderDiry = (int)UnityEngine.Random.Range(-1, 1);
        } 
        else if ((wanderDirx == -1) && !pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x-1.5), (int)(transform.position.y), 0)))
        {
            wanderDirx = 1;
            wanderDiry = (int)UnityEngine.Random.Range(-1, 1);
        }
   
        if (transform.position.y >= (startY + wanderRangeY))
        {
            wanderDiry = -1;
            wanderDirx = (int)UnityEngine.Random.Range(-1, 1);
        } 
        else if (transform.position.y <= (startY - wanderRangeY))
        {
            wanderDiry = 1;
            wanderDirx = (int)UnityEngine.Random.Range(-1, 1);
        }
        if (transform.position.x >= (startX + wanderRangeX))
        {
            wanderDirx = -1;
            wanderDiry = (int)UnityEngine.Random.Range(-1, 1);
        } 
        else if (transform.position.x <= (startX - wanderRangeX))
        {
            wanderDirx = 1;
            wanderDiry = (int)UnityEngine.Random.Range(-1, 1);
        }
        wanderDir = new Vector2(wanderDirx, wanderDiry);       
    }

    //See if the tilemap has a given tile
    public bool CheckOnTile()
    {
        return pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x), (int)(transform.position.y)));
    }

    //check if the distance to the player is less than the enemy's given detection range
    public bool PlayerInRange(float detectRange)
    {

        float distance;
        //use pythagorean theorem to determine how far player is away
        distance  = (float)Math.Pow(player.transform.position.x, 2) + (float)Math.Pow(player.transform.position.y, 2);
        distance = (float)Math.Sqrt(distance);
        if (distance <= detectRange)
        {
            return true;
        } 
        else 
        {
            return false;
        }

        
    }

    //Check if the player is in an enemy's attack range
    public bool PlayerInRangeAtk(float atkRange)
    {
        return false;
    }

    //Attack the player
    public void Attack()
    {

    }

}
