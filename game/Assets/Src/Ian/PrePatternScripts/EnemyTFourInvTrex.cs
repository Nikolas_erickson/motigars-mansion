/*
 * EnemyTFourInvTrex.cs
 * Ian Finnigan
 * This script is for the opposite of the Trex enemy
 * It is used to implement an enemy that can only see the player when they are standing still
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTFourInvTrex : Enemy
{

    [SerializeField] int speed = 2;
    [SerializeField] float detectRange = 5;
    [SerializeField] float atkRange = 1;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (PlayerInRangeAtk(atkRange) && SpecialConAtk() && SpecialConDetect())
        {
            Attack();
        }
        else if (PlayerInRange(detectRange) && SpecialConDetect())
        {
            Approach(speed);
        }
        else
        {
            Wander(speed);
        }
        
    }

    bool SpecialConAtk()
    {
        return false;
    }

    bool SpecialConMove()
    {
        return true;
    }

    bool SpecialConDetect()
    {
        return true;
    }
}
