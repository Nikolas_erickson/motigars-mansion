/*
 * EnemyTOneBasic.cs
 * Ian Finnigan
 * This script is for the simplest enemy subclass
 * It is used to implement some basic constraints on enemy behavior
 * It also serves as a template to be copied for other subclasses
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTOneBasic : Enemy
{

    [SerializeField] float speed = 1;
    [SerializeField] float detectRange = 300;
    [SerializeField] float atkRange = 2;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (PlayerInRangeAtk(atkRange) && SpecialConAtk() && SpecialConDetect()){
            Attack();
        }
        else if (PlayerInRange(detectRange) && SpecialConDetect())
        {
            Approach(speed);
        }
        else
        {
            Wander(speed);
        }
        
    }

    //Conditions for attacking
    bool SpecialConAtk()
    {
        return false;
    }

    //Conditions for moving
    bool SpecialConMove()
    {
        return true;
    }

    //Conditions for Detecting
    bool SpecialConDetect()
    {
        return true;
    }

    //Used to set speed for the stress test.
    public void SetSpeed(int newSpeed)
    {
        speed = newSpeed;
    }
}
