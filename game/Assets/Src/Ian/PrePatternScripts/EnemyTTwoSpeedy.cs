/*
 * EnemyTTwoSpeedy.cs
 * Ian Finnigan
 * This script is for the speedy enemy class
 * It is used for an enemy which moves faster but has a shorter detection range
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTTwoSpeedy : Enemy
{
    [SerializeField] int speed = 2;
    [SerializeField] float detectRange = 5;
    [SerializeField] float atkRange = 1;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (PlayerInRangeAtk(atkRange) && SpecialConAtk() && SpecialConDetect())
        {
            Attack();
        }
        else if (PlayerInRange(detectRange) && SpecialConDetect() && SpecialConMove())
        {

            Approach(speed);
        }
        else if (SpecialConMove())
        {
            Wander(speed);
        }
        
    }

    //Conditions for attacking
    bool SpecialConAtk()
    {
        return false;
    }

    //Conditions for moving
    bool SpecialConMove()
    {
        return true;
    }

    //Conditions for detecting
    bool SpecialConDetect()
    {
        return true;
    }
}
