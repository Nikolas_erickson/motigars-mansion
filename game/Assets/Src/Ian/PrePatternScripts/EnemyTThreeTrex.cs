/*
 * EnemyTThreeTrex.cs
 * Ian Finnigan
 * This script is for the "Trex" enemy
 * It is used for an enemy which can only see the player when they are moving
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTThreeTrex : Enemy
{

    [SerializeField] int speed = 2;
    [SerializeField] float detectRange = 5;
    [SerializeField] float atkRange = 1;
    Vector3 oldPos;
    Vector3 currPos;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (currPos != null)
        {
            oldPos = currPos;
            currPos = player.transform.position;
        } 
        else 
        {
            oldPos = player.transform.position;
            currPos = player.transform.position;    
        }

        if (PlayerInRangeAtk(atkRange) && SpecialConAtk() && SpecialConDetect())
        {
            Attack();
        }
        else if ((PlayerInRange(detectRange) && SpecialConDetect()) && SpecialConMove())
        {

            Approach(speed);
        }
        else
        {
            if (SpecialConMove())
            {
                Wander(speed);
            }

        }
        
    }
    //Conditions for attacking
    bool SpecialConAtk()
    {
        return false;
    }

    //Conditions for moving

    bool SpecialConMove()
    {
        return true;
    }

    //Conditions for detecting

    bool SpecialConDetect()
    {
        float yDiff = Math.Abs(currPos.y - oldPos.y);
        float xDiff = Math.Abs(currPos.x - oldPos.x);
        if ((yDiff < .01) && (xDiff < .01)){
            return false;
        } 
        else 
        {
            return true;
        } 
    }
}
