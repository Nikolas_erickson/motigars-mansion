/*
 * PatternedEnemy.cs
 * Ian Finnigan
 * This script is for the basic enemy class
 * It is used for all common enemy behavior, mainly movement.
 */
using System;
using System.Collections.Generic;
//using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PatternedEnemy : MonoBehaviour
{
    static bool playingDetect = false;
    static bool playingMain = false;
    static int musicDelay = 100;
    
    public bool killPlayer = true;
    public bool reachedPlayer = false;
    //GameObject text;
    //TextMeshPro textBox;
    public bool typeBlind = false;
    public bool typeCoil = false;
    public float wanderSpeed = 0;
    //public GameObject pathPrefab;
    public GameObject player;
    public GameObject wanderObject;
    public List<MyTile> path;
    public Pathfinder pathfinder;
    public string levelSound = "Basement";
    public Vector2 wanderDir;

    [SerializeField] float atkRange = 2;
    [SerializeField] float detectRange = 300;
    [SerializeField] float speed = 1;
    [SerializeField] float tileMapScale = 1.5f;
    [SerializeField] GameObject detectField;
    [SerializeField] int wanderRangeX = 1;
    [SerializeField] int wanderRangeY = 1;
    [SerializeField] Rigidbody2D rigidBody;
    [SerializeField] string moveSound = "GhostMoan";
    
    bool beingSeen = false;
    bool forcedDetect = false;
    bool hitPlayer = false;
    bool inView = false;
    bool pathMade = false;
    bool wandering = true;
    bool weAreTheOnePlayingDetect = false;
    Camera cam;
    EnemyCons conditions;
    EnemyInterface enemyInter;
    Flashlight playerLight;
    float startX;
    float startY;
    GameObject fieldInst;
    GameObject flashlight;
    GameObject wanderInst;
    int i = 0;
    int j = -2;
    Rigidbody2D rb;
    Vector3 oldPlayerPos;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        flashlight = GameObject.FindGameObjectWithTag("Flashlight");
        playerLight = flashlight.GetComponent<Flashlight>();
        conditions = new EnemyCons();
        if (typeBlind)
        {
            conditions = new Blind(conditions);
        }
        if (typeCoil)
        {
            conditions = new Coilhead(conditions);
        }

        oldPlayerPos = player.transform.position;
       
        startX = transform.position.x;
        startY = transform.position.y;
        if (conditions != null)
        {
            conditions.player = player;
            conditions.fieldInst = fieldInst;
        }
        
        //cam = Camera.main;
        enemyInter = EnemyInterface.Instance;
        Debug.Log("We Are Awake");
        cam = Camera.main;
        
        
        fieldInst = Instantiate(detectField, player.transform);
        rb = GetComponent<Rigidbody2D>();
        if (wanderInst != null)
        {
            Destroy(wanderInst);
        }
    }

    void FixedUpdate()
    {

        RaycastHit2D hit;
        int layer2 = 1 << 2;
        int notLayer2 = ~layer2;
        Vector3 veiwPos = cam.WorldToViewportPoint(gameObject.transform.position);
        hit = Physics2D.Raycast(transform.position, player.transform.position - transform.position, Mathf.Infinity, notLayer2);
        //Debug.Log(musicDelay);
        if (pathMade == false)
        {
            if (!PlayerInRange(detectRange))
            {
                //textBox.text = "We are outside the detection range";
                Debug.Log("We are outside the detection range");
                wandering = false;
                //path = null;
                Wander(speed);
            }
            else
            {
                wandering = true;
                Approach(speed);
            }
            pathMade = true;
        }

        if (musicDelay > 0)
        {
            musicDelay--;
        }
        //Debug.Log(musicDelay);
        //Debug.Log(playingDetect);
        //textBox.text = playingDetect;

        if (enemyInter != null)
        {
            forcedDetect =enemyInter.forcedDetect;
            killPlayer = !enemyInter.drBCMode;
        }

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.tag == "Player")
            {
                //textBox.text = "We're In Sight";
                Debug.Log("We're In Sight");
                hitPlayer = true;
            }
            else if (hit.collider.gameObject.tag == "Trap")
            {
                gameObject.layer = 2;
                hitPlayer = false;
            }
            else
            {
                Debug.Log(hit.collider.gameObject.tag + "Was Hit");
                hitPlayer = false;
            }
            if (conditions != null)
            {
                conditions.inView = inView;
                conditions.beingSeen = beingSeen;
            }
            
        }

        fieldInst.transform.SetPositionAndRotation(player.transform.position, player.transform.rotation);
        if ((veiwPos.x >= 0 && veiwPos.x <= 1) && (veiwPos.y >= 0 && veiwPos.y <= 1))
        {
            inView = hitPlayer;
        }
        else
        {
            inView = false;
        }
        Debug.Log(SpecialConDetect() + " is the detection status");
        if (PlayerInRangeAtk(atkRange) && SpecialConAtk() && SpecialConDetect())
        {
            Attack();
        }
        else if ((forcedDetect || (PlayerInRange(detectRange) && SpecialConDetect())) && SpecialConMove())
        {
            Debug.Log("Forced Detect: " + forcedDetect);
            //Debug.Log("Player in Range: " + PlayerInRange(detectRange));

            Approach(speed);
            FindObjectOfType<AudioManager>().PlayMovementSound(moveSound, this.transform.position);
            if ((playingMain == false) && (musicDelay == 0) && (DistanceToPlayer() > 5) && weAreTheOnePlayingDetect)
            {   
                playingMain = true;
                playingDetect = false;
                weAreTheOnePlayingDetect = false;
                //Debug.Log(gameObject.name + "has set playingDetect to false");
                //musicDelay = 400;
                FindObjectOfType<AudioManager>().PlayBackgroundMusic(levelSound, Camera.main.transform.position);
            }
        }
        else
        {
            if ((playingMain == false) && (musicDelay == 0) && (DistanceToPlayer() > 5) && weAreTheOnePlayingDetect)
            {   
                playingMain = true;
                playingDetect = false;
                weAreTheOnePlayingDetect = false;
                //Debug.Log(gameObject.name + "has set playingDetect to false");
                //musicDelay = 400;
                FindObjectOfType<AudioManager>().PlayBackgroundMusic(levelSound, Camera.main.transform.position);
            }
            if (SpecialConMove())
            {
                Wander(speed);
                FindObjectOfType<AudioManager>().PlayMovementSound(moveSound, this.transform.position);
            }
        }
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Attack();
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log("Entered Field of View");
        if (other.gameObject.tag == "fov")
        {
            if (playerLight.IsOn())
            {
                beingSeen = true;
            }
            else
            {
                beingSeen = false;
            }
        }
    }

    //register that the player is no longer looking at the enemy
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "fov")
        {
            beingSeen = false;
        }
    }

    //Have the enemy follow their pathfound path at a specific speed, and update the path to reflect changes in position.
    public void Approach(float speed)
    {
        //Debug.Log("Approaching");
        Vector3 temp;
        if (wandering == true)
        {
            wandering = false;
            j = -2;
            transform.Translate (wanderDir * 0 * Time.deltaTime);
            path = pathfinder.UpdatePath(player, gameObject);
            if (path != null)
            {
                i = (path.Count-2);
            }
            else
            {
                i = 0;
            }
        }
        
        if (((int)player.transform.position.x != (int)oldPlayerPos.x) || ((int)player.transform.position.y != (int)oldPlayerPos.y))    //has player moved?
        { 
            oldPlayerPos = player.transform.position;
            temp = new Vector3(transform.position.x, transform.position.y, 0); //Don't move while updating
            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * 0);//don't move while updating
            path = pathfinder.UpdatePath(player, gameObject); //update path
            if (path == null)
            {
                return;
            }

            i = (path.Count-2); //minus two skips current spot and next to smooth movement
        }
        if (path != null && i >= 0) //changed from >= to > to account for argument out of range error
        {
            temp = new Vector3((float)((float)(path[i].X+.5)*tileMapScale), (float)((float)(path[i].Y+.5)*tileMapScale), 0); //tell enemy where to move to, targetting next tile

            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * speed); //perform movement
            if (playingDetect == false)
            {
                //playingMain = false;
                if (typeCoil || typeBlind || (DistanceToPlayer() < 5))
                {
                    playingDetect = true;
                    weAreTheOnePlayingDetect = true;
                    if (musicDelay == 0)
                    {
                        musicDelay = 500;
                        //Debug.Log("Playing Detected from " + gameObject.name);
                        FindObjectOfType<AudioManager>().PlayBackgroundMusic("EnemyTriggered", Camera.main.transform.position);
                    }
                }
                else
                {
                    playingMain = false;
                }
            }
            if ((transform.position.x == (float)((float)(path[i].X+.5)*tileMapScale)) && (transform.position.y == (float)((float)(path[i].Y+.5) * tileMapScale)))
            { //check whether or not we have completely reached the next tile
                if (i > 0)
                {
                    i--; //update the next tile
                }
                
            }
        } 
        else 
        {
            //Wander(speed);
            return;
        }

        
    }

    //Attack the player
    public void Attack()
    {
        reachedPlayer = true;
        if (killPlayer)
        {
            FindObjectOfType<AudioManager>().PlayBackgroundMusic("GameOver", Camera.main.transform.position);
            SceneManager.LoadScene("GameOverScene");
        }
        else
        {
            //Debug.Log("Reached Player");
            gameObject.transform.SetPositionAndRotation(new Vector3 (startX, startY, 0), Quaternion.identity);
            path = pathfinder.UpdatePath(player, gameObject);
            i = (path.Count - 2);
        }
    }

    //See if the tilemap has a given tile
    public bool CheckOnTile()
    {
        return pathfinder.tilemap.HasTile(new Vector3Int((int)(transform.position.x), (int)(transform.position.y)));
    }

    //Return the distance the enemy is from the player
    float DistanceToPlayer()
    {
        float distance;
        //use pythagorean theorem to determine how far player is away
        distance  = (float)Math.Pow((player.transform.position.x - gameObject.transform.position.x), 2) + (float)Math.Pow((player.transform.position.y - gameObject.transform.position.y), 2);
        distance = (float)Math.Sqrt(distance);
        return distance;
    }

    //check if the distance to the player is less than the enemy's given detection range
    public bool PlayerInRange(float detectRange)
    {
        float distance = DistanceToPlayer();

        if (distance <= detectRange)
        {
            //Debug.Log(distance);
            return true;
        } 
        else 
        {
            return false;
        }

        
    }

    //Check if the player is in an enemy's attack range
    public bool PlayerInRangeAtk(float atkRange)
    {
        return false;
    }

    //Used to set speed for the stress test.
    public void SetSpeed(int newSpeed)
    {
        speed = newSpeed;
    }

    public bool SpecialConAtk()
    {
        return conditions.SpecialConAtk();
    }

    //Conditions for Detecting
    public bool SpecialConDetect()
    {
        return conditions.SpecialConDetect();
    }

    //Conditions for moving
    public bool SpecialConMove()
    {
        return conditions.SpecialConMove();
    }

    //Have the enemy move in a given direction until they hit a tilemap edge
    public void Wander(float speed)
    {
        int wanderX;
        int wanderY;
        Vector3 temp;
        //Debug.Log("Wandering");
        //Debug.Log(wandering);
        //Debug.Log("Wandering");
        if (wandering == false)
        {
            //Debug.Log("Changing Wander");
            wandering = true;
            i = -2;
            if (wanderInst != null)
            {
                Destroy(wanderInst);
            }
            wanderX = (int)UnityEngine.Random.Range((startX-wanderRangeX), (startX+wanderRangeX));
            wanderY = (int)UnityEngine.Random.Range((startY-wanderRangeY), (startY+wanderRangeY));
            wanderInst = Instantiate(wanderObject, new Vector3 (wanderX, wanderY, 0), Quaternion.identity);
            //Debug.Log("Exists: " + wanderInst.transform.position);
            path = pathfinder.UpdatePath(wanderInst, gameObject);

            if (path == null)
            {
                wandering = false;
                //Debug.Log("Null Path");
                return;
            }

            j = (path.Count-2);
        }
        //Debug.Log("j = " + j);
        if ((path != null) && (j >= 0) && ((j <= ((wanderRangeX+1) + (wanderRangeY+1))) || (Math.Abs(Math.Abs(transform.position.x) - Math.Abs(startX)) > wanderRangeX) || (Math.Abs(Math.Abs(transform.position.y) - Math.Abs(startY)) > wanderRangeY)))
        {
            temp = new Vector3((float)((float)(path[j].X+.5)*tileMapScale), (float)((float)(path[j].Y+.5)*tileMapScale), 0);
            transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * wanderSpeed); //perform movement
            if ((transform.position.x == (float)((float)(path[j].X+.5)*tileMapScale)) && (transform.position.y == (float)((float)(path[j].Y+.5) * tileMapScale))) //check whether or not we have completely reached the next tile
            { 
                if (j > 0)
                {
                    j--; //update the next tile
                    //Debug.Log("Next Tile");
                }
                else if (j <= 0)
                {
                    //Debug.Log("Not Wandering, reset Path");
                    wandering = false;
                }
            }
        } 
        else 
        {
            //Debug.Log("In the else");
            wandering = false;
            return;
        }
    }
}
