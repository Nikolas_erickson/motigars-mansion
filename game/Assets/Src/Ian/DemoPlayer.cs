/*
 * DemoPlayer.cs
 * Ian Finnigan
 * This script is the demo mode
 * It causes the player to move and navigate without user input
 */

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DemoPlayer : MonoBehaviour
{
    public static DemoPlayer instance;

    public bool reachedGoal = false;
    public GameObject wanderObject;
    public Pathfinder pathfinder;
    private Vector2 playerMovement;

    [SerializeField] int speed = 1;
    [SerializeField] float tileMapScale = 1.5f;
    [SerializeField] Tile defaultTile;
    [SerializeField] private int memoryLength = 3000;
    [SerializeField] private Flashlight flashlight;
    [SerializeField] private Lantern lantern;

    bool needsUpdate = false;
    Camera cam;
    GameObject nearestItem;
    GameObject nearestCoil;
    GameObject nearestBlind;
    GameObject wanderInst;
    int blindNearby = 0;
    int closeTimer = 150;
    int i = 0;
    int memoryTimer;
    int useTimer = 600;
    List<MyTile> path;
    List<double> removedX = new List<double>();
    List<double> removedY = new List<double>();
    List<GameObject> oldVisEnemies;
    List<GameObject> oldVisItems;
    List<GameObject> oldVisTraps;
    List<GameObject> visibleEnemies;
    List<GameObject> visibleItems;
    List<GameObject> visibleTraps;

    private bool enemiesFound = false;
    private bool haveKey = false;
    private bool inventoryOpen = false;
    private GameObject[] enemies;
    private GameObject goal;
    private GameObject[] items;
    private GameObject[] traps;
    private inputTesterInventory inventoryListener;
    private Rigidbody2D rigidBody;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            //Debug.Log("Warning: More than one instance of DemoPlayer found! Deleting extra.");
            Destroy(this);
            return;
        }
        if (wanderInst != null)
        {
            Destroy(wanderInst);
        }
        items = GameObject.FindGameObjectsWithTag("Item");
        goal = GameObject.FindGameObjectWithTag("Goal");
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        traps = GameObject.FindGameObjectsWithTag("Trap");
        cam = Camera.main;
        inventoryListener = GameObject.FindObjectOfType<inputTesterInventory>();
    }

    // Find the player's rigid body.
    void Start()
    {
        memoryTimer = memoryLength;
        rigidBody = GetComponent<Rigidbody2D>();
        pathfinder.GenerateTiles();
    }

    // Update is called once per frame
    void Update()
    {
        if ((flashlight.GetBatteryLife() < 25) && (FindDistance(nearestCoil, gameObject) > 5))
        {
            if (flashlight.IsOn())
            {
                flashlight.toggleState();
            }
        }
        else if (flashlight.IsOn() == false)
        {
            flashlight.toggleState();
        }

        if ((Math.Abs(transform.position.x - goal.transform.position.x) <= 1) && (Math.Abs(transform.position.y - goal.transform.position.y) <= 1))
        {
            reachedGoal = true;
        }

        if (blindNearby > 0)
        {
            blindNearby --;
        }
        //Debug.Log(haveKey);
        if (!enemiesFound)
        {
            enemies = GameObject.FindGameObjectsWithTag("enemy");
            enemiesFound = true;
        }
        memoryTimer --;
        Debug.Log("Memory Timer = " + memoryTimer);
        if (memoryTimer <= 0)
        {
            ForgetPositions();
            memoryTimer = memoryLength;
        }
        items = GameObject.FindGameObjectsWithTag("Item");
        oldVisItems = visibleItems;
        oldVisEnemies = visibleEnemies;
        oldVisTraps = visibleTraps;
        visibleItems = AreInView(items);
        visibleEnemies = AreInView(enemies);
        visibleTraps = AreInView(traps);
        FindNearestBlind();
        FindNearestCoil();
        if (((i <= 0) || ((visibleItems != oldVisItems) && (visibleItems.Count > 0) && (Inventory.getInstance().Count() < 9))) || (needsUpdate == true))
        {
            GeneratePath();
        }
        Move();
        //Debug.Log("Door Open = " + doorOpen);
        useTimer --;
        if (useTimer <= 0)
        {
            if (Inventory.getInstance().Count() > 0)
            {
                if (inventoryOpen == false)
                {
                    inventoryListener.openInventoryUI();
                    inventoryOpen = true;
                    closeTimer = 150;
                    foreach (Item item in Inventory.getInstance().getItems())
                    {
                        Debug.Log(item.getType());
                        if (item.getType() == ItemType.Key)
                        {
                            haveKey = true;
                        }
                        else if ((item.getType() == ItemType.Battery) || (item.getType() == ItemType.BigBattery))
                        {
                            if (flashlight.GetBatteryLife() < 50)
                            {
                                item.use();
                            }
                        }
                        else
                        {
                            item.use();
                        }
                    }
                }
                else
                {
                    closeTimer --;
                    if (closeTimer <= 0)
                    {
                        useTimer = 600;
                        inventoryOpen = false;
                        inventoryListener.closeInventoryUI();
                    }
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D obstacle)
    {
        if (obstacle != null)
        {
            if (obstacle.gameObject != null)
            {
                if (pathfinder != null)
                {
                    if (pathfinder.tilemap.HasTile(new Vector3Int((int)obstacle.gameObject.transform.position.x, (int)obstacle.gameObject.transform.position.y)) && (obstacle.gameObject.tag != "Item") && (obstacle.gameObject.tag != "Door"))
                    {
                        //tempTile = pathfinder.tilemap.GetTile(new Vector3Int((int)obstacle.transform.position.x, (int)obstacle.transform.position.y));
                        pathfinder.tilemap.SetTile(new Vector3Int((int)(obstacle.gameObject.transform.position.x/tileMapScale), (int)(obstacle.gameObject.transform.position.y/tileMapScale)), null);
                        pathfinder.tilemap.SetTile(new Vector3Int((int)(gameObject.transform.position.x/tileMapScale), (int)(gameObject.transform.position.y/tileMapScale)), null);
                        removedX.Add(obstacle.gameObject.transform.position.x);
                        removedY.Add(obstacle.gameObject.transform.position.y);
                    }
                }
            }
        }
        i = 0;
    }

    //Returns a list of all gameobjects in list that the player can currently see.
    List<GameObject> AreInView(GameObject[] list)
    {
        //Debug.Log("Checking View");
        Vector3 viewPos;
        List<GameObject> temp = new List<GameObject>();
        foreach (GameObject entity in list)
        {
            viewPos = cam.WorldToViewportPoint(entity.transform.position);
            //Debug.Log("Checking: " + entity.name + " Position: " + viewPos);
            if ((viewPos.x >= 0 && viewPos.x <= 1) && (viewPos.y >= 0 && viewPos.y <= 1))
            {
                temp.Add(entity);
                //Debug.Log("There is something in view");
                //Debug.Log(entity.transform.position);
            }
        }
        return temp;
    }
    float FindDistance(GameObject ob1, GameObject ob2)
    {
        float distance;
        distance = (float)Math.Pow((ob1.transform.position.x - ob2.transform.position.x), 2);
        distance += (float)Math.Pow((ob1.transform.position.y - ob2.transform.position.y), 2);
        distance = (float)Math.Sqrt(distance);
        //Debug.Log("Checking Distance");
        //Debug.Log(distance);
        return distance;
    }

    //Locates the nearest enemy with the Blind feature
    void FindNearestBlind()
    {
        nearestBlind = null;
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        foreach(GameObject enemy in enemies)
        {
            if (enemy.GetComponent<PatternedEnemy>().typeBlind == true)
            {
                if (nearestBlind == null)
                {
                    nearestBlind = enemy;
                }
                else
                {
                    if (FindDistance(enemy, gameObject) < FindDistance(nearestBlind, gameObject))
                    {
                        nearestBlind = enemy;
                    }
                }
            }
        }
    }

    //Locates nearest enemy with the Coilhead feature
    void FindNearestCoil()
    {
        nearestCoil = null;
        //Debug.Log("Enemies.Length = " + enemies.Length);
        foreach (GameObject enemy in enemies)
        {
            //Debug.Log("WE ARE IN THE FOR LOOP");
            //Debug.Log(enemy.name);
            if (enemy.GetComponent<PatternedEnemy>().typeCoil == true)
            {
                if (nearestCoil == null)
                {
                    nearestCoil = enemy;
                }
                else
                {
                    if (FindDistance(enemy, gameObject) < FindDistance(nearestCoil, gameObject))
                    {
                        nearestCoil = enemy;
                    }
                }
            }
        }
    }

    void ForgetPositions()
    {
        Debug.Log("removed items: " + removedX.Count);
        for (int t = 0; (t < removedX.Count && t < removedY.Count); t++)
        {
            Debug.Log("t: " + t);
            pathfinder.tilemap.SetTile(new Vector3Int((int)removedX[t], (int)removedY[t], 0), defaultTile);
        }
        pathfinder.GenerateTiles();
        removedX = new List<double>();
        removedY = new List<double>();
        /*Destroy(pathFinder);
        pathFinder = Instantiate(pathPrefab);
        pathfinder = pathFinder.GetComponent<Pathfinder>();
        pathfinder.GenerateTiles();*/
    }

    //Generates Path. If no items or enemies are on screen, path is random. Otherwise path is towards nearest item
    void GeneratePath()
    {
        //bool needsUpdate = false;
        //float distance;
        int wanderX;
        int wanderY;

        //visibleItems = AreInView(items);
        //visibleEnemies = AreInView(enemies);
        //Debug.Log("Generating Path");
        if (nearestItem != null)
        {
            if (FindDistance(gameObject, nearestItem) < 2)
            {
                //Debug.Log("We are about to pick up items");
                pickUpNearItems();
                items = GameObject.FindGameObjectsWithTag("Item");
                visibleItems = AreInView(items);
                oldVisItems = visibleItems;
                nearestItem = null;
            }
        }

        if (haveKey == false)
        {
            if ((visibleItems.Count > 0) && (Inventory.getInstance().Count() < 9))
            {
                //Debug.Log("We are stalling at item pathfinding");
                //Debug.Log(Inventory.getInstance().Count() + " is the count");
                //Debug.Log("There are " + visibleItems.Count + " visible items");
                nearestItem = null;
                foreach (GameObject item in visibleItems)
                {
                    if (nearestItem == null)
                    {
                        nearestItem = item;
                    }
                    else
                    {
                        if (FindDistance(item, gameObject) < FindDistance(nearestItem, gameObject))
                        {
                            nearestItem = item;
                        }
                    }
                }
                if (nearestItem != null)
                {
                    path = pathfinder.UpdatePath(nearestItem, gameObject);
                    if (path == null)
                    {
                        if (wanderInst != null)
                        {
                            Destroy(wanderInst);
                        }
                        wanderX = (int)UnityEngine.Random.Range(-23, 31);
                        wanderY = (int)UnityEngine.Random.Range(-33, 35);
                        wanderInst = Instantiate(wanderObject, new Vector3 (wanderX, wanderY, 0), Quaternion.identity);
                        path = pathfinder.UpdatePath(wanderInst, gameObject);
                    }
                }
                else
                {
                    //Debug.Log("nearestItem is null");
                    return;
                }
                //i = (path.Count - 2);
            }
            else
            {
                //Debug.Log("We are stalling in the else");
                if (wanderInst != null)
                {
                    Destroy(wanderInst);
                }
                wanderX = (int)UnityEngine.Random.Range(-23, 31);
                wanderY = (int)UnityEngine.Random.Range(-33, 35);
                wanderInst = Instantiate(wanderObject, new Vector3 (wanderX, wanderY, 0), Quaternion.identity);
                path = pathfinder.UpdatePath(wanderInst, gameObject);
                //i = (path.Count - 2);
            }
        }
        else
        {
            path = pathfinder.UpdatePath(goal, gameObject);
            if (path == null)
            {
                if (wanderInst != null)
                {
                    Destroy(wanderInst);
                }
                wanderX = (int)UnityEngine.Random.Range(-23, 31);
                wanderY = (int)UnityEngine.Random.Range(-33, 35);
                wanderInst = Instantiate(wanderObject, new Vector3 (wanderX, wanderY, 0), Quaternion.identity);
                path = pathfinder.UpdatePath(wanderInst, gameObject);
            }
        }

        if (path != null)
        {
            i = (path.Count - 2);
            needsUpdate = false;
        }
        else
        {
            //i = -2;
            needsUpdate = true;
        }

        if ((visibleEnemies != null) && (path != null))
        {
            foreach (MyTile tile in path)
            {
                foreach (GameObject enemy in visibleEnemies)
                {
                    if(((Math.Abs((int)enemy.transform.position.x - (int)(tile.X*tileMapScale)) <= 1) && (Math.Abs((int)enemy.transform.position.y - (int)(tile.Y*1.5)) <= 1)))
                    {
                        if (tile != path[i])
                        {
                            needsUpdate = true;
                            pathfinder.tilemap.SetTile(new Vector3Int(tile.X, tile.Y, 0), null);
                            removedX.Add(tile.X);
                            removedY.Add(tile.Y);
                            //i = -2;
                        }
                        //GeneratePath();
                    }
                }
            }
        }
    }

    void Move()
    {
        Vector3 temp;
        Vector3 targetDir;

        if ((nearestBlind != null) && (FindDistance(nearestBlind, gameObject) < 3))
        {
            blindNearby = 500;
        }

        if ((path != null) && (i >= 0)) //changed from >= to > to account for argument out of range error
        {
            temp = new Vector3((float)((float)(path[i].X+.5)*tileMapScale), (float)((float)(path[i].Y+.5)*tileMapScale), 0); //tell enemy where to move to, targetting next tile

            if (nearestCoil != null)
            {
                targetDir = nearestCoil.transform.position - transform.position;
                //newDir = UnityEngine.Vector3.RotateTowards(transform.forward, targetDir, 5*Time.deltaTime, 0.0f);
                transform.rotation = Quaternion.LookRotation(Vector3.forward, targetDir);
            }
            
            if (((nearestBlind == null) || (FindDistance(nearestBlind, gameObject) > 3)) && (blindNearby <= 0))
            {
                transform.position = UnityEngine.Vector2.MoveTowards(transform.position, temp, 3f * Time.deltaTime * speed); //perform movement
            }

            if ((transform.position.x == (float)((float)(path[i].X+.5)*tileMapScale)) && (transform.position.y == (float)((float)(path[i].Y+.5) * tileMapScale))) //check whether or not we have completely reached the next tile
            { 
                if (i > 0)
                {
                    i--; //update the next tile
                }
                
            }
            if ((visibleEnemies != null) && (path != null))
            {
                foreach (MyTile tile in path)
                {
                    foreach (GameObject enemy in visibleEnemies)
                    {
                        if(((Math.Abs((int)enemy.transform.position.x - (int)(tile.X*tileMapScale)) <= 1) && (Math.Abs((int)enemy.transform.position.y - (int)(tile.Y*1.5)) <= 1)))
                        {
                            pathfinder.tilemap.SetTile(new Vector3Int(tile.X, tile.Y, 0), null);
                            removedX.Add(tile.X);
                            removedY.Add(tile.Y);
                            needsUpdate = true;
                        }
                    }
                }
            }
        } 
        else 
        {
            needsUpdate = true;
            return;
        }
    }

    private void pickUpNearItems()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 2f);
        //Debug.Log("Picking up items, checking " + colliders.Length + " colliders.");
        foreach (Collider2D collider in colliders)
        {
            //Debug.Log("Checking collider " + collider.name + " with tag " + collider.tag + ".");
            if (collider.CompareTag("Item"))
            {
                //Debug.Log("Item found");
                Item item = collider.GetComponent<ItemScript>().getItem();
                if (item != null)
                {
                    if (Inventory.getInstance().Add(item))
                    {
                        Destroy(collider.gameObject);
                    }
                }
            }
        }
    }
}
