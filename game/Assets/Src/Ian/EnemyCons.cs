/*
 * EnemyCons.cs
 * Ian Finnigan
 * The basic enemy decorator
 * It provides the basic virtual functions to be overridden by other files
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCons
{
    public bool beingSeen;
    public bool inView;

    public GameObject player;
    public GameObject fieldInst;

    virtual public bool SpecialConAtk()
    {
        return false;
    }
    virtual public bool SpecialConMove()
    {
        return true;
    }

    //Conditions for Detecting
    virtual public bool SpecialConDetect()
    {
        return true;
    }
}


