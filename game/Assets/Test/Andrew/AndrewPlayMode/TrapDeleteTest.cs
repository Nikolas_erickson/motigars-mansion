/*
 * TrapDeleteTest.cs
 * Andrew VanCorbach
 * The Play Mode test for deleting traps
 * All of the tests are defined with a comment above that specific test
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapDeleteTest
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {        
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));                        // load test scene for debug
        TrapManager.CreateTrapManager();
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        yield return new EnterPlayMode();
    }

    // deletes GameObject associated with trap and ensures that it isn't updating anymore
    [UnityTest]
    public IEnumerator TrapDeletion()
    {
        // create traps
        int sTrapID = TrapManager.Instance.CreateTrap("Spike", 0.1f, 0.1f, 0f, 0f);
        Trap sTrap = TrapManager.Instance.AccessTrap(sTrapID);

        TrapManager.Instance.DeleteTrap(sTrapID);

        Assert.IsNull(TrapManager.Instance.AccessTrap(sTrapID));

        Assert.IsFalse(sTrap.CheckTrapStatus());

        yield return new WaitForSeconds(0.1f);

        Assert.IsFalse(sTrap.CheckTrapStatus());

        yield return null;
    }

    // deletes one trap and checks that the other is still there 
    [UnityTest]
    public IEnumerator TrapSingleDeletion()
    {
        // create traps
        int sTrapID = TrapManager.Instance.CreateTrap("Spike", 0.2f, 0.1f, 4f, 0f);
        int aTrapID = TrapManager.Instance.CreateTrap("Alert", 0.2f, 0.1f, 0f, 0f);
        Trap sTrap = TrapManager.Instance.AccessTrap(sTrapID);
        Trap aTrap = TrapManager.Instance.AccessTrap(aTrapID);

        TrapManager.Instance.DeleteTrap(aTrapID);

        Assert.IsNull(TrapManager.Instance.AccessTrap(aTrapID));               // assert deleted trap is inaccessible

        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(sTrapID));                // checks that other trap is still active
        yield return new WaitForSeconds(0.1f);
        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(sTrapID));

        yield return null;
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
