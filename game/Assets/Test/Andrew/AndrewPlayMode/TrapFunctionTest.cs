using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapFunctionTest
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));                        // load test scene
        TrapManager.CreateTrapManager();
    }

    [UnityTest]
    public IEnumerator SpikeLowerAndRaise()
    {
        Assert.That(SceneManager.GetActiveScene().name == "AndrewTestScene");                   // assert test scene is loaded

        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Spike", 0.2f, 0.2f, -4f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);

        // Tests for spikes raising and lowering
        Assert.That(trap.CheckTrapStatus(), Is.False);

        yield return new WaitForSeconds(0.2f);
        Assert.That(trap.CheckTrapStatus(), Is.True);

        yield return new WaitForSeconds(0.2f);
        Assert.That(trap.CheckTrapStatus(), Is.False);

        yield return null;
    }

    [UnityTest]
    public IEnumerator TrapReset()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Alert", 0.1f, 0.1f, 4f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);
        ActiveTrap actTrap = (ActiveTrap)trap;

        GameObject player = GameObject.Find("Player");

        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        player.transform.position = new Vector2(4f, 0f);

        yield return new WaitForSeconds(0.05f);

        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        player.transform.position = new Vector2(0f, 0f);

        yield return new WaitForSeconds(0.2f);

        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsFalse(actTrap.IsTrapActive());

        TrapManager.Instance.ResetTrap(trapID);
        TrapManager.Instance.ModifyTrapParams(trapID, 0.1f, 0.1f);

        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        player.transform.position = new Vector2(4f, 0f);

        yield return new WaitForSeconds(0.05f);

        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        player.transform.position = new Vector2(0f, 0f);

        yield return new WaitForSeconds(0.2f);

        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsFalse(actTrap.IsTrapActive());

        yield return null;
    }
    

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
