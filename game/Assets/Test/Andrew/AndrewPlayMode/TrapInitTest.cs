/*
 * TrapInitTest.cs
 * Andrew VanCorbach
 * The Play Mode test for initializing traps
 * All of the tests are defined with a comment above that specific test
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapInitTest
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));                        // load test scene for debug
        TrapManager.CreateTrapManager();
    }

    // tests spike trap instantiation
    [UnityTest]
    public IEnumerator SpikeTrapInstantiation()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Spike", 0.5f, 0.2f, 0f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);

        Assert.IsNotNull(trap);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));

        yield return null;
    }

    // tests dark trap instantiation
    [UnityTest]
    public IEnumerator DarkTrapInstantiation()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Dark", 0.5f, 0.2f, 0f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);
        ActiveTrap actTrap = (ActiveTrap)trap;

        Assert.IsNotNull(trap);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        yield return null;
    }

    // tests item trap instantiation
    [UnityTest]
    public IEnumerator ItemTrapInstantiation()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Item", 0.5f, 0.2f, 0f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);
        ActiveTrap actTrap = (ActiveTrap)trap;

        Assert.IsNotNull(trap);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        yield return null;
    }

    // tests alert trap instantiation
    [UnityTest]
    public IEnumerator AlertTrapInstantiation()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Alert", 0.5f, 0.2f, 0f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);
        ActiveTrap actTrap = (ActiveTrap)trap;

        Assert.IsNotNull(trap);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));
        Assert.IsTrue(actTrap.IsTrapActive());

        yield return null;
    }

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
