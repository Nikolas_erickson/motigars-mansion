/*
 * TrapManagerTest.cs
 * Andrew VanCorbach
 * The Play Mode test for the trap manager
 * All of the tests are defined with a comment above that specific test
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapManagerTest
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));                        // load test scene for debug
        TrapManager.CreateTrapManager();
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        yield return new EnterPlayMode();
    }

    // tests for the RandomPassableTrap function only creating passable traps
    [UnityTest]
    public IEnumerator RandomPassableTrap()
    {
        Assert.IsNotNull(TrapManager.Instance);

        for(int i = 1; i <= 50; i++)
        {
            TrapManager.Instance.CreateRandomPassableTrap(4f, 0f);
        }

        for (int i = 1; i <= 50; i++)
        {
            Assert.AreNotEqual(TrapManager.Instance.GetTrapGameObject(i).name, "alertTrap");
        }

        yield return null;
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
