/*
 * TrapStressTest.cs
 * Andrew VanCorbach
 * The Play Mode test for stressing a trap
 * The test runs a player over a trap and records the speed at which the trap is no longer triggered
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapStressTest
{
    [UnitySetUp]
    public IEnumerator SetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewStressTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));        // load test scene
        TrapManager.CreateTrapManager();
        yield return new EnterPlayMode();
    }

    [UnityTest]
    public IEnumerator TrapStressTestWithEnumeratorPasses()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Alert", 1f, 10f, 0f, 0f);
        Trap trap = TrapManager.Instance.AccessTrap(trapID);

        // find player object
        GameObject player = GameObject.Find("Player");
        MovementScript movementScript = player.GetComponent<MovementScript>();

        Assert.IsNotNull(player);
        Assert.IsNotNull(movementScript);

        float speed = 32.0f;
        float xPosTrap = 2.5f;
        float xPosPlayer = player.transform.position.x;

        while (speed < 1024f)
        {
            while(xPosPlayer < xPosTrap)
            {
                yield return null;
                xPosPlayer = player.transform.position.x;
            }

            if (!trap.CheckTrapStatus())
            {
                Debug.Log("Test Break on speed: " + speed);
                break;
            }
            else
            {
                speed *= 2f;
                Debug.Log("Success, Next Step: " + speed);
                movementScript.resetPlayerX();
                movementScript.setSpeed(speed);
                xPosPlayer = player.transform.position.x;
                yield return new WaitForFixedUpdate();
                trap.ResetTrap();
                trap.SetParams(1f, 10f);
            }
        }

        if(speed >= 256.0f)            // test succeeds if player can move greater than 256 and still triggers trap
        {
            Assert.That(true);
        }
        else
        {
            Assert.That(false);
        }
        
        yield return null;
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        TrapManager.DeleteTrapManager();
        yield return new ExitPlayMode();
    }
}
