/*
 * MovementScript.cs
 * Andrew VanCorbach
 * This simulates player movement for the stress test
 */

using UnityEngine;

public class MovementScript : MonoBehaviour
{
    // Variables dealing with Player movement
    [SerializeField] private float currentSpeed = 0.0f;
    private Rigidbody2D rigidBody;

    // Find the player's rigid body.
    private void Start()
    {
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        setSpeed(currentSpeed);
    }

    private void Update()
    {
        
    }

    public void setSpeed(float speed)
    {
        currentSpeed = speed;
        rigidBody.velocity = new Vector2(currentSpeed, 0.0f);
    }

    // Get the player's x and y for other files. 
    public void resetPlayerX()
    {
        transform.position = new Vector3(-3.5f, 0, 0);
    }
}
