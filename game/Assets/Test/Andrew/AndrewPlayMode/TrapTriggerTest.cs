/*
 * TrapTriggerTest.cs
 * Andrew VanCorbach
 * The Play Mode test for triggering traps
 * All of the tests are defined with a comment above that specific test
 */

using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class TrapTriggerTest
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        TrapManager.CreateTrapManager();
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Andrew/AndrewPlayMode/AndrewTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));                        // load test scene for debug
        yield return new EnterPlayMode();
    }

    // tests spike trap behavior when player walks over and spikes are raised
    [UnityTest]
    public IEnumerator SpikeTrapTrigger()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Spike", 0.3f, 0.001f, 0f, 0f);

        // find and verify player object
        GameObject player = GameObject.Find("Player");
        Assert.IsNotNull(player);

        Assert.AreEqual(0.0f, player.transform.position.x);
        Assert.AreEqual(0.0f, player.transform.position.y);
        Assert.AreEqual(0.0f, player.transform.position.z);

        yield return new WaitForSeconds(0.05f);
        Assert.That(SceneManager.GetActiveScene().name == "GameOverScene");

        yield return null;
    }

    // tests spike trap behavior when player walks over and spikes arent raised
    [UnityTest]
    public IEnumerator SpikeTrapTriggerInactive()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Spike", 0.3f, 0.5f, 0f, 0f);

        // find player object
        GameObject player = GameObject.Find("Player");
        Assert.IsNotNull(player);

        Assert.AreEqual(0.0f, player.transform.position.x);
        Assert.AreEqual(0.0f, player.transform.position.y);
        Assert.AreEqual(0.0f, player.transform.position.z);

        yield return new WaitForSeconds(0.1f);
        Assert.That(SceneManager.GetActiveScene().name == "AndrewTestScene");

        yield return null;
    }

    // tests Item trap behavior when player walks over
    [UnityTest]
    public IEnumerator ItemTrapTrigger()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Dark", 0.5f, 0.1f, 0f, 0f);

        // find player object
        GameObject player = GameObject.Find("Player");
        Assert.IsNotNull(player);

        yield return new WaitForSeconds(0.05f);
        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(trapID));

        yield return new WaitForSeconds(0.1f);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));

        yield return null;
    }

    // tests dark trap behavior when player walks over
    [UnityTest]
    public IEnumerator DarkTrapTriggerTest()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Dark", 0.5f, 0.1f, 0f, 0f);

        // find player object
        GameObject player = GameObject.Find("Player");
        Assert.IsNotNull(player);

        yield return new WaitForSeconds(0.05f);
        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(trapID));

        yield return new WaitForSeconds(0.1f);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));

        yield return null;
    }

    // tests alert trap behavior when player walks over
    [UnityTest]
    public IEnumerator AlertTrapTrigger()
    {
        // create traps
        int trapID = TrapManager.Instance.CreateTrap("Alert", 0.1f, 0.1f, 0f, 0f);

        // find player object
        GameObject player = GameObject.Find("Player");
        Assert.IsNotNull(player);

        yield return new WaitForSeconds(0.05f);
        Assert.IsTrue(TrapManager.Instance.GetTrapStatus(trapID));

        yield return new WaitForSeconds(0.2f);
        Assert.IsFalse(TrapManager.Instance.GetTrapStatus(trapID));

        yield return null;
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }

    [OneTimeTearDown]
    public void OneTimeTearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
