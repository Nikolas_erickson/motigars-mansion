/*
 * TrapManagerTest.cs
 * Andrew VanCorbach
 * The Edit Mode test for the trap manager
 * All of the tests are defined with a comment above that specific test
 */

using System.Collections;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TrapManagerTestEdit
{
    [SetUp]
    public void SetUp()
    {
        TrapManager.CreateTrapManager();
    }

    // tests that the manager initializes properly
    [Test]
    public void ManagerInitalization()
    {
        Assert.IsNotNull(TrapManager.Instance);
    }

    // tests that the [TrapUpdater] initializes when a trap is created
    [Test]
    public void UpdaterInitializer()
    {
        int trapID = TrapManager.Instance.CreateTrap("Spike", 1f, 1f, 0f, 0f);
        GameObject obj = GameObject.Find("[TrapUpdater]");

        Assert.IsNotNull(obj);
    }

    // tests that the manager returns a null when a nonexistent trap's trap is accessed
    [Test]
    public void AccessTrapNonexistentTrap()
    {
        Trap trap = TrapManager.Instance.AccessTrap(1);
        Assert.IsNull(trap);
    }

    // tests that the manager returns a null when a nonexistent trap's GameObject is accessed
    [Test]
    public void GetGameObjectNonExistentTrap()
    {
        GameObject obj = TrapManager.Instance.GetTrapGameObject(1);
        Assert.IsNull(obj);
    }

    // tests that the manager returns a null when a nonexistent trap's status is accessed
    [Test]
    public void StatusNonExistentTrap()
    {
        Trap trap = TrapManager.Instance.AccessTrap(1);
        Assert.IsNull(trap);
    }

    // tests that the manager returns a null when a nonexistent trap is reset
    [Test]
    public void ResetNonExistentTrap()
    {
        int temp = TrapManager.Instance.ResetTrap(1);
        Assert.AreEqual(temp, -1);
    }

    // tests that the manager returns a null when a nonexistent trap's parameters are modified
    [Test]
    public void ModifyNonExistentTrap()
    {
        int temp = TrapManager.Instance.ModifyTrapParams(1, 5f, 5f);
        Assert.AreEqual(temp, -1);
    }

    // tests that the manager returns a null when a nonexistent trap is deleted
    [Test]
    public void DeleteNonExistentTrap()
    {
        int temp = TrapManager.Instance.DeleteTrap(1);
        Assert.AreEqual(temp, -1);
    }

    [TearDown]
    public void TearDown()
    {
        TrapManager.DeleteTrapManager();
    }
}
