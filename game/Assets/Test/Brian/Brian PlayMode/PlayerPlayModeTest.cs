using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PlayerTest
{
    Player player;

    [SetUp]
    public void SetUp()
    {
        Player.CreatePlayer();
        player = Player.instance;
    }

    [TearDown]
    public void TearDown()
    {
        player.Delete();
    }

    // Check that get player position function works
    [Test]
    public void GetPositionTest()
    {
        Assert.AreEqual(player.transform.position, player.getPlayerPos());
    }

    // check that face mouse script rotates player
    [Test]
    private void FaceMouseTest()
    {
        Vector3 OriginalUp = new Vector3(0f, 1f, 0f);
        Assert.AreEqual(player.transform.up, OriginalUp);

        Vector3 mousePosition = new Vector3(0f, -100f, 0f);
        Vector2 direction = new Vector2(mousePosition.x - player.transform.position.x, mousePosition.y - player.transform.position.y);

        player.transform.up = direction;
        Assert.AreNotEqual(player.transform.up, OriginalUp);
    }

    // check that GetPlayerSpeed returns 0 when no input is given
    [Test]
    public void GetPlayerSpeedStillTest()
    {
        Assert.AreEqual(0f, player.GetPlayerSpeed());
    }

    // check that player velocity is not 0 when an input is given
    [Test]
    public void GetPlayerSpeedMovingTest()
    {
        player.SetInput();
        Assert.AreNotEqual(0f, player.GetPlayerSpeed());
    }

    // check that the player cannot accelerate past set upper speed
    [Test]
    public void PlayerAccelerationTest()
    {
        player.SetInput();
        Assert.IsTrue(player.GetPlayerSpeed() <= 5f);
    }
}
