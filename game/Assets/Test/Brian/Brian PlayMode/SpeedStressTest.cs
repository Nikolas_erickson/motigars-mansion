using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class SpeedStressTest
{
    [UnityTest]
    public IEnumerator SpeedStressTestWithEnumeratorPasses()
    {
        float speed = 1f;
        float wallPosX = 2f;
        float playerPosX = 0f;

        GameObject wall = new GameObject();
        wall.AddComponent<Rigidbody2D>();
        wall.AddComponent<BoxCollider2D>();

        Player player = Player.instance;

        Rigidbody2D rigidBody = player.gameObject.GetComponent<Rigidbody2D>();
        BoxCollider2D boxCollider = player.gameObject.GetComponent<BoxCollider2D>();

        player.SetSpeed(speed);
        rigidBody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rigidBody.gravityScale = 0f;
        boxCollider.isTrigger = true;

        playerPosX = player.transform.position.x;

        while (speed < 32768)
        {
            yield return null;
            playerPosX = player.transform.position.x;

            if (playerPosX > wallPosX)
            {
                Debug.Log("Test Break on speed: " + speed);
                break;
            }
            else
            {
                speed *= 2;
                Debug.Log("Success, Next Step: " + speed);
                player.SetSpeed(speed);
                player.transform.position = new Vector2(playerPosX, 0f);
            }

            if (speed >= 256)
            {
                Assert.That(true);
            }
            else
            {
                Assert.That(false);
            }

            yield return null;
        }
    }
 }
