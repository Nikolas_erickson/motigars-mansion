using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class LightingTests
{
    Flashlight flashlight;
    Lantern lantern;

    OnState onState = new OnState();
    OffState offState = new OffState();

    [SetUp]
    public void SetUp()
    {
        Flashlight.CreateFlashlight();
        flashlight = Flashlight.instance;
        Lantern.CreateLantern();
        lantern = Lantern.instance;
        flashlight.lantern = lantern;
        lantern.flashlight = flashlight;
        flashlight.onState = onState;
        flashlight.offState = offState;
    }

    [TearDown]
    public void TearDown()
    {
        flashlight = null;
        lantern = null;
    }

    // check that flashlight in initialized
    [Test]
    public void FlashlightInitTest()
    {
        Assert.IsNotNull(flashlight);
    }

    // check that lantern is initialized
    [Test]
    public void LanternInitTest()
    {
        Assert.IsNotNull(lantern);
    }

    // check that flashlight has off initial state
    [Test]
    public void FlashlightInitStateTest()
    {
        Assert.IsFalse(flashlight.IsOn());
    }

    // check that lantern has off initial state
    [Test]
    public void LanternInitStateTest()
    {
        Assert.IsFalse(lantern.IsOn());
    }

    // check that flashlight enters on state properly
    [Test]
    public void FlashlightOnTest()
    {
        flashlight.toggleState();
        Assert.IsTrue(flashlight.IsOn());
    }

    // check that lantern turns on properly
    [Test]
    public void LanternOnTest()
    {
        lantern.TurnOn();
        Assert.IsTrue(lantern.IsOn());
    }

    // check that flashlight enters off state properly
    [Test]
    public void FlashlightOffTest()
    {
        flashlight.TurnOn();
        flashlight.TurnOff();
        Assert.IsFalse(flashlight.IsOn());
    }

    // check that lantern enters off state properly
    [Test]
    public void LanternOffTest()
    {
        lantern.TurnOn();
        lantern.TurnOff();
        Assert.IsFalse(lantern.IsOn());
    }

    // check that flashlight battery is initialized to 100 
    [Test]
    public void BatteryInitTest()
    {
        Assert.AreEqual(flashlight.GetBatteryLife(), 100f);
    }

    // check that flashlight battery is drained properly
    [Test]
    public void BatteryDrainTest()
    {
        flashlight.DrainBattery(50f);
        Assert.AreEqual(flashlight.GetBatteryLife(), 50f);
    }

    // check that battery life in added to flashlight properly
    [Test]
    public void BatteryAddTest()
    {
        flashlight.DrainBattery(50f);
        flashlight.AddBatteryLife(50f);
        Assert.AreEqual(flashlight.GetBatteryLife(), 100f);
    }

    // check that flashlight turns off lantern when turned on if lantern is on
    [Test]
    public void FlashlightMutualExclusionTest()
    {
        lantern.TurnOn();
        flashlight.TurnOn();
        Assert.IsFalse(lantern.IsOn());
    }

    // check that lantern turns off flashlight when turned on if flashlight is on
    [Test]
    public void LanternMutualExclusionTest()
    {
        flashlight.TurnOn();
        lantern.TurnOn();
        Assert.IsFalse(flashlight.IsOn());
    }

    // check that flashlight charge cannot go below 0
    [Test]
    public void BatteryLifeLowerBoundTest()
    {
        flashlight.DrainBattery(110f);
        Assert.AreEqual(flashlight.GetBatteryLife(), 0f);
    }

    // check that flashlight charge cannot go above 100
    public void BatteryLifeUpperBoundTest()
    {
        Assert.AreEqual(flashlight.GetBatteryLife(), 100f);
        flashlight.AddBatteryLife(10f);
        Assert.AreEqual(flashlight.GetBatteryLife(), 100f);
    }

    // check that flashlight game object is initialized to inactive
    public void FlashlightInactiveInit()
    {
        Assert.IsFalse(flashlight.lightObject.activeSelf);
    }

    // check that lantern game object is initialized to inactive
    public void LanternInactiveInit()
    {
        Assert.IsFalse(lantern.lightObject.activeSelf);
    }

    // check that putting flashlight in on state activates the game object
    [Test]
    public void FlashlightActiveTest()
    {
        flashlight.TurnOn();
        Assert.IsTrue(flashlight.lightObject.activeSelf);
    }

    // check that putting lantern in on state activates the game object
    [Test]
    public void LanternActiveTest()
    {
        lantern.TurnOn();
        Assert.IsTrue(lantern.lightObject.activeSelf);
    }

    // check that flashlight is set to inactive by TurnOff function
    [Test]
    public void FlashlightInactiveTest()
    {
        flashlight.TurnOn();
        flashlight.TurnOff();
        Assert.IsFalse(flashlight.lightObject.activeSelf);
    }

    // check that lantern is set to inactive by TurnOff function
    [Test]
    public void LanternInactiveTest()
    {
        lantern.TurnOn();
        lantern.TurnOff();
        Assert.IsFalse(lantern.lightObject.activeSelf);
    }
}
