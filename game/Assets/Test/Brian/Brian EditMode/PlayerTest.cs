using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PlayerTest
{
    Player player;

    [SetUp]
    public void SetUp()
    {
        Player.CreatePlayer();
        player = Player.instance;
    }

    // Check if player properly initialized
    [Test]
    public void PlayerIntializeTest()
    {
        Assert.IsNotNull(player.GetInstance());
    }
    
    // check that current speed is initialized to 0
    [Test]
    public void CurrentSpeedInitTest()
    {
        Assert.AreEqual(player.GetPlayerSpeed(), 0f);
    }

    // check that player is singleton
    [Test]
    public void PlayerIsSingletonTest()
    {
        Player player2 = Player.instance;
        Assert.IsTrue(player2 == player);
    }

    [TearDown]
    public void TearDown()
    {
        player.Delete();
    }
}
