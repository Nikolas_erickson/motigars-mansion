using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ObjectTests
{
    [SetUp]
    public void SetUp()
    {
        ObjectManager.createManager();
    }

    [Test]
    public void CrateInstantiation()
    {
        Object objectCheck = ObjectManager.Instance.createObject("Crate");

        Assert.IsNotNull(objectCheck);
    }

    [Test]
    public void TableInstantiation()
    {
        Object objectCheck = ObjectManager.Instance.createObject("Table");

        Assert.IsNotNull(objectCheck.envObject);
    }

    [Test]
    public void TorchInstantiation()
    {
        GameObject objectCheck = GameObject.Instantiate(Resources.Load("LanternPrefab", typeof(GameObject))) as GameObject;

        Assert.IsNotNull(objectCheck);
    }

    [Test]
    public void ObjectContainsObjectInfoHolder()
    {
        Object tempObj = ObjectFactory.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity, 1);

        Assert.IsNotNull(tempObj.envObject.GetComponent<ObjectInfoHolder>());
    }

    [Test]
    public void GetObjectDictionaryTest()
    {
        Assert.IsNotNull(ObjectManager.Instance.getObjectDictionary());
    }

    [Test]
    public void EachObjectHasADifferentLocation()
    {
        Vector2[] crateLocations = new Vector2[13];

        int numObjects = 8;
        int[] crateLocationsArray = new int[numObjects];
        bool found = false;

        // Initialize the int array.
        for (int i = 0; i < numObjects; i++)
        {
            crateLocationsArray[i] = -1;
        }

        //Spawn numObjects around the map while making sure that they do not repeat.
        for (int i = 0; i < numObjects; i++)
        {
            found = false;
            int randNum = Random.Range(1, crateLocations.Length);

            for (int j = 0; j < numObjects; j++)
            {
                if (crateLocationsArray[j] == randNum)
                {
                    found = true;
                    i--;
                    break;
                }
            }

            if (!found)
            {
                crateLocationsArray[i] = 1;
            }
        }

        foreach(int location in crateLocationsArray)
        {
            Debug.Log(location);
            Assert.Positive(location);
        }
    }

    [Test]
    public void InsureObjectHasDifferentID()
    {
        int tempInt = 0;
        for (int i = 0; i < 20; i++)
        {
            int tempObjID = ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);

            Assert.AreNotEqual(tempInt, tempObjID);

            tempInt = tempObjID;
        }
    }

    [Test]
    public void DeleteAllObjects()
    {
        for (int i = 0; i < 8; i++)
        {
            ObjectManager.Instance.createObject("Table");
        }

        ObjectManager.Instance.clearAllObjects();

        Assert.AreEqual(0, ObjectManager.Instance.getObjectDictionary().Count);
    }

    [Test]
    public void DeleteAllObjectsOfCertainType()
    {
        for (int i = 0; i < 8; i++)
        {
            ObjectManager.Instance.createObject("Table", new Vector2(0f, 0f), Quaternion.identity);
        }

        for (int i = 0; i < 20; i++)
        {
            ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);
        }

        ObjectManager.Instance.destroyAllCertainObjects("Crate");

        Assert.AreEqual(8, ObjectManager.Instance.getObjectDictionary().Count);
    }

    [Test]
    public void EnsureCorrectInfoHolderName()
    {
        int tempID = ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);

        ObjectManager.Instance.getObjectDictionary().TryGetValue(tempID, out Object tempObj);

        ObjectInfoHolder objectInfoHolder = tempObj.envObject.GetComponent<ObjectInfoHolder>();

        Assert.AreEqual("Crate", objectInfoHolder.getName());
    }

    [Test]
    public void ManagerInit()
    {
        Assert.IsNotNull(ObjectManager.Instance);
    }

    [Test]
    public void GetNullGameObject()
    {
        GameObject objCheck = ObjectManager.Instance.getGameObject(2);

        Assert.IsNull(objCheck);
    }

    [Test]
    public void CreateNullObject()
    {
        Assert.IsNull(ObjectManager.Instance.createObject("FakeObject"));
    }

    [Test]
    public void DestroyNullObject()
    {
        Assert.AreEqual(-1, ObjectManager.Instance.destroyObject(3));
    }

    [Test]
    public void ChangePositionOfNullObject()
    {
        Assert.AreEqual(-1, ObjectManager.Instance.updateObjectPosition(3, new Vector2(0f, 0f), Quaternion.identity));
    }

    [TearDown]
    public void Teardown()
    {
        ObjectManager.deleteManager();
    }

}
