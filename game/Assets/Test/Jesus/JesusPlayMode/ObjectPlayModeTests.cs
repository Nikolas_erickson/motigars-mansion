using System.Collections.Generic;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class ObjectPlayModeTests
{
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        ObjectManager.createManager();
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Jesus/JesusPlayMode/JesusTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));
        yield return new EnterPlayMode();
    }

    [UnityTest]
    public IEnumerator FindObjectNearPlayer()
    {
        ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        yield return new WaitForSeconds(1);

        int tempInt = ObjectManager.Instance.findObjectNearby(player.transform.position);
        ObjectInfoHolder objectInfoHolder = ObjectManager.Instance.getGameObject(tempInt).GetComponent<ObjectInfoHolder>();

        Assert.AreEqual(objectInfoHolder.getIDHolder(), tempInt);

        yield return null;
    }

    [UnityTest]
    public IEnumerator CrateOnUseFunction()
    {
        ObjectManager objectManager = ObjectManager.Instance;

        int tempID = objectManager.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        yield return new WaitForSeconds(1);

        objectManager.getObjectDictionary().TryGetValue(tempID, out Object tempObj);

        tempObj.interactWithObject(player.transform.position);
        SpriteRenderer spriteRenderer = tempObj.envObject.GetComponent<SpriteRenderer>();

        Assert.That(spriteRenderer.sprite.name == "crateOpened");

        yield return null;
    }

    [UnityTest]
    public IEnumerator CrateLocationTest()
    {

        int tempObj = ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);

        Vector3 originalPos = ObjectManager.Instance.getGameObject(tempObj).transform.position;

        yield return null;

        ObjectManager.Instance.updateObjectPosition(tempObj, new Vector2(18.5f, -2f), Quaternion.identity);

        Assert.AreNotEqual(originalPos, ObjectManager.Instance.getGameObject(tempObj).transform.position);
    }

    [UnityTest]
    public IEnumerator CrateGenerationLimitTest()
    {
        int itemsPlaced = 1;
        int fpsThreshold = 10;

        ObjectManager objectManager = ObjectManager.Instance;

        int loopsLeft = 500;
        float framerate = 100;


        objectManager.createObject("Crate");

        while (framerate > fpsThreshold && loopsLeft > 0)
        {
            Debug.Log("Placing items loop");
            float startTime = Time.time;

            for (int i = 0; i < itemsPlaced; i++)
            {
                objectManager.createObject("Crate");
            }

            itemsPlaced *= 2;
            yield return null;

            float timeTaken = Time.time - startTime;
            framerate = 1 / timeTaken;
            Debug.Log("The frame while placing " + itemsPlaced + " items is: " + framerate);

            loopsLeft--;
        }

        Assert.True(framerate < fpsThreshold);

        yield return null;
    }

    [UnityTest]
    public IEnumerator KeyNearby()
    {
        ObjectManager.Instance.AwakeLogic();

        for(int i = 0; i < 10; i++)
        {
            ObjectManager.Instance.createObject("Crate", new Vector2(0f, 0f), Quaternion.identity);
        }

        GameObject player = GameObject.FindGameObjectWithTag("Player");

        List<int> allObjects = new List<int>(ObjectManager.Instance.getObjectDictionary().Keys);
        foreach (int tempID in allObjects)
        {
            ObjectManager.Instance.getObjectDictionary().TryGetValue(tempID, out Object tempObj);

            tempObj.interactWithObject(player.transform.position);
        }

        yield return new WaitForSeconds(5);

        GameObject keyObject = GameObject.FindGameObjectWithTag("Item");

        Assert.IsNotNull(keyObject);
    }
}