using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class itemGenerationLimit
{
    int itemsPlaced = 1;
    int FPS_THRESHOLD = 20;

    [UnityTest]
    public IEnumerator itemGenerationLimitWithEnumeratorPasses()
    {
        GameObject itemGenObj = new GameObject();
        itemGenObj.AddComponent<Camera>();
        IItemGenerator itemGenerator = ItemGenerator.getInstance();
        //itemGenObj.AddComponent<SpriteArrays>();
        int loopsLeft = 20;
        float frameRate = 100;

        placeItem();

        while (frameRate > FPS_THRESHOLD && loopsLeft-- > 0)
        {
            Debug.Log("placing items loop");
            float startTime = Time.time;
            for (int i = 0; i < itemsPlaced; i++)
            {
                placeItem();
            }
            itemsPlaced *= 2;
            yield return null;
            float timeTaken = Time.time - startTime;
            frameRate = 1 / timeTaken;
            Debug.Log("Frame rate while placing " + itemsPlaced / 2 + " items: " + frameRate);
        }

        Assert.True(frameRate < FPS_THRESHOLD);

        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;

    }

    private void placeItem()
    {
        GameObject myObj = ItemGenerator.getInstance().generateItem(ItemType.Key);
        SpriteRenderer rend = myObj.GetComponent<SpriteRenderer>();
        rend.sortingOrder = 1;
        myObj.transform.position = RandomPosition();
    }
    private Vector3 RandomPosition()
    {
        int screenSize = 5;
        return new Vector3(Random.Range(-screenSize, screenSize), Random.Range(-screenSize, screenSize), 0);
    }
}
