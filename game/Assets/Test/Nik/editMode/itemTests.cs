using NUnit.Framework;

public class itemTests
{

    Inventory inventory;
    ItemGenerator itemGenerator;

    [SetUp]
    public void SetUp()
    {
        inventory = Inventory.getInstance();
        itemGenerator = ItemGenerator.getInstance();
    }

    [TearDown]
    public void TearDown()
    {
        inventory.Delete();
    }

    // Tests if item genator is singleton
    [Test]
    public void itemGeneratorIsSingleton()
    {
        IItemGenerator itemGenerator2 = ItemGenerator.getInstance();
        Assert.IsTrue(itemGenerator == itemGenerator2);
    }

    // Tests if item initializes properly
    [Test]
    public void itemTestsItemInstanceCreation()
    {
        Item item = new Item();
        Assert.IsTrue(item != null);
    }

    // Tests if item can be set to itemtype.key
    [Test]
    public void itemTestsItemTypeKey()
    {
        Item item = new Item();
        item.setItemType(ItemType.Key);
        Assert.IsTrue(item.getType() == ItemType.Key);
    }

    // Tests if item can be set to itemtype.key
    [Test]
    public void itemTestsItemTypeBattery()
    {
        Item item = new Item();
        item.setItemType(ItemType.Battery);
        Assert.IsTrue(item.getType() == ItemType.Battery);
    }

    // Tests if item can be set to itemtype.key
    [Test]
    public void itemTestsItemTypeNote()
    {
        Item item = new Item();
        item.setItemType(ItemType.Note);
        Assert.IsTrue(item.getType() == ItemType.Note);
    }

    // Tests if item can be set to itemtype.key
    [Test]
    public void itemTestsItemTypeDefault()
    {
        Item item = new Item();
        item.setItemType(ItemType.Default);
        Assert.IsTrue(item.getType() == ItemType.Default);
    }

    // Test to see if default item sprites match descriptions
    [Test]
    public void itemTestsDefaultItemSpritesMatchDescriptions()
    {
        Assert.IsTrue(itemGenerator.defaultItemDescriptionsMatchSpriteArray());
    }

}
