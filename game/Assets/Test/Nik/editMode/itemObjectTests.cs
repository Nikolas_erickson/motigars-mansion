using NUnit.Framework;
using UnityEngine;

public class itemObjectTests
{


    Inventory inventory;
    ItemGenerator itemGenerator;
    GameObject item;

    [SetUp]
    public void SetUp()
    {
        inventory = Inventory.getInstance();
        itemGenerator = ItemGenerator.getInstance();
    }

    [TearDown]
    public void TearDown()
    {
        if (item != null)
            UnityEngine.Object.DestroyImmediate(item);
        inventory.Delete();
    }

    // Test if item Game Objects are created
    [Test]
    public void itemObjectTestsKeyItemInstanceCreation()
    {
        item = itemGenerator.generateItem(ItemType.Key);
        Assert.IsTrue(item != null);
    }

    // Test if item Game Objects are created
    [Test]
    public void itemObjectTestsNoteItemInstanceCreation()
    {
        item = itemGenerator.generateItem(ItemType.Note);
        Assert.IsTrue(item != null);
    }

    // Test if item Game Objects are created
    [Test]
    public void itemObjectTestsBatteryItemInstanceCreation()
    {
        item = itemGenerator.generateItem(ItemType.Battery);
        Assert.IsTrue(item != null);
    }

    // Test if item Game Objects are created
    [Test]
    public void itemObjectTestsDefaultItemInstanceCreation()
    {
        item = itemGenerator.generateItem(ItemType.Default);
        Assert.IsTrue(item != null);
    }

    // Test if item keys are assiged a door id
    [Test]
    public void itemObjectTestsKeyAssignedDoorId()
    {
        item = itemGenerator.generateItem(ItemType.Key, 27465);
        Assert.IsTrue(((Key)item.GetComponent<ItemScript>().getItem()).getDoorID() == 27465);
    }

    // Test to make sure two keys dont have same door id
    [Test]
    public void itemObjectTestsKeyDoorIdNotEqualToAnotherKeyDoorId()
    {
        item = itemGenerator.generateItem(ItemType.Key, 1);
        GameObject item2 = itemGenerator.generateItem(ItemType.Key, 2);
        Assert.IsTrue(((Key)item.GetComponent<ItemScript>().getItem()).getDoorID() != ((Key)item2.GetComponent<ItemScript>().getItem()).getDoorID());
        UnityEngine.Object.DestroyImmediate(item2);
    }

    // Test if item batteries are assigned a charge
    [Test]
    public void itemObjectTestsBatteryAssignedChargeLevel()
    {
        item = itemGenerator.generateItem(ItemType.Battery);
        Assert.IsTrue(((Battery)item.GetComponent<ItemScript>().getItem()).getCharge() > 0);
    }

    // Test if small batteries are assigned a charge of 50
    [Test]
    public void itemObjectTestsSmallBatteryAssignedChargeLevel50()
    {
        item = itemGenerator.generateItem(ItemType.Battery);
        Assert.IsTrue(((Battery)item.GetComponent<ItemScript>().getItem()).getCharge() == 50);
    }

    // Test if small batteries are assigned a charge of 100
    [Test]
    public void itemObjectTestsBigBatteryAssignedChargeLevel100()
    {
        item = itemGenerator.generateItem(ItemType.BigBattery);
        Assert.IsTrue(((Battery)item.GetComponent<ItemScript>().getItem()).getCharge() == 100);
    }

    // Test if using battery results in error when there is no player object
    [Test]
    public void itemObjectTestsBatteryUseNoPlayerObject()
    {
        item = itemGenerator.generateItem(ItemType.Battery);
        Assert.IsTrue(((Battery)item.GetComponent<ItemScript>().getItem()).use().StartsWith("E#"));
    }

    // Test if using key results in error when there is no player object
    [Test]
    public void itemObjectTestsKeyUseNoPlayerObject()
    {
        item = itemGenerator.generateItem(ItemType.Key);
        Debug.Log(((Key)item.GetComponent<ItemScript>().getItem()).use());
        Assert.IsTrue(((Key)item.GetComponent<ItemScript>().getItem()).use().StartsWith("E#"));
    }

    // Test if using generic item results in no error when there is no player object
    [Test]
    public void itemObjectTestsDefaultItemUseNoPlayerObject()
    {
        item = itemGenerator.generateItem(ItemType.Default);
        Assert.IsFalse(item.GetComponent<ItemScript>().getItem().use().StartsWith("E#"));
    }
}
