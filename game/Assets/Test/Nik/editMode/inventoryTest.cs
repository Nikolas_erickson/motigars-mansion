using System.Collections;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class inventoryTest
{
    Inventory inventory;

    [SetUp]
    public void SetUp()
    {
        inventory = Inventory.getInstance();
    }

    [TearDown]
    public void TearDown()
    {
        inventory.Delete();
    }

    // Test if inventory initializes properly
    [Test]
    public void inventoryTestInit()
    {
        // Use the Assert class to test conditions
        Assert.IsTrue(inventory != null);
    }

    // Test if can add item to inventory
    [Test]
    public void inventoryTestAddItem()
    {
        Assert.IsTrue(inventory.Count() == 0);
        inventory.Add(new Item());
        Assert.IsTrue(inventory.Count() == 1);
    }

    // Test if can add too many items to inventory
    [Test]
    public void inventoryTestCantAddToManyItems()
    {
        int space = inventory.SpaceAvailable();
        for (int i = 0; i < space; i++)
        {
            Assert.IsTrue(inventory.Add(new Item()));
        }
        Assert.IsFalse(inventory.Add(new Item()));
    }

    // Test if can remove item from inventory
    [Test]
    public void inventoryTestRemoveItem()
    {
        Item item = new Item();
        inventory.Add(item);
        inventory.Remove(item);
        Assert.IsFalse(inventory.getItems().Contains(item));
        Assert.IsTrue(inventory.Count() == 0);
    }

    // Test if can clear inventory
    [Test]
    public void inventoryTestClearInventory()
    {
        for (int i = 0; i < inventory.SpaceAvailable(); i++)
        {
            inventory.Add(new Item());
        }
        inventory.clearInventory();
        Assert.IsTrue(inventory.Count() == 0);
    }

    // Test if inventory size limit is enforced
    [Test]
    public void inventoryTestSizeLimit()
    {
        Assert.IsTrue(inventory.SpaceAvailable() == inventory.getSize());
        for (int i = 0; i < inventory.getSize(); i++)
        {
            inventory.Add(new Item());
        }
        Assert.IsTrue(inventory.SpaceAvailable() == 0);
    }

    // Test if item count is returned properly
    [Test]
    public void inventoryTestCount()
    {
        Assert.IsTrue(inventory.Count() == 0);
        inventory.Add(new Item());
        Assert.IsTrue(inventory.Count() == 1);
        inventory.Add(new Item());
        Assert.IsTrue(inventory.Count() == 2);
    }

    // Test if inventory returns space properly
    [Test]
    public void inventoryTestSpaceAvailable()
    {
        int size = inventory.getSize();
        Assert.IsTrue(inventory.SpaceAvailable() == size);
        inventory.Add(new Item());
        Assert.IsTrue(inventory.SpaceAvailable() == size - 1);
        for (int i = 0; i < size - 2; i++)
        {
            inventory.Add(new Item());
        }
        Assert.IsTrue(inventory.SpaceAvailable() == 1);
        inventory.Add(new Item());
        Assert.IsTrue(inventory.SpaceAvailable() == 0);
    }

    // remove item that isn't in inventory
    [Test]
    public void inventoryTestRemoveItemNotInInventory()
    {
        Item item = new Item();
        Assert.IsFalse(inventory.Remove(item));
        Assert.IsFalse(inventory.getItems().Contains(item));
        Assert.IsTrue(inventory.Count() == 0);
    }

    // remove item from empty inventory
    [Test]
    public void inventoryTestRemoveItemFromEmptyInventory()
    {
        Item item = new Item();
        inventory.Remove(item);
        Assert.IsFalse(inventory.getItems().Contains(item));
        Assert.IsTrue(inventory.Count() == 0);
    }

    // test that inventory is a singleton
    [Test]
    public void inventoryTestSingleton()
    {
        Inventory inventory2 = Inventory.getInstance();
        Assert.IsTrue(inventory == inventory2);
    }
}
