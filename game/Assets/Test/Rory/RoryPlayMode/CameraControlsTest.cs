/*
 * CameraControlTest.cs
 * Rory Arnone-Wheat
 * This class checks that the camera
 * follows that player and stays within
 * the bounds when zooming in and out.
 */

using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class CameraControlsTest : InputTestFixture
{
    private Keyboard keyboard;

    [SetUp]
    //Intialization of keyboard
    public override void Setup()
    {
        keyboard = InputSystem.AddDevice<Keyboard>();
    }

    // Tests that the camera follows the player
    [UnityTest]
    public IEnumerator CameraControls()
    {
        yield return SceneManager.LoadSceneAsync("Basement");

        GameObject gameObject = new GameObject();
        float initXPos = gameObject.transform.position.x;
        gameObject.AddComponent<Player>();
        gameObject.AddComponent<Rigidbody2D>();

        Press(keyboard.upArrowKey);
        yield return new WaitForSeconds(2);
        Release(keyboard.upArrowKey);

        Debug.Log(gameObject.transform.position);
        Debug.Log(Camera.main.transform.position);

        if((Camera.main.transform.position - new Vector3(-21.73f, -7.88f, -10.00f)).magnitude < 0.1)
        {
            Camera.main.transform.position = new Vector3(-21.73f, -7.88f, -10.00f);
        }

        Assert.AreEqual(new Vector3(-21.73f, -7.88f, -10.00f), Camera.main.transform.position);
    }

    // Tests that the camera doesnt zoom in past its bounds
    [UnityTest]
    public IEnumerator ZoomInTest()
    {
        yield return SceneManager.LoadSceneAsync("Basement");

        Camera.main.GetComponent<CameraScript>().ZoomTime();

        yield return new WaitForSeconds(10);

        Camera.main.GetComponent<CameraScript>().ZoomTime();

        yield return new WaitForSeconds(10); 

        Assert.IsTrue(Camera.main.GetComponent<CameraScript>().HowZoomyIsIt() >= 2f);
    }

    // Tests that the camera doesnt zoom out past its bounds
    [UnityTest]
    public IEnumerator ZoomOutTest()
    {
        yield return SceneManager.LoadSceneAsync("Basement");

        Camera.main.GetComponent<CameraScript>().ZoomTime();

        yield return new WaitForSeconds(10);

        Assert.IsTrue(Camera.main.GetComponent<CameraScript>().HowZoomyIsIt() <= 8f);
    }
}
