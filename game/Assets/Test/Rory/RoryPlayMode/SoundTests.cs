/*
 * SoundTest.cs
 * Rory Arnone-Wheat
 * This class checks that the 
 * associated functions of the 
 * AudioManager all work properly.
 */

using System.Collections;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class SoundTests
{
    //private AudioManager audioManager;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Rory/RoryPlayMode/RoryPlayModeTestScene.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        yield return new EnterPlayMode();
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }

    // Checks if the audio manager can play the MainMenu music
    [UnityTest]
    public IEnumerator MainMenuCheck()
    {
        
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[0];
        audioManager.PlayBackgroundMusic("MainMenu", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;        
    }

    // Checks if the audio manager can play the Basement music
    [UnityTest]
    public IEnumerator BasementMusicCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        Debug.Log(audioManager);
        BackgroundMusic audioCheck = audioManager.music[1];
        audioManager.PlayBackgroundMusic("Basement", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the GameOver music
    [UnityTest]
    public IEnumerator GameOverCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[2];
        audioManager.PlayBackgroundMusic("GameOver", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the PauseMenu music
    [UnityTest]
    public IEnumerator PauseMenuCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[3];
        audioManager.PlayBackgroundMusic("PauseMenu", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the EnemyTriggered music
    [UnityTest]
    public IEnumerator EnemyTriggeredCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[4];
        audioManager.PlayBackgroundMusic("EnemyTriggered", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the YouEscaped music
    [UnityTest]
    public IEnumerator YouEscapedCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[5];
        audioManager.PlayBackgroundMusic("YouEscaped", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the 2nd Floor music
    [UnityTest]
    public IEnumerator SecondFloorCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[6];
        audioManager.PlayBackgroundMusic("2ndFloor", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the GroundFloor music
    [UnityTest]
    public IEnumerator GroundFloorCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        BackgroundMusic audioCheck = audioManager.music[7];
        audioManager.PlayBackgroundMusic("GroundFloor", Camera.main.transform.position);

        Assert.IsTrue(audioCheck.isPlaying());
        yield return null;
    }

    // Checks if the audio manager can play the alertTrapTrigger Sound
    [UnityTest]
    public IEnumerator AlertTrapTriggerCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("alertTrapTrigger", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the darkTrapOff Sound
    [UnityTest]
    public IEnumerator darkTrapOffCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("darkTrapOff", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the darkTrapTrigger Sound
    [UnityTest]
    public IEnumerator DarkTrapTriggerCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("darkTrapTrigger", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the itemTrapTrigger Sound
    [UnityTest]
    public IEnumerator ItemTrapTriggerCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("itemTrapTrigger", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the spikeTrapDown Sound
    [UnityTest]
    public IEnumerator SpikeTrapDownCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("spikeTrapDown", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the spikeTrapHitPlayer Sound
    [UnityTest]
    public IEnumerator SpikeTrapHitPlayerCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("spikeTrapHitPlayer", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the spikeTrapUp Sound
    [UnityTest]
    public IEnumerator SpikeTrapUpCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("spikeTrapUp", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the chestOpen Sound
    [UnityTest]
    public IEnumerator ChestOpenCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("chestOpen", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the keyJingle Sound
    [UnityTest]
    public IEnumerator KeyJingleCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("keyJingle", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the letterOpen Sound
    [UnityTest]
    public IEnumerator LetterOpenCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("letterOpen", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the flashlightOn Sound
    [UnityTest]
    public IEnumerator FlashlightOnCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("flashlightOn", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the flashlightOff Sound
    [UnityTest]
    public IEnumerator FlashlightOffCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("flashlightOff", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the doorOpen Sound
    [UnityTest]
    public IEnumerator DoorOpenCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.Play("doorOpen", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the WetStep movement
    [UnityTest]
    public IEnumerator WetStepCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("WetStep", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the GhostMoan movement
    [UnityTest]
    public IEnumerator GhostMoanCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(2.5f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("GhostMoan", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the GoreRip movement
    [UnityTest]
    public IEnumerator GoreRipCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(3f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("GoreRip", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the MonsterGrowl movement
    [UnityTest]
    public IEnumerator MonsterGrowlCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(3f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("MonsterGrowl", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the CrunchyStep movement
    [UnityTest]
    public IEnumerator CrunchyStepCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(3f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("CrunchySteps", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the Psych movement
    [UnityTest]
    public IEnumerator PsychCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(3f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("Psych", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }

    // Checks if the audio manager can play the playerSteps movement
    [UnityTest]
    public IEnumerator PlayerStepsCheck()
    {
        //yield return SceneManager.LoadSceneAsync("Basement");
        yield return new WaitForSeconds(3f);
        AudioManager audioManager = GameObject.FindObjectOfType<AudioManager>();
        audioManager.PlayMovementSound("playerSteps", Camera.main.transform.position);
        AudioSource audioCheck = GameObject.FindObjectOfType<AudioSource>();

        Assert.IsTrue(audioCheck.isPlaying);
        yield return null;
    }
}
