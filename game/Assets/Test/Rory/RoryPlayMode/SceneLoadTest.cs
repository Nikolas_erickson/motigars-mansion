/*
 * SceneLoadTest.cs
 * Rory Arnone-Wheat
 * This class checks that all scenes
 * load properly upon being called.
 */

using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class SceneLoadTest
{
    // Tests that the first game scene loads
    [UnityTest]
    public IEnumerator StartGameSceneLoadTest()
    {
        yield return SceneManager.LoadSceneAsync("Basement");
        Debug.Log("load level");

        Assert.IsTrue(SceneManager.GetActiveScene().name == "Basement");
        Debug.Log("active scene test");
    }

    // Tests that the game over scene loads
    [UnityTest]
    public IEnumerator GameOverSceneLoadTest()
    {
        yield return SceneManager.LoadSceneAsync("GameOverScene");
        Debug.Log("load game over");

        Assert.IsTrue(SceneManager.GetActiveScene().name == "GameOverScene");
        Debug.Log("active scene test");
    }

    // Tests that the main menu scene loads
    [UnityTest]
    public IEnumerator MainMenuLoadTest()
    {
        yield return SceneManager.LoadSceneAsync("MainMenu");
        Debug.Log("load level");

        Assert.IsTrue(SceneManager.GetActiveScene().name == "MainMenu");
        Debug.Log("active scene test");
    }

    // Tests that the first floor scene loads
    [UnityTest]
    public IEnumerator FirstFloorLoadTest()
    {
        yield return SceneManager.LoadSceneAsync("FirstFloor");
        Debug.Log("load level");

        Assert.IsTrue(SceneManager.GetActiveScene().name == "FirstFloor");
        Debug.Log("active scene test");
    }

    // Tests that the escaped scene loads
    [UnityTest]
    public IEnumerator EscapedLoadTest()
    {
        yield return SceneManager.LoadSceneAsync("YouEscaped");
        Debug.Log("load level");

        Assert.IsTrue(SceneManager.GetActiveScene().name == "YouEscaped");
        Debug.Log("active scene test");
    }
}
