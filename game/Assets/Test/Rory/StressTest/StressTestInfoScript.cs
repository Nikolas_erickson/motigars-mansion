/*
 * StressTestInfoScript.cs
 * Rory Arnone-Wheat
 * This class stress tests
 * the camera's zoom functions.
 */

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StressTestInfoScript : MonoBehaviour
{
    [SerializeField] private CameraScript zoomies;
    [SerializeField] private TextMeshProUGUI speedUI;

    private float speed;

    // Update is called once per frame
    void Update()
    {
        // If the camera is within the bounds keep running the test. If it isn't stop the test and display where it failed
        if(2 < zoomies.GetComponent<CameraScript>().HowZoomyIsIt() && zoomies.GetComponent<CameraScript>().HowZoomyIsIt() < 8)
        {
            speed = zoomies.GetComponent<CameraScript>().WhatsTheSpeed();           // Sets the variable for zoomSpeed
            speedUI.text = "Speed is " + speed.ToString();                          // Displays the zoom speed

            if (zoomies.GetComponent<CameraScript>().HowZoomyIsIt() > 7)            // If the camera is zoomed past 7 zooms in 
            {
                zoomies.GetComponent<CameraScript>().ZoomTime();                    // Switches the zoom bool so it will zoom in
            }
            else if (zoomies.GetComponent<CameraScript>().HowZoomyIsIt() < 3)       // If the camera is zoomed below 3 zooms out
            {
                zoomies.GetComponent<CameraScript>().DoubleZoom();                  // Doubles the zoom speed
                zoomies.GetComponent<CameraScript>().ZoomTime();                    // Switches the zoom bool so it will zoom out
            }
        } else
        {
            speed = zoomies.GetComponent<CameraScript>().WhatsTheSpeed();           // sets the final speed variable when goes out of bounds
            speedUI.text = "Went out of bounds at " + speed.ToString();             // displays final speed
        }
        
    }
}
