/*
 * EnemyWanderTest.cs
 * Ian Finnigan
 * This script is for performing a simple boundary test.
 * It makes sure the wander function still behaves properly
 */

using System;
using System.Collections;
using NUnit.Framework;
//using NUnit.Framework.Constraints;
//using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class EnemyWanderTest
{
    GameObject enemy;
    GameObject player;
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Ian/EnemyTestingScene2.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }
    
    [UnitySetUp]
    public IEnumerator Setup()
    {
        yield return new EnterPlayMode();
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }
 
    [UnityTest]
    public IEnumerator EnemyWanderTestWithEnumeratorPasses()
    {
        //SceneManager.LoadScene("EnemyTestingScene2");
        yield return new WaitForSeconds(1);
        enemy = GameObject.FindGameObjectWithTag("enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        Debug.Log(enemy.name);
        Debug.Log(player.name);

        Vector3 startPos;

        startPos = enemy.transform.position;
        Time.timeScale = 10f;
        yield return new WaitForSeconds(10);

        Time.timeScale = 1;
        Debug.Log(enemy.transform.position);
        Assert.AreNotEqual(startPos, enemy.transform.position);
    }
}
