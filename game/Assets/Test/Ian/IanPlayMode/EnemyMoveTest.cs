/*
 * EnemyMoveTest.cs
 * Ian Finnigan
 * This script is for performing a simple boundary test.
 * It makes sure the approach function still behaves properly
 */

using System;
using System.Collections;
using NUnit.Framework;
//using NUnit.Framework.Constraints;
//using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class EnemyMoveTest
{

    Flashlight flashlight;
    GameObject enemy;
    GameObject coil;
    GameObject blind;
    GameObject blindCoil;
    GameObject[] enemies;
    GameObject player;
    IItemGenerator itemGenerator = ItemGenerator.getInstance();
    PatternedEnemy enemyScrpt;
    Vector3 playerStart = new Vector3((float)8.5, (float)-3.13, 0);
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Ian/EnemyTestingScene1.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        yield return new EnterPlayMode();
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        //enemy = GameObject.FindGameObjectWithTag("enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        foreach (GameObject en in enemies)
        {
            if (en.name == "EnemyBasic")
            {
                enemy = en;
            }
            else if (en.name == "EnemyBlind")
            {
                blind = en;
            }
            else if (en.name == "EnemyCoilhead")
            {
                coil = en;
            }
            else
            {
                blindCoil = en;
            }
        }
        //yield return new WaitForSeconds(1);
        blind.SetActive(false);
        coil.SetActive(false);
        blindCoil.SetActive(false);
        enemy.SetActive(false);
        player.transform.SetPositionAndRotation(playerStart, Quaternion.identity);
        flashlight = GameObject.FindGameObjectWithTag("Flashlight").GetComponent<Flashlight>();
        flashlight.AddBatteryLife(100);
        if (flashlight.IsOn() == false)
        {
            flashlight.toggleState();
        }
        
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }
 
    [UnityTest]
    public IEnumerator EnemyMoveTestWithEnumeratorPasses()
    {
        //SceneManager.LoadScene("EnemyTestingScene1");
        enemy.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = enemy.GetComponent<PatternedEnemy>();
        //enemyScrpt.SetTestMode(true);
        //Debug.Log(enemy.name);
        //Debug.Log(player.name);

        //float distance;
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), player.transform.rotation);
        Time.timeScale = 10f;
        yield return new WaitForSeconds(36);
        /*distance = (float)Math.Pow(enemy.transform.position.x - player.transform.position.x, 2) + (float)Math.Pow(enemy.transform.position.y - player.transform.position.y, 2);
        distance = (float)Math.Sqrt(distance);
        //Debug.Log(distance);
        if (distance < 2)
        {
            distance = 0;
        }
        Time.timeScale = 1;*/
        //Debug.Log(enemyScrpt.reachedPlayer);
        Assert.AreEqual(enemyScrpt.reachedPlayer, true);
    }

    [UnityTest]
    public IEnumerator EnemySpeedStressTestWithEnumeratorPasses()
    {
        //SceneManager.LoadScene("EnemyTestingScene1");
        enemy.SetActive(true);
        yield return new WaitForSeconds(1);
        //enemy = GameObject.FindGameObjectWithTag("enemy");
        enemyScrpt = enemy.GetComponent<PatternedEnemy>();
        enemyScrpt.killPlayer = false;
        player = GameObject.FindGameObjectWithTag("Player");
        //enemyScrpt.SetTestMode(true);
        int testSpeed = 1;
        int incremenents = 1;
        Vector3 startPos;
        startPos = enemy.transform.position;

        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), player.transform.rotation);
        Time.timeScale = 100f;
        yield return new WaitForSeconds(20);

        while (enemyScrpt.CheckOnTile())
        {
            Debug.Log(enemyScrpt.CheckOnTile());
            testSpeed = (testSpeed + 1) * 2;
            incremenents ++;
            enemyScrpt.SetSpeed(testSpeed);
            enemy.transform.SetPositionAndRotation(startPos, enemy.transform.rotation);
            player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + (float)Math.Pow(-1, incremenents), player.transform.position.y, player.transform.position.z), player.transform.rotation);
            yield return new WaitForSeconds(10);
            Debug.Log(testSpeed);
        }
        Time.timeScale = 1;
        Assert.AreEqual(0, 0);
    }

    [UnityTest]
    public IEnumerator EnemyBlindSpecialConFalse()
    {
        blind.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = blind.GetComponent<PatternedEnemy>();
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), player.transform.rotation);
        yield return new WaitForSeconds(10);
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyBlindSpecialConTrue()
    {
        
        int moveTimer = 1000;
        blind.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = blind.GetComponent<PatternedEnemy>();
        //playerStart = player.transform.position;
        while (moveTimer > 0)
        {
            player.transform.position = UnityEngine.Vector2.MoveTowards(player.transform.position, new Vector3(8, 14, 0), 3f * Time.deltaTime);
            yield return new WaitForSeconds((float)0.005);
            moveTimer --;
        }
        
        Assert.IsTrue(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyCoilSpecialConFalse()
    {
        coil.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = coil.GetComponent<PatternedEnemy>();
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z), player.transform.rotation);
        Time.timeScale = 10;
        yield return new WaitForSeconds(10);
        Time.timeScale = 1;
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyCoilSpecialConTrue()
    {
        coil.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = coil.GetComponent<PatternedEnemy>();
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z), new Quaternion(player.transform.rotation.x, player.transform.rotation.y, player.transform.rotation.z + 90, player.transform.rotation.w));
        Time.timeScale = 5;
        yield return new WaitForSeconds(5);
        Time.timeScale = 1;
        Assert.IsTrue(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyBlindCoilSpecialConFalseWatched()
    {
        blindCoil.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = blindCoil.GetComponent<PatternedEnemy>();
        enemyScrpt.reachedPlayer = false;
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z), player.transform.rotation);
        Time.timeScale = 10;
        yield return new WaitForSeconds(10);
        Time.timeScale = 1;
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyBlindCoilSpecialConFalseStill()
    {
        blindCoil.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = blindCoil.GetComponent<PatternedEnemy>();
        enemyScrpt.reachedPlayer = false;
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), new Quaternion(player.transform.rotation.x, player.transform.rotation.y, player.transform.rotation.z + 90, player.transform.rotation.w));
        yield return new WaitForSeconds(10);
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator EnemyBlindCoilSpecialConTrue()
    {
        int moveTimer = 1000;
        blindCoil.SetActive(true);
        yield return new WaitForSeconds(1);
        enemyScrpt = blindCoil.GetComponent<PatternedEnemy>();
        player.transform.SetPositionAndRotation(new Vector3(player.transform.position.x + 1, player.transform.position.y, player.transform.position.z), new Quaternion(player.transform.rotation.x, player.transform.rotation.y, player.transform.rotation.z + 90, player.transform.rotation.w));
        while (moveTimer > 0)
        {
            player.transform.position = UnityEngine.Vector2.MoveTowards(player.transform.position, new Vector3(8, 14, 0), 3f * Time.deltaTime);
            yield return new WaitForSeconds((float)0.005);
            moveTimer --;
        }
        Assert.IsTrue(enemyScrpt.reachedPlayer);
    }
}
