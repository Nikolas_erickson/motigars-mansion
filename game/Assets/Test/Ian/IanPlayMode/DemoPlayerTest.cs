//using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEditor.TestTools;

public class DemoPlayerTest
{
    Flashlight flashlight;
    GameObject enemy;
    GameObject coil;
    GameObject blind;
    GameObject blindCoil;
    GameObject[] enemies;
    GameObject goal;
    GameObject player;
    PatternedEnemy enemyScrpt;
    Vector3 playerStart = new Vector3((float)8.5, (float)-3.13, 0);
    Vector3 blindStart = new Vector3(-9, -3, 0);
    Vector3 blindCoilStart = new Vector3(8, 2, 0);
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        EditorSceneManager.LoadSceneInPlayMode("Assets/Test/Ian/DemoPlayerTestingScene.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        yield return new EnterPlayMode();
        enemies = GameObject.FindGameObjectsWithTag("enemy");
        //enemy = GameObject.FindGameObjectWithTag("enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        goal  = GameObject.FindGameObjectWithTag("Goal");
        flashlight = GameObject.FindGameObjectWithTag("Flashlight").GetComponent<Flashlight>();
        foreach (GameObject en in enemies)
        {
            if (en.name == "EnemyBasic")
            {
                enemy = en;
            }
            else if (en.name == "EnemyBlind")
            {
                blind = en;
            }
            else if (en.name == "EnemyCoilhead")
            {
                coil = en;
            }
            else
            {
                blindCoil = en;
            }
        }
        //yield return new WaitForSeconds(1);
        blind.SetActive(false);
        coil.SetActive(false);
        blindCoil.SetActive(false);
        enemy.SetActive(false);
        player.transform.SetPositionAndRotation(playerStart, Quaternion.identity);
        blind.transform.SetPositionAndRotation(blindStart, Quaternion.identity);
        blindCoil.transform.SetPositionAndRotation(blindCoilStart, Quaternion.identity);
        placeItem();
        if (flashlight.IsOn())
        {
            flashlight.toggleState();
        }
        flashlight.AddBatteryLife(100);
        flashlight.UpdateBatteryChargeIndicator();
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        yield return new ExitPlayMode();
    }

    private void placeItem()
    {
        GameObject myObj = ItemGenerator.getInstance().generateItem(ItemType.Battery);
        SpriteRenderer rend = myObj.GetComponent<SpriteRenderer>();
        rend.sortingOrder = 1;
        myObj.transform.position = RandomPosition();
    }
    private Vector3 RandomPosition()
    {
        int screenSize = 5;
        return new Vector3(Random.Range(-screenSize, screenSize), Random.Range(-screenSize, screenSize), 0);
    }

    [UnityTest]
    public IEnumerator PathToItem()
    {

        yield return new WaitForSeconds(15);
        Assert.IsTrue((GameObject.FindGameObjectsWithTag("Item").Length == 0));
    }

    [UnityTest]
    public IEnumerator AvoidEnemy()
    {
        enemy.SetActive(true);
        Time.timeScale = 3;
        yield return new WaitForSeconds(30);
        enemyScrpt.reachedPlayer = false;
        Time.timeScale = 1;
        enemyScrpt = enemy.GetComponent<PatternedEnemy>();
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator PathToTheGoal()
    {
        blind.transform.SetPositionAndRotation(new Vector3(100, 100, 0), Quaternion.identity);
        blindCoil.transform.SetPositionAndRotation(new Vector3(-100, -100, 0), Quaternion.identity);
        GameObject myObj = ItemGenerator.getInstance().generateItem(ItemType.Key);
        SpriteRenderer rend = myObj.GetComponent<SpriteRenderer>();
        rend.sortingOrder = 1;
        myObj.transform.position = RandomPosition();
        Time.timeScale = 5;
        yield return new WaitForSeconds(50);
        Time.timeScale = 1;
        Assert.IsTrue(player.GetComponent<DemoPlayer>().reachedGoal);
    }

    [UnityTest]
    public IEnumerator AvoidCoil()
    {
        coil.SetActive(true);
        Time.timeScale = 3;
        enemyScrpt = coil.GetComponent<PatternedEnemy>();
        enemyScrpt.reachedPlayer = false;
        flashlight.toggleState();
        yield return new WaitForSeconds(30);
        Time.timeScale = 1;
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator AvoidBlind()
    {
        blind.SetActive(true);
        Time.timeScale = 3;
        enemyScrpt = blind.GetComponent<PatternedEnemy>();
        enemyScrpt.reachedPlayer = false;
        yield return new WaitForSeconds(30);
        Time.timeScale = 1;
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }

    [UnityTest]
    public IEnumerator AvoidBlindCoil()
    {
        blindCoil.SetActive(true);
        Time.timeScale = 10;
        enemyScrpt = blindCoil.GetComponent<PatternedEnemy>();
        enemyScrpt.reachedPlayer = false;
        flashlight.toggleState();
        yield return new WaitForSeconds(30);
        Time.timeScale = 1;
        Assert.IsFalse(enemyScrpt.reachedPlayer);
    }
}
