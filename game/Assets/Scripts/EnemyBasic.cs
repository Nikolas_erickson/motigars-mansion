using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBasic : MonoBehaviour
{
    [SerializeField] private Player player;
    private bool isColliding = false;

    public Rigidbody2D rigidBody;

    // Have this enemy just move towards the player, very simple. 
    void FixedUpdate()
    {
        Vector2 playerPos = player.getPlayerPos();
        if (!isColliding)
        {
            //transform.position = Vector2.MoveTowards(transform.position, playerPos, 3f * Time.deltaTime);
        }
        else
        {
            rigidBody.velocity = Vector2.zero;
        }
    }

    // Stop moving once it collids with the player.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isColliding = true;
        }
    }

    // Resume moving once it stops colliding with the player.
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isColliding = false;
        }
    }
}
