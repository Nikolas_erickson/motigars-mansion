Readme for the InventoryCanvas prefab
Author: Nik Erickson (4/3/24)

Usage: Add to scene if the player should be able to access the 
inventory screen.

Video: https://vandalsuidaho-my.sharepoint.com/:v:/g/personal/eric7862_vandals_uidaho_edu/EYTnhCd2a2RPsq_r3ulWucsBjenpz6a5xYGK0QF5M3OOfA?e=dQfRzM&nav=eyJwbGF5YmFja09wdGlvbnMiOnt9LCJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbE1vZGUiOiJtaXMiLCJyZWZlcnJhbFZpZXciOiJwb3N0cm9sbC1jb3B5bGluayIsInJlZmVycmFsUGxheWJhY2tTZXNzaW9uSWQiOiJjOTI5ODhmNy03NmJmLTRkMTQtOTNmNC05YjllNmYwODEyZjAifX0%3D


Description: This prefab is a fully contained inventory management UI.
The UI starts with a toggle button and a listener for an activating keypress.
The default key is set to 'I'. When the user presses the activation key or button
the UI appears. The UI can then be closed in the same manner.

While the screen is open, the user is able to view their inventory. The UI
displays each item in the inventory, item information, and allows for some
inventory actions to be initiated.

When the user selects an item, a short description of the item is displayed.
The player is then able to drop the item or use the item.


Implementation: The inventory itself is a class that manages user inventory. It
has methods for adding, removing, clearing, and listing contents.

Items are a class hierarchy that has classes for each type of item, all drawing
from an Item superclass. The Item superclass has a virtual function for item 
descriptions, which is overridden in the child classes. The items implement
a factory pattern that assigns their sprites, descriptions, and other
attributes.

The UI gets a list of the items currently held in the inventory and then creates
a button for each one and places those buttons in a grid. When the user selects
an item, a variable is set to that stores the item information for the item
selected. The drop button and the use button execute their respective code
for the item selected.
