using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapInitializer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TrapManager.CreateTrapManager();

        TrapManager.Instance.CreateRandomTrap(4f, 4f);
        TrapManager.Instance.CreateRandomPassableTrap(4f, 0f);

        TrapManager.Instance.CreateTrap("Alert", 2f, 2f, -4f, 4f);
        TrapManager.Instance.CreateTrap("Dark", 2f, 2f, -4f, 0f);
        TrapManager.Instance.CreateTrap("Item", 2f, 2f, -4f, -4f);
        TrapManager.Instance.CreateTrap("Spike", 2f, 2f, 0f, -3f);
    }
}
