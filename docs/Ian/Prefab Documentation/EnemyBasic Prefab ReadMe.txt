EnemyBasic Prefab ReadMe
-------------------------

Description:
-------------------------
This prefab is a fully-functional 2D enemy that you can integrate into any 2D game with minimal effort. The enemy 
willl pathfind to the player, using the quickest route as defined by the A* algorithm. Once the player starts moving, the enemy will begin to move towards it.
The script automatically finds the GameObject tagged as player, so you don't even need to define the target. The scripts also make use of a decorator pattern to define special conditions for enemy attacks, detection, and movement, making it easy to add unique behavior to your enemies.

Included:
-----------------------
EnemyBasic prefab
FieldOfView prefab
wanderTarget prefab
EnemyManager prefab
AudioManager prefab
PatternedEnemy.cs
EnemyCons.cs
Blinc.cs
Coil.cs

Getting Started:
----------------
In order for each instance of the prefab to function properly, you must have at least one tilemap in the scene, defining the enemy's movable area, with the Pathfinder.cs script on the parent portion (called Grid by default) of the tilemap. For the pathfinder script, both public tilemap variables must be assigned to the child portion of the tilemap. For information on how to create a tilemap, you can visit https://docs.unity3d.com/Manual/Tilemap.html. It is highly recommended that the tilemap for Path Prefab be invisible.

You must also defnine WanderObject as a prefab of an empty GameObject. Additionally, there must be an audio manager in the scene, as the enemy plays certain audio queues.

Options:
----------------
KillPlayer: Bool variable that determines whether or not the enemy is lethal. For default behavior, when this is on, once the enemy collides with the player a GameOver scene is called. When this is is off, when the enemy collides with the player the enemy's position is set back to its initial position. Both of these behaviors can be easily modified.

Pathfinder: A tilemap with a Pathfinder script on the parent object. The tilemap should define what areas the enemy is capable of moving to. Although the item can be assigned at runtime, it cannot be created at runtime and must exist in the scene.

WanderObject: A GameObject which is instantiated and used as a movement target for the wander function. If you wish for your enemy to wander while the player is unreachable, this object is instantiated at a random location, the enemy is told to move towards it, and when the enemy reaches the instance said instance is destroyed and a new one created elsewhere.

WanderRangeX & WanderRangeY: Integers defining how far from the enemy's initial position the WanderObject can be instantiated.

WanderSpeed: Float defining how fast the enemy moves while wandering.

Atk Range: If your enemy has a ranged attack, this defines the range at which it can make said attack. By default, due to the SpecialConAtk function, the enemy never attacks except when colliding with the player, but this can be easily modified.

Detect Range: Integer defining how close the player must be for the enemy to be aware of them.

Speed: Float defing the enemy's speed while approaching the player.

TileMapScale: Used to adjust some internal values to match the scale of your TileMap if your tilemap has been scaled up or down.

DetectField: A GameObject which is instantiated and used to help determine if the player can see the enemy, used for one of the included decorators. By default, this is set to the FieldOfView prefab.

TypeBlind: Bool variable telling the enemy to use a decorator that makes it so that the enemy can only detect the player while they are moving.

TypeCoil: Bool variable telling the enemy to use a decorator that makes it so that the enemy cannot move while the player is looking at it.

levelSound: The name of the default background audio you wand playing in the level.

moveSound: The name of the sound you want to play when the enemy moves.



