﻿This README is for the Crate Prefab Environmental Object.
Please read all of it before attempting to integrate it into your project.


Modifying code:
    If you want to change how the interaction with the object works, all you have to change is the
    onUse() function and adjust it your liking.

    Everything else will work itself at, for example, the interaction with it is within the ObjectManager()
    which therefore means that anything you do change in the Crate.cs only affects that object.

How to create an object:
    1. Create an instance of the ObjectManager by doing 'ObjectManager.createManager()', if one doesn't already exist.

    2. Once an instance is created, do one of the falling calls depending on what you want to do:
        'ObjectManager.Instance.createObject("Crate", Vector2 position, Quaternion rotation);'
        'ObjectManager.Instance.createObject("Crate");'

        Note: Use the first one if you are wanting to adjust either the position or rotation of the object when instantiating or
              or use the second one if you want to create the object at the prefab's origin (In this case the object's origin is 0,0,0).
   
   
Crate interaction:
    In order to interact with this object, walk up to the crate until you are right next to it and the proceed by
    pressing the interact button: Z.

    This will open the crate which will call the AudioManager to do a sound effect and also change the sprite to an opened version