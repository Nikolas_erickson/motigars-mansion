This README is for the 2D Player with Lights.
Please read all of it before attempting to integrate it into your project.
The package contains a demo scene. 

Asset includes:
Player: Game object that is move by user input with user with movement acceleration 
Flashlight: Light2D object that is a child of the player and illuminates a cone in front of the player
Lantern: Light2D object that is a child of the player and illuminates a small area around the player

How to use:
1. Drag the player prefab into the scene you want to use it in.
2. Create a slider UI object and drag it into the field on the flashlight script

Code: 

PlayerScript:
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR.Haptics;

public class Player : MonoBehaviour
{
    public static Player instance;
    private static readonly object padlock = new object();

    // Variables dealing with Player movement.
    private Vector2 playerMovement;

    [SerializeField] private float maxSpeed = 5f;
    [SerializeField] private float currentSpeed = 0f;
    [SerializeField] private float acceleration = 5f;

    [SerializeField] private Flashlight flashlight;
    [SerializeField] private Lantern lantern;

    /*
     * Doing it this is not really necessary the more that I think about but Dr. BC did mention making everything
     * private and not public so this is one way to do I guess. This method really is only necessary when dealing 
     * prefabs I believe but I did want to provide a couple of examples. 
     */
    private Rigidbody2D rigidBody;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("Warning: More than one instance of Player found! Deleting extra.");
            Destroy(this);
            return;
        }
    }

    public static void CreatePlayer()
    {
        if(instance == null)
        {
            GameObject playerObject = new GameObject();
            instance = playerObject.AddComponent<Player>();
        }
        else
        {
            Debug.Log("Instance of player already exists");
        }
    }

    public Player GetInstance() 
    { 
        return instance; 
    }

    // delete the instance of the player for testing purposes
    public void Delete()
    {
        instance = null;
    }

    // Find the player's rigid body.
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Get the player's movement, calls FaceMouse function.
    private void Update()
    {
        playerMovement.x = Input.GetAxisRaw("Horizontal");
        playerMovement.y = Input.GetAxisRaw("Vertical");
        if (playerMovement.x != 0f || playerMovement.y != 0f)
        {
            FindObjectOfType<AudioManager>().PlayMovementSound("playerSteps", this.transform.position);
        }
        FaceMouse();
    }

    // Move the player based on the direction of the input.
    private void FixedUpdate()
    {
        currentSpeed += Time.deltaTime * acceleration;
        if (currentSpeed > maxSpeed)
        {
            currentSpeed = maxSpeed;
        }

        rigidBody.velocity = (new Vector2(playerMovement.x, playerMovement.y).normalized) * currentSpeed;

        if (playerMovement.x == 0 && playerMovement.y == 0)
        {
            currentSpeed = 0f;
        }
    }

    // rotates player to face toward mouse
    private void FaceMouse()
    {

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);

        transform.up = direction;
    }

    // Get the player's x and y for other files. 
    public Vector2 getPlayerPos()
    {
        return transform.position;
    }

    // Get current speed of player entity
    public float GetPlayerSpeed()
    {
        return currentSpeed;
    }

    // Move player for testing purposes
    public void SetInput()
    {
        playerMovement.x = 1f;
        playerMovement.y = 1f;
    }

    // set player speed for testing purposes
    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }
}

LightEntity:
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;

public class LightEntity : MonoBehaviour
{
    public LightState currentState;
    public OnState onState = new OnState();
    public OffState offState = new OffState();

    [SerializeField] public GameObject lightObject;

    // sets light state to on
    public virtual void TurnOn()
    {
        currentState = onState;
        currentState.EnterState(lightObject);
    }

    // sets light state to off
    public virtual void TurnOff()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // returns state for testing purposes
    public LightState GetState()
    {
        return currentState;
    }

    // checks if flashlight is on for testing purposes
    public bool IsOn()
    {
        if (currentState == onState)
        {
            return true;
        }
        return false;
    }
}

Flashlight:
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Flashlight : LightEntity
{
    public static Flashlight instance; 
    
    [SerializeField] public LightEntity lantern;

    private float batteryLife = 100;
    [SerializeField] private Slider batteryChargeIndicator;

    public static void CreateFlashlight()
    {
        GameObject flashlightObject = new GameObject();
        instance = flashlightObject.AddComponent<Flashlight>();
    }

    // initialized flashlight to off state
    void Start()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // checks for player flashlight inputs and drains the battery if in the on state
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            toggleState();
        }
    }

    private void FixedUpdate()
    {
        if (currentState == onState)
        {
            DrainBattery(0.05f);
            UpdateBatteryChargeIndicator();
            if (batteryLife <= 0f)
            {
                toggleState();
            }
        }
    }

    // switches the state of the flashlight
    public void toggleState()
    {
        if (currentState == onState)
        {
            TurnOff(); // inherited from LightEntity class
        }
        else if(batteryLife >= 0f)
        {
            TurnOn();
        }
    }

    // sets the flashlight to the on state and the lantern to the off state
    public override void TurnOn()
    {
        //Debug.Log("test");
        lantern.TurnOff();
        currentState = onState;
        currentState.EnterState(lightObject);
    }

    // reduces the amount of charge in the flashlight by amount
    public void DrainBattery(float amount)
    {
        batteryLife -= amount;
        if(batteryLife < 0)
        {
            batteryLife = 0;
        }
    }
    
    // increases the amount of charge in the flashlight by amount
    public void AddBatteryLife(float amount)
    {
        batteryLife += amount;
        // clamp battery life at 100
        if (batteryLife > 100)
        {
            batteryLife = 100;
        }
        UpdateBatteryChargeIndicator();
    }

    // updates the on screen battery indicator
    public void UpdateBatteryChargeIndicator()
    {
        batteryChargeIndicator.value = batteryLife;
    }

    // returns current battery life for testing purposes
    public float GetBatteryLife()
    {
        return batteryLife;
    }

    public LightState GetOffState()
    {
        return offState;
    }

    public LightState GetOnState()
    {
        return onState;
    }
}

Lantern: 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR.Haptics;

[System.Serializable]
public class Lantern : LightEntity
{
    public static Lantern instance;

    [SerializeField] public LightEntity flashlight;

    public static void CreateLantern()
    {
        GameObject lanternObject = new GameObject();
        instance = lanternObject.AddComponent<Lantern>();
    }

    // initializes lantern to off
    void Start()
    {
        currentState = offState;
        currentState.EnterState(lightObject);
    }

    // checks for player lantern input
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            toggleState();
        }
    }

    public void toggleState()
    {
        if (currentState == onState)
        {
            TurnOff();
        }
        else
        {
            TurnOn();
        }
    }

    // turns on the lantern and turns off the flashlight
    public override void TurnOn()
    {
        flashlight.TurnOff();
        currentState = onState;
        currentState.EnterState(lightObject);
    }
}

