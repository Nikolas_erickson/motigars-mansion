## TL1 (Nik) - Git Information

[TL1 Presentation Video](https://vandalsuidaho-my.sharepoint.com/:v:/g/personal/eric7862_vandals_uidaho_edu/EXuICslkaBdKvymRV2VPf_gBURRxbk2rsu3HnNbFeelKMw?e=wMrol4&nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZy1MaW5rIiwicmVmZXJyYWxBcHBQbGF0Zm9ybSI6IldlYiIsInJlZmVycmFsTW9kZSI6InZpZXcifX0%3D)

[TL1 Document](http://tinyurl.com/TL1GitDoc)


## TL2 (Jesus) - MVP

[TL2 Presentation Video](paste link here)

[Jesus - Minimum Viable Product](paste link here)

[Zoe - Start Animations](paste link here)

[Zoe and Lacey - Finish Animation and Start Menus](https://youtu.be/SqAtcnm9zzk)

[Lacey - Finish Menus and start Background](https://youtu.be/JuBbmCn5YLM)

[K - Enemies](https://youtu.be/4mFkOq9Y_XY)

[Sam - Parallax](paste link here)

[Sam - Continue Parallax Part 2](https://youtu.be/qiCRzbM6sR8)

[Matteo - ?](paste link here)

[Upal - ?](paste link here)

[TL2 Document](http://tinyurl.com/gombdoc)


## TL3 (Rory) - Unit Testing & Design Patterns

[TL3 Presentation Video](https://www.youtube.com/watch?v=Z8Vl61bSiz8)

[TL3 Document](http://tinyurl.com/tl3docs)

[Tl3 Slides](https://tinyurl.com/TL3SLIDES)


## TL4 (Andrew) - Coupling and Cohesion

[TL4 Presentation Video]()

[TL4 Document](https://vandalsuidaho-my.sharepoint.com/:p:/g/personal/plum0598_vandals_uidaho_edu/ER9mhvEfGUxHvtaxm_G6pGcBUYPZMnhOzJ_MsmExt3oyPw?e=e1Vnoa)


## TL5 (Ian) - AI

[TL5 Presentation Video]()

[TL5 Document]()


## TL6 (Brian) - Version Control

[TL6 Presentation Video](https://www.dropbox.com/scl/fi/zt6d7ks9xu025k40u7w1m/video1103716213.mp4?rlkey=cw6h8oso0a5goiplokan5hfh3&dl=0)

[TL6 Slides](https://tinyurl.com/tl6slides)

[TL6 What's Next](https://tinyurl.com/tl6next)
