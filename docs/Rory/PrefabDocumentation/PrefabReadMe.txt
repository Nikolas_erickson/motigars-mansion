This README is for the Pause Screen Package.
Please read all of it before attempting to integrate it into your project.
The package contains a demo scene.

Asset includes:
	Pause Button: A button that displays in the top right of the screen and on clicking it pauses the game and pulls up the Pause Menu.
	Pause Menu: A panel that overlays over the game and allows the user to either go the main menu, resume game, or open the Options Menu.
	Options Menu: A panel that overlays over the game and allows the user to adjust the volume of the game and change the graphical quality of the game.

How to use:
	1. Ensure that the PauseTime script and the SceneSwitch script are attached to the PausePanel.
		a. Ensure that in the PauseTime script, the PausePanel is the object in PauseMenu and that the PauseButton is the object in PauseButton.
	2. Ensure that OptionsMenu has the OptionsMenu script attached.
		a. The OptionsMenu object should be the OptionsMenu panel, the MainMenu object should be the PausePanel, and the AudioMixer object should be the main audio mixer for your game.
	3. Ensure that every button has the ClickButton script on it with the Img being the button object itself and the Up and Pressed being the necessary images for your buttons
	4. Ensure that in the PausePanel buttons, that the HomeButton has to OnClick() functionalities, both using the PausePanel object with one calling SceneSwitch.SwitchScene for your main menu and the other calling SceneSwitch.SwitchBackgroundMusic with the associated music being the string used.
	5. For the ResumeButton have two on click functionalities again, one being PauseTime.ResumeGame and the other being SceneSwitch.SwitchBackgroundMusic with the music of the scene being the string called.
	6. For the PauseButton have two on click functionalities again, one being PauseTime.PauseGame using the PausePanel object and the other being SceneSwitch.SwitchBackgroundMusic with the string being the music for your pause menu.
	7. Ensure that the ReturnButton in the OptionsMenu has the OnClick functionality OptionsMenu.MainTime from the OptionsMenu object.
	8. Ensure that the Slider has OnValueChanged functionality OptionsMenu.SetVolume from the options menu object.
	9. Ensure that the GraphicsDropDown has the OnValueChanged functionality OptionsMenu.SetQuality from the OptionsMenu object.
	10. Drag the prefab into the scene you would like to add it to.

Code:
	OptionsMenu:

using UnityEngine;
using UnityEngine.Audio;

interface IOptionsMenu
{
    void SetVolume(float volume);      // Sets volume of game
    void SetQuality(int qualityIndex); // Sets graphical quality
    void OptionsTime();                // Opens option menu and closes menu it was called from
    void MainTime();                   // Closes option menu and opens the menu it was called from
}

public class OptionsMenu : MonoBehaviour, IOptionsMenu
{
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private AudioMixer audioMixer;

    // Function for adjusting the volume of the game
    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    // Function for adjusting graphical quality
    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // Function for pausing game so that it will stop the time and open menu
    public void OptionsTime()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    // Function for unpausing game so it will start time and close pause menu
    public void MainTime()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
}

	PauseTime:

using UnityEngine;

interface IPauseTime
{
    void PauseGame();   // Function for pausing game so that it will stop the time and open menu
    void ResumeGame();  // Function for unpausing game so it will start time and close pause menu
}

public class PauseTime : MonoBehaviour, IPauseTime
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject pauseButton;

    // Function for pausing game so that it will stop the time and open menu
    public void PauseGame()
    {
        Debug.Log("Button Clicked");
        pauseButton.SetActive(false);
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    // Function for unpausing game so it will start time and close pause menu
    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        pauseButton.SetActive(true);
        Time.timeScale = 1f;
    }
}

	ClickyButton:

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image img;
    [SerializeField] private Sprite up, pressed;

    //Changes to appropriate sprite when button is clicked
    public void OnPointerDown(PointerEventData eventData)
    {
        img.sprite = pressed;
    }

    //Changes to appropriate sprite when button is unclicked
    public void OnPointerUp(PointerEventData eventData)
    {
        img.sprite = up;
    }
}

	SceneSwitch:

using UnityEngine;
using UnityEngine.SceneManagement;

interface ISceneSwitch
{
    void LeaveTheGame();                          // Closes application
    void SwitchScene(string sceneName);           // Switches to correct scene
    void SwitchBackgroundMusic(string musicName); // Switches to the correct background music
}

public class SceneSwitch : MonoBehaviour, ISceneSwitch
{
    // Closes the game on button press
    public void LeaveTheGame()
    {
        Application.Quit();
    }

    // Changes a scene when button is clicked
    public void SwitchScene(string sceneName)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneName);
    }

    // Changes music on SceneSwitch
    public void SwitchBackgroundMusic(string musicName)
    {
        FindObjectOfType<AudioManager>().PlayBackgroundMusic(musicName, Camera.main.transform.position);
    }
}